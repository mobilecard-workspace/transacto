<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
    <title>Transacci&oacute;n declinada</title>
    <!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
%>
    	<style>
	    * {
		  /*with these codes padding and border does not increase it's width.Gives intuitive style.*/
		  -webkit-box-sizing: border-box;   
		  -moz-box-sizing: border-box;
		  box-sizing: border-box;
		  color:#33313;
		  font-family: "Arial", Helvetica, sans-serif;
		}

		body {
		   margin:0;
		   padding:0;		  
		   background-color:#8C8C8C;
		   color: white;  
		}
		div#envelope{
			width:90%;
			margin: 0 auto;
			background-color:#D3441C;
			padding:10px 0;
			border:1px solid gray;
			border-radius:10px;
		} 
		table{
			width:80%;
			margin:0 10%;
		}  
		table header {
		  text-align:center;
		  font-family: 'Roboto Slab', serif;
		}
		
	</style>
	<script>
		function myFunction(){
	
	
		var formData = ${json}; // la cadena del json con los parametros {"autorizacion": "autorizaction","status": 200, "total": 1.0}
		$.ajax({
		            type : 'POST',
		            
		            url : "http://199.231.161.38:8081/APIServices/services/3DSresponse",
		            //Add the request header
		            headers : {
		            	'Content-Type': 'application/json' ,
		                'Authorization' : '${token}' // token tambien lo paso en el modelo
		            },
		            contentType : 'application/x-www-form-urlencoded',
		            data : formData,
		            success : function(response) {
		                console.log(response);
		            },
		            error : function(xhr, status, error) {
		                var err = eval('(' + xhr.responseText + ')');
		                console.log(err);                   
		            }
		        }); 
		}
	</script>
	<script type="text/javascript">
		function sendform() {
			document.form1.submit();
		}
	</script>
</head>
<body onload="sendform();">
	<form method="post" name="form1" action="http://199.231.161.38:8081/APIServices/api/v1.0/services/3DSresponse">
		<input type="hidden" name="autorizacion" value="${autorizacion}" /> 
		<input type="hidden" name="status" value="${status}" /> 
		<input type="hidden" name="total" value="${total}" /> 
		<input type="hidden" name="idBitacora" value="${idBitacora}" /> 
		<input type="hidden" name="access_token" value="${access_token}" /> 
		<div id="envelope">
			<table class="table ">
				<thead>
					<tr class="cart_menu">
						<th class="description">Resultado de la Transacci&oacute;n</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><p>${mensajeError}</p></td>
					</tr>
					<tr>
						<td><BR>
						<BR>Presione el boton Atras para terminar.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</form>
</body>
</html>