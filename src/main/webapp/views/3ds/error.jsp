<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width"/>
	<title>Error</title>
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
</head>
<body>
	<table cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="2" align="center">
				<h2>Error al procesar el pago</h2>
			</td>
		</tr>
		<tr>
			<td>Error:</td><td>${descError}</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<br><br>
			</td>
		</tr>
		<tr>
			<td colspan="2">Presione el Boton Atras para terminar su transacci&oacute;n.</td>
		</tr>
	</table>
</body>
</html>