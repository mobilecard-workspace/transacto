<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
    <title>Purchase Verification</title>
    <!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
%>
</head>
<body>

<div id="envelope">
	<%-- <p style="text-align: center;">
			<img src="https://www.mobilecard.mx:8443/ADOServicios/resources/logo-ado.jpg" width="100px" height="30px" align="center"/>
		</p>--%>
		<p style="text-align: center;">3D Secure Portal to mobile payments</p>
		<p style="text-align: center;">Provides the following information:</p>
		<br/>
		<form:form id="main-form" name="main-form" method="post" 
    		autocomplete="off" modelAttribute="pagoForm"
    		action="${pageContext.request.contextPath}/${accountId}/enviaPago3DS">
    		<form:hidden id="referencia" path="referencia"  />
    		<form:hidden id="idUsuario" path="idUsuario" />
    		<form:hidden id="idProveedor" path="idProveedor" />
    		<form:hidden id="emisor" path="emisor" />
    		<form:hidden id="operacion" path="operacion" />
    		<form:hidden id="concepto" path="concepto" />
    		<form:hidden id="comision" path="comision" />
    		<form:hidden id="monto" path="monto" />
    		<form:hidden id="eMail" path="email" />
    		<form:hidden id="idTransaccion" path="idTransaccion" />
    		<form:hidden id="idTarjeta" path="idTarjeta" />
    		<form:hidden id="banco" path="banco" />
    		<form:hidden id="codigoPais" path="codigoPais" />
    		<form:hidden id="idPais" path="idPais" />
    		<form:hidden id="idioma" path="idioma" />
    		<form:hidden id="idApp" path="idApp" />
    		<form:hidden id="lat" path="lat" />
    		<form:hidden id="lon" path="lon" />
    		<form:hidden id="tarjetaT" path="tarjetaT" />
    		<form:hidden id="token" path="token" />
				<p> Credit Card Information</p>

				<label for="nombre">Name: ${pagoForm.nombre}</label>
				<br></br>
				<!-- form:input type="text" path="nombre" size="40,1" maxlength="40" value="" required="true" /-->
				
				<label for="tarjeta">Card Number: ${pagoForm.tarjeta}</label>
				<br></br>
				<!-- form:input type="text" path="tarjeta" size="40,1"	maxlength="16" value="" required="true" /-->

				<label for="tipoTarjeta">Type: ${pagoForm.tarjetaT}</label><br/>
				
				<!-- div class="styled-select">
					<form:select path="tipoTarjeta" style="-moz-appearance: none;text-indent: 0.01px;text-overflow: '';">
						<option value="1">VISA</option>
						<option value="2">MasterCard</option>
						<option value="3">Carnet</option>
					</form:select  >
				</div -->
				<div style="width:100%;padding-top:10px">
					<label for ="mes">Expiration Date (month/year) : ${pagoForm.vigencia}</label><br/>
					<!-- div class="styled-select" style="float:left;width:40%;">
						<form:select  path="mes" style="-moz-appearance: none;text-indent: 0.01px;text-overflow: '';">
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</form:select> 
					</div>
					<div  class="styled-select" style="float:right;width:40%;">
						<form:select  path="anio" class="selmenu">
							<%
								java.util.Calendar C = java.util.Calendar.getInstance();
								int anio = C.get(java.util.Calendar.YEAR);
								for (int i = 0; i < 15; i++) {
									out.println("<option value=\""+anio+"\">" + anio+ "</option>");
									anio++;
								}
							%>
						</form:select>
					</div -->
				</div>
				<div style="width:100%;padding-top:10px">
					<label for ="cvv2">Security Code (CVV2/CVC2)</label>
					<form:input type="password" path="cvv2" size="3,1" maxlength="3" value="" required="true" class="txtinput"/>
				</div>
				<p>Verify the data and select the Pay button to charge your card.</p>
				<div style="width:100%;padding-top:10px">
					<input type="submit" value="Pay" class="btn-style"/>
				</div>
				<!-- <div style="width:100%;padding-top:10px">
					<input type="button" value="Cancelar" />
				</div> -->
			</div>
		</form:form>
	</div>
</body>
</html>
