<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
    <title>Purchase Verification</title>
    <!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
%>
  
</head>
<body>

<div id="envelope">

<form method="post" name="form1" action="https://via.banorte.com/payw2">
	<input type="hidden" name="ID_AFILIACION" value="${prosa.ID_AFILIACION}" /> 
	<input type="hidden" name="USUARIO" value="${prosa.USUARIO}" /> 
	<input type="hidden" name="CLAVE_USR" value="${prosa.CLAVE_USR}" /> 
	<input type="hidden" name="CMD_TRANS" value="${prosa.CMD_TRANS}" /> 
	<input type="hidden" name="ID_TERMINAL" value="${prosa.ID_TERMINAL}" /> 
	<input type="hidden" name="MONTO" value="${prosa.Total}" /> 
	<input type="hidden" name="MODO" value="${prosa.MODO}" /> 
	<input type="hidden" name="REFERENCIA" value="${prosa.REFERENCIA}" />
	<input type="hidden" name="NUMERO_CONTROL" value="${prosa.Reference3D}" /> 
	<%-- <input type="hidden" name="REF_CLIENTE1" value="${prosa.REF_CLIENTE1}" /> 
	<input type="hidden" name="REF_CLIENTE2" value="${prosa.REF_CLIENTE2}" /> 
	<input type="hidden" name="REF_CLIENTE3" value="${prosa.REF_CLIENTE3}" /> 
	<input type="hidden" name="REF_CLIENTE4" value="${prosa.REF_CLIENTE4}" /> 
	<input type="hidden" name="REF_CLIENTE5" value="${prosa.REF_CLIENTE5}" /> --%> 
	<%-- <input type="hidden" name="NUMERO_TARJETA" value="${prosa.Number}" /> 
	<input type="hidden" name="FECHA_EXP" value="${prosa.FECHA_EXP}" /> 
	<input type="hidden" name="CODIGO_SEGURIDAD" value="${prosa.CODIGO_SEGURIDAD}" /> --%> 
	<%-- <input type="hidden" name="CODIGO_AUT" value="${prosa.auth}" /> --%> 
	<input type="hidden" name="MODO_ENTRADA" value="${prosa.MODO_ENTRADA}" />
	<%-- <input type="hidden" name="LOTE" value="${prosa.lote}" /> --%>
	<input type="hidden" name="URL_RESPUESTA" value="${prosa.URL_RESPUESTA}" />
	<input type="hidden" name="IDIOMA_RESPUESTA" value="${prosa.IDIOMA_RESPUESTA}" />
	<c:if test="${prosa.XID != null}">
		<input type="hidden" name="XID" value="${prosa.XID}" />
		<input type="hidden" name="CAVV" value="${prosa.CAVV}" />
		<input type="hidden" name="ESTATUS_3D" value="${prosa.ESTATUS_3D}" />
		<input type="hidden" name="ECI" value="${prosa.ECI}" />	
	</c:if>
	<%-- <input type="hidden" name="DIFERIMIENTO_INICIAL" value="${prosa.DIFERIMIENTO_INICIAL}" />
	<input type="hidden" name="NUMERO_PAGOS" value="${prosa.NUMERO_PAGOS}" />
	<input type="hidden" name="TIPO_PLAN" value="${prosa.TIPO_PLAN}" /> --%>
</form>

<table class="table ">
	<thead>
		<tr class="cart_menu">
			<th class="description">Envio de informaci&oacute;n.</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="cart_description">
				<p><br>Ocurrio un error y se est&aacute; enviando la informaci&oacute;n al Banco para realizar la devoluci&oacuten, favor de esperar.<br><br></p>
			</td>
		</tr>
		<%-- <tr>
			<td align="center">
				<p><img src="<c:url value="/resources/images/loading2.gif"/>" /></p>
			</td>
		</tr> --%>
	</tbody>
</table>

<script type="text/javascript">
	function sendform() {
	    setTimeout (function () {
	    	document.form1.submit();
	    }, 1000);
	}
	
	sendform();            
</script>
</div>
</body>
</html>
