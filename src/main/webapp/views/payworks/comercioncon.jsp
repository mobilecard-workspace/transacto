<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Respuesta Pago</title>
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
</head>
<body>
	<table cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="2" align="center">
				<h1>Transaccion exitosa</h1>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				Su pago fue procesado correctamente.
			</td>
		</tr>
		<%-- <tr>
			<td>Respuesta:</td>
			<td>${prosa.respuesta}%></td>
		</tr>
		<tr>
			<td>Linea:</td>
			<td>${prosa.linea}%></td>
		</tr>
		<tr>
			<td>Placa:</td>
			<td>${prosa.placa}%></td>
		</tr>
		<tr>
			<td>Autorizaci&oacute;n:</td>
			<td>${prosa.autorizacion}%></td>
		</tr>
		<tr>
			<td>Referencia:</td>
			<td>${prosa.referencia}%></td>
		</tr> --%>

		<tr>
			<td>Respuesta:</td>
			<td>${ECI}%></td>
		</tr>
		<tr>
			<td>CardType:</td>
			<td>${CardType}%></td>
		</tr>
		<tr>
			<td>XID:</td>
			<td>${XID}%></td>
		</tr>
		<tr>
			<td>CAVV:</td>
			<td>${CAVV}%></td>
		</tr>
		<tr>
			<td>Status:</td>
			<td>${Status}%></td>
		</tr>
		<tr>
			<td>Reference3D:</td>
			<td>${Reference3D}%></td>
		</tr>
		<tr>
			<td colspan="2">Presione OK para terminar su transacci&oacute;n</td>
		</tr>
	</table>
</body>
</html>