<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
	<title>Purchase Verification</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width" />
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
</head>
<body style="width: 100%">
	<form method="post" name="form1" autocomplete="off"
		action="https://eps.banorte.com/secure3d/Solucion3DSecure.htm">
		<input type="hidden" name="Reference3D" value="${prosa.Reference3D}" />
		<input type="hidden" name="MerchantId" value="${prosa.MerchantId}" />
		<input type="hidden" name="MerchantName" value="TRANSACTO" />
		<input type="hidden" name="MerchantCity" value="Monterrey" />
		<input type="hidden" name="Cert3D" value="03" />
		<input type="hidden" name="NoFrame" value="True" />
		<input type="hidden" name="ForwardPath" value="${prosa.ForwardPath}" />
		<input type="hidden" name="Expires" value="${prosa.Expires}" />
		<INPUT TYPE="hidden" NAME="Total" VALUE="${prosa.Total}"> 
		<INPUT TYPE="hidden" NAME="Card" VALUE="${prosa.Card}"> 
		<INPUT TYPE="hidden" NAME="CardType" VALUE="${prosa.CardType}">
	</form>
	<div class="container">
		
		<br />
		<table border="0" width="100%">
			<thead>
			<tr>
				<th><p>Envio de informaci&oacute;n.</p></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					<p><br/>Se est&aacute; enviando la informaci&oacute;n, favor de esperar. <br/>
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		document.form1.submit();
	</script>
</body>
</html>
