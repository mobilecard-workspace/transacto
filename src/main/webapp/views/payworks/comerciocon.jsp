<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Respuesta Pago</title>
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
</head>
<body>
<center>
<input type="hidden" name="linea" value="${prosa.linea}" /> 
	<table cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="2" align="center">
				<h1>Transaccion exitosa</h1>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				</BR>
				Su pago fue procesado correctamente.
			</td>
		</tr>
		<tr>
			<td>Respuesta:</td>
			<td>${prosa.RESULTADO_PAYW}</td>
		</tr>
		<tr>
			<td>Texto:</td>
			<td>${prosa.TEXTO}</td>
		</tr>
		<tr>
			<td>Autorización:</td>
			<td>${prosa.CODIGO_AUT}</td>
		</tr>
		<tr>
			<td>ReferenciaMC:</td>
			<td>${prosa.NUMERO_CONTROL}</td>
		</tr>
		<%-- <tr>
			<td>Respuesta:</td>
			<td>${resp.ECI}</td>
		</tr>
		<tr>
			<td>CardType:</td>
			<td>${resp.CardType}</td>
		</tr>
		<tr>
			<td>XID:</td>
			<td>${resp.XID}</td>
		</tr>
		<tr>
			<td>CAVV:</td>
			<td>${resp.CAVV}</td>
		</tr>
		<tr>
			<td>Status:</td>
			<td>${resp.Status}</td>
		</tr>
		<tr>
			<td>Reference3D:</td>
			<td>${resp.Reference3D}</td>
		</tr>
		<tr>
			<td>Card:</td>
			<td>${resp.Card}</td>
		</tr>
		<tr>
			<td>_cc_expmonth:</td>
			<td>${resp._cc_expmonth}</td>
		</tr>
		<tr>
			<td>_cc_expyear:</td>
			<td>${resp._cc_expyear}</td>
		</tr>
		<tr>
			<td>cc_cvv2:</td>
			<td>${resp.cc_cvv2}</td>
		</tr> --%>
		<tr>
			</BR>
			<td colspan="2">Presione el Boton Atras para terminar la transacci&oacute;n</td>
		</tr>
	</table>
</center>
</body>
</html>