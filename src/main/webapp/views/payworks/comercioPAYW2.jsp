<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width" />
	<title>Redirección Payworks</title>
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
	<script type="text/javascript">
		function sendform() {
			document.form1.submit();
		}
	</script>
</head>
<body onload="sendform();">
	<form method="post" name="form1" action="https://via.banorte.com/payw2">
		<input type="hidden" name="ID_AFILIACION" value="${prosa.ID_AFILIACION}" /> 
		<input type="hidden" name="USUARIO" value="${prosa.USUARIO}" /> 
		<input type="hidden" name="CLAVE_USR" value="${prosa.CLAVE_USR}" /> 
		<input type="hidden" name="CMD_TRANS" value="${prosa.CMD_TRANS}" /> 
		<input type="hidden" name="ID_TERMINAL" value="${prosa.ID_TERMINAL}" /> 
		<input type="hidden" name="MONTO" value="${prosa.Total}" /> 
		<input type="hidden" name="MODO" value="${prosa.MODO}" /> 
		<input type="hidden" name="NUMERO_CONTROL" value="${prosa.Reference3D}" /> 
		<input type="hidden" name="NUMERO_TARJETA" value="${prosa.Number}" /> 
		<input type="hidden" name="FECHA_EXP" value="${prosa.FECHA_EXP}" /> 
		<input type="hidden" name="CODIGO_SEGURIDAD" value="${prosa.CODIGO_SEGURIDAD}" />  
		<input type="hidden" name="MODO_ENTRADA" value="${prosa.MODO_ENTRADA}" />
		<input type="hidden" name="URL_RESPUESTA" value="${prosa.URL_RESPUESTA}" />
		<input type="hidden" name="IDIOMA_RESPUESTA" value="${prosa.IDIOMA_RESPUESTA}" />
		<%
		    String xid = request.getParameter("XID");
			String cavv = request.getParameter("CAVV");
		    if (xid != null || xid != "" && cavv != null || cavv != "") {
		%>
		    <input type="hidden" name="XID" value="${prosa.XID}" />
			<input type="hidden" name="CAVV" value="${prosa.CAVV}" />
		<%
		    }
		%>
		
		
		<input type="hidden" name="ESTATUS_3D" value="${prosa.Status}" />
		<input type="hidden" name="ECI" value="${prosa.ECI}" />
	</form>
	<div class="envelope">
		
		<br />
		<table border="0" width="100%">
			<thead>
			<tr class="cart_menu">
				<th class="description"><p>Envio de informaci&oacute;n.</p></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td class="cart_description">
					<p><br/>Se est&aacute; enviando la informaci&oacute;n, favor de esperar. <br/>
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</body>
</html>