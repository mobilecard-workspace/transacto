<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
    <title>Purchase Verification</title>
    <!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
%>
</head>
<body>

<div id="envelope">
	<%-- <p style="text-align: center;">
			<img src="https://www.mobilecard.mx:8443/ADOServicios/resources/logo-ado.jpg" width="100px" height="30px" align="center"/>
		</p>--%>
		<p style="text-align: center;">Portal 3D Secure para pago m&oacute;vil</p>
		<p style="text-align: center;">Proporciona la siguiente informaci&oacute;n:</p>
		<br/>
		<form:form id="main-form" name="main-form" method="post" 
    		autocomplete="off" modelAttribute="pagoForm"
    		action="${pageContext.request.contextPath}/${accountId}/enviaPago3DS">
    		<form:hidden id="referencia" path="referencia"  />
    		<form:hidden id="idUsuario" path="idUsuario" />
    		<form:hidden id="idProveedor" path="idProveedor" />
    		<form:hidden id="emisor" path="emisor" />
    		<form:hidden id="operacion" path="operacion" />
    		<form:hidden id="concepto" path="concepto" />
    		<form:hidden id="comision" path="comision" />
    		<form:hidden id="monto" path="monto" />
    		<form:hidden id="eMail" path="email" />
    		<form:hidden id="idTransaccion" path="idTransaccion" />
    		<form:hidden id="idTarjeta" path="idTarjeta" />
    		<form:hidden id="banco" path="banco" />
    		<form:hidden id="codigoPais" path="codigoPais" />
    		<form:hidden id="idPais" path="idPais" />
    		<form:hidden id="idioma" path="idioma" />
    		<form:hidden id="idApp" path="idApp" />
    		<form:hidden id="lat" path="lat" />
    		<form:hidden id="lon" path="lon" />
    		<form:hidden id="tarjetaT" path="tarjetaT" />
    		<form:hidden id="token" path="token" />
				<p> Informaci&oacute;n de la tarjeta de cr&eacute;dito</p>

				<label for="nombre">Nombre: ${pagoForm.nombre}</label>
				<br></br>
				<label for="tarjeta">N&uacute;mero de Tarjeta: ${pagoForm.tarjeta}</label>
				<br></br>
				<label for="tipoTarjeta">Tipo: ${pagoForm.tarjetaT}</label><br/>
				<div style="width:100%;padding-top:10px">
					<label for ="mes">Fecha de Vencimiento (mes/a&ntilde;o) : ${pagoForm.vigencia}</label><br/>
				</div>
				<div style="width:100%;padding-top:10px">
					<label for ="cvv2">C&oacute;digo de seguridad (CVV2/CVC2)</label>
					<form:input type="password" path="cvv2" size="3,1" maxlength="3" value="" required="true" class="txtinput"/>
				</div>
				<p>Verifica los datos y selecciona el bot&oacute;n Pagar para efectuar el cargo a tu tarjeta.</p>
				<div style="width:100%;padding-top:10px">
					<input type="submit" value="Pagar" class="btn-style"/>
				</div>
			</div>
		</form:form>
	</div>
</body>
</html>
