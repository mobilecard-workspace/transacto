<?xml version="1.0" encoding="UTF-8" ?>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width"/>
	<title>Respuesta transaccion MobileCard Pago</title>
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/>
</head>
<body>

	<%-- <input type="hidden" name="EM_Response" value="${prosa.EM_Response}" />  --%>
	<table cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="2" align="center">
				<h1>
				Transaccion Exitosa
				</h1>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				 
					Su pago fue procesado correctamente.    
				
				</br></br>
			</td>
		</tr>
		<tr>
			<td>Id Transaccion: </td><td>${idTransaccion}</td>
		</tr>
		<tr>
			<td>Tag: </td><td>${tag}</td>
		</tr>
		<tr>
			<td>Autorizacion:</td><td>${autorizacion}</td>
		</tr>
		<tr>
			<td>Importe:</td><td>$ ${importe} MXN</td>
		</tr>
		<tr>
			<td>Comision:</td><td>$ ${comision} MXN</td>
		</tr>
		<tr>
			<td>Total:</td><td>$ ${total} MXN</td>
		</tr>
		<tr>
			<td>Descripcion:</td><td>${descripcionCode}</td>
		</tr>
		<tr>
			<td>Fecha:</td><td>${fecha}</td>
		</tr>
		<tr>
			<td colspan="2">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2">
					</br></br>
					Se ha enviado un correo a la dirección registrada en Mobilecard con los detalles del su pago.
					</br></br>
			</td>
		</tr>
						
		<tr>
			<td colspan="2">Presione el Boton Atras para terminar su transacci&oacute;n.</td>
		</tr>
	</table>
</body>
</html>