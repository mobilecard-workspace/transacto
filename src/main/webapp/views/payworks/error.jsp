<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Error Pago</title>
	<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"/> 
</head>
<body>
	<center>
	<table cellpadding="1" cellspacing="1">
		<tr>
			<td align="center">
				<h1>Error al procesar pago</h1>
			</td>
		</tr>
		<tr>
			<td >
				<h3>No es posible procesar su transaccion</h3>
			</td>
		</tr>
		
		<tr>
			<td>
				<form:form id="main-form" name="main-form" method="post" 
		    		autocomplete="off" modelAttribute="pagoForm"
		    		action="${pageContext.request.contextPath}/enviaReporte/transacto">
					<form:hidden id="idTransaccion" path="idTransaccion" />
					<input type="submit" value="Envia un incidente a Mesa de Servicio" class="btn-style"/>
				</form:form>
			</td>
		</tr>
	</table>
	</center>
</body>
</html>