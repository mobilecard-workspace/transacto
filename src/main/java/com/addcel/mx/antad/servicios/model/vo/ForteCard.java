package com.addcel.mx.antad.servicios.model.vo;

public class ForteCard {

	private String name_on_card;
	private String account_number;
	private String last_4_account_number;
	private String masked_account_number;
	private Integer expire_month;
	private Integer expire_year;
	private String card_type;
	private Integer card_verification_value;
	
	public String getName_on_card() {
		return name_on_card;
	}
	public void setName_on_card(String name_on_card) {
		this.name_on_card = name_on_card;
	}
	public String getAccount_number() {
		return account_number;
	}
	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}
	public String getLast_4_account_number() {
		return last_4_account_number;
	}
	public void setLast_4_account_number(String last_4_account_number) {
		this.last_4_account_number = last_4_account_number;
	}
	public String getMasked_account_number() {
		return masked_account_number;
	}
	public void setMasked_account_number(String masked_account_number) {
		this.masked_account_number = masked_account_number;
	}
	public Integer getExpire_month() {
		return expire_month;
	}
	public void setExpire_month(Integer expire_month) {
		this.expire_month = expire_month;
	}
	public Integer getExpire_year() {
		return expire_year;
	}
	public void setExpire_year(Integer expire_year) {
		this.expire_year = expire_year;
	}
	public String getCard_type() {
		return card_type;
	}
	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}
	public Integer getCard_verification_value() {
		return card_verification_value;
	}
	public void setCard_verification_value(Integer card_verification_value) {
		this.card_verification_value = card_verification_value;
	}
	
	
}