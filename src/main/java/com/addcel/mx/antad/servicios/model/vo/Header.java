package com.addcel.mx.antad.servicios.model.vo;

public class Header {

	private String ID;
	
	private String Test;
	
	private String Truncated;
	
	private String Name;
	
	private String Prepared;

	private Sender Sender;
	
	private String DataSetAction;
	
	private String Extracted;

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getTest() {
		return Test;
	}

	public void setTest(String test) {
		Test = test;
	}

	public String getTruncated() {
		return Truncated;
	}

	public void setTruncated(String truncated) {
		Truncated = truncated;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getPrepared() {
		return Prepared;
	}

	public void setPrepared(String prepared) {
		Prepared = prepared;
	}

	public Sender getSender() {
		return Sender;
	}

	public void setSender(Sender sender) {
		Sender = sender;
	}

	public String getDataSetAction() {
		return DataSetAction;
	}

	public void setDataSetAction(String dataSetAction) {
		DataSetAction = dataSetAction;
	}

	public String getExtracted() {
		return Extracted;
	}

	public void setExtracted(String extracted) {
		Extracted = extracted;
	}
	
}
