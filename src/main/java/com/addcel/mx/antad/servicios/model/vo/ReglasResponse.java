package com.addcel.mx.antad.servicios.model.vo;

public class ReglasResponse {

	private String folio;
    
	private int resultado;
    
	private String mensaje;
    
    private String numAutorizacion; 
    
    private long referencia;
    
    private double cargo;
    
    private double comision;
    
    private String claveRechazo;
    
    private String descripcionRechazo;

    public ReglasResponse(int resultado, String folio, String mensaje){
        this.resultado = resultado;
        this.folio = folio;
        this.mensaje = mensaje;
    }
    
    public ReglasResponse() {
	}
    
	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public int getResultado() {
		return resultado;
	}

	public void setResultado(int resultado) {
		this.resultado = resultado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public long getReferencia() {
		return referencia;
	}

	public void setReferencia(long referencia) {
		this.referencia = referencia;
	}

	public double getCargo() {
		return cargo;
	}

	public void setCargo(double cargo) {
		this.cargo = cargo;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getClaveRechazo() {
		return claveRechazo;
	}

	public void setClaveRechazo(String claveRechazo) {
		this.claveRechazo = claveRechazo;
	}

	public String getDescripcionRechazo() {
		return descripcionRechazo;
	}

	public void setDescripcionRechazo(String descripcionRechazo) {
		this.descripcionRechazo = descripcionRechazo;
	}
	
}
