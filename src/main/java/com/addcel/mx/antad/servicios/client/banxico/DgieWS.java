/**
 * DgieWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.mx.antad.servicios.client.banxico;

public interface DgieWS extends javax.xml.rpc.Service {
    public java.lang.String getDgieWSPortAddress();

    public com.addcel.mx.antad.servicios.client.banxico.DgieWSPort getDgieWSPort() throws javax.xml.rpc.ServiceException;

    public com.addcel.mx.antad.servicios.client.banxico.DgieWSPort getDgieWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
