package com.addcel.mx.antad.servicios.model.vo;

public class CompactData {

	private Header Header;
	
	private DataSet DataSet;

	public Header getHeader() {
		return Header;
	}

	public void setHeader(Header header) {
		Header = header;
	}

	public DataSet getDataSet() {
		return DataSet;
	}

	public void setDataSet(DataSet dataSet) {
		DataSet = dataSet;
	}
	
}
