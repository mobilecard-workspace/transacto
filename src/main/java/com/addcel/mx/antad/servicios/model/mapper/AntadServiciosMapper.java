package com.addcel.mx.antad.servicios.model.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.addcel.mx.antad.servicios.model.vo.AfiliacionVO;
import com.addcel.mx.antad.servicios.model.vo.AntadCredentials;
import com.addcel.mx.antad.servicios.model.vo.AntadDetalle;
import com.addcel.mx.antad.servicios.model.vo.AntadInfo;
import com.addcel.mx.antad.servicios.model.vo.BitacoraDetalleVO;
import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.DatosDevolucion;
import com.addcel.mx.antad.servicios.model.vo.PagosDirectos;
import com.addcel.mx.antad.servicios.model.vo.RuleVO;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.TemplateCorreo;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;

public interface AntadServiciosMapper {

	public int insertaBitacoraTransaccion(BitacoraVO bitacora);
	
	public int insertaBitacoraTransaccionDetalle(BitacoraDetalleVO bitacora);

	public double getComision(int idProveedor);

	public void actualizaBitacora(BitacoraVO bitacora);

	public UsuarioVO getUsuario(@Param(value = "id") long idUsuario, @Param(value="idTarjeta") int idTarjeta);
	
	public void insertaAntadDetalle(AntadDetalle antadDetalle);

	public void insertaBitacoraProsa(BitacoraVO bitacora);
	
	public int addBitacoraProsa(TBitacoraProsaVO b);

	public AfiliacionVO buscaAfiliacion(@Param(value="id") String id);
	
	public int guardaTBitacoraPIN(@Param(value="idBitacora") long idBitacora, @Param(value="pin") String pin);
	
	public String buscarTBitacoraPIN(@Param(value="idBitacora")String idBitacora);
	
	public int updateBitacora(TBitacoraVO b);
	
	public int updateBitacoraProsa(TBitacoraProsaVO b);
	
	public AntadInfo buscarDetallePago(@Param(value="idBitacora") String idBitacora);
	
	public void insertaDatosDevolucion(DatosDevolucion devolucion);
	
	public DatosDevolucion getDatosDevolucion(@Param(value="idBitacora") long idBitacora);

	public String getParametro(@Param(value = "clave") String valor);
	
	public RuleVO selectRule(@Param(value = "idUsuario") long idUsuario, @Param(value = "tarjeta") String tarjeta,
			@Param(value = "idProveedores") String idProveedores, @Param(value = "parametro") String parametro);
	
	public int getBloqueoTarjeta(@Param(value = "tarjeta") String tarjeta);
	
	public double obtenDivisa(@Param(value = "id") String string);

	public PagosDirectos getPago(@Param(value = "id")String idTransaccion);

//	public void actualizaPagoDirecto(String nUMERO_CONTROL, String rESULTADO_PAYW, String tEXTO, String cODIGO_AUT,
//			String cODIGO_PAYW, String bANCO_EMISOR);

	public void insertaPagoDirecto(PagosDirectos pago);
	
	public void actualizaPagoDirecto3D(PagosDirectos pago);
	
	public void actualizaPagoDirectoPayworks(PagosDirectos pago);
	
	public RuleVO validaIntentosFallidos(@Param(value = "id") long idUsuario, @Param(value = "idProveedores") String idProveedores,
			@Param(value = "tarjeta") String tarjeta);

	public int validaTarjetasBloqueadas(@Param(value = "id") long idUsuario);

	public void getBloqueoUsuario(@Param(value = "id") long idUsuario);

	public ArrayList<String> getTarjetasUsuario(@Param(value = "id") long idUsuario);

	public void getBloqueoIMEI(@Param(value = "imei") String imei);
	
	public void bloqueaTarjeta(@Param(value = "tarjeta") String imei);

	public String validaReglas(@Param(value = "idUsuario")long idUsuario, @Param(value = "imei")String imei, 
			@Param(value = "idTarjeta")int idTarjeta, @Param(value = "tarjeta")String tarjeta, 
			@Param(value = "operacion") String operacion);

	public AntadCredentials antadCredenciales();
	
	public UsuarioVO getDatosUsuario(@Param(value = "id") long idUsuario);
	
	public TemplateCorreo getTemplateMail(@Param(value = "id") String nombre,
			@Param(value = "idioma") String idioma, @Param(value = "idApp") Integer idApp);

	public void updateDivisa(@Param(value = "dolar") double dolar, @Param(value = "id") String id);

	public String getToken();
	
	public int difFechaMin(String token);

	public int validaUsuario(@Param(value = "idUsuario") String idUsuario);

	public String getKeyCipher(Integer idApp);

	public Integer validaAcceso(@Param(value = "idApp") Integer idApp);
	
	public int getWhiteList(@Param(value = "idUsuario") long idUsuario);
	
	public double getMontoUsdMXN(@Param(value = "montoUsd") double montoUsd);
	
	public void updateAntadDetalle(@Param(value = "total") double total,
			@Param(value = "comision") double comision,
			@Param(value = "id") long idTransaccion);
	
}
