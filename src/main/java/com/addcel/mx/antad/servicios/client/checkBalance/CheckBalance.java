/**
 * CheckBalance.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.mx.antad.servicios.client.checkBalance;

public interface CheckBalance extends javax.xml.rpc.Service {
    public java.lang.String getCheckBalanceSoapAddress();

    public com.addcel.mx.antad.servicios.client.checkBalance.CheckBalanceSoap getCheckBalanceSoap() throws javax.xml.rpc.ServiceException;

    public com.addcel.mx.antad.servicios.client.checkBalance.CheckBalanceSoap getCheckBalanceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
