package com.addcel.mx.antad.servicios.services;

import static com.addcel.mx.antad.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;
import static com.addcel.mx.antad.servicios.utils.Constantes.JSON_ERROR_INESPERADO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_REALIZA_CONSULTA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_REALIZA_CONSULTA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_PREALIZA_CONSULTA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_SERVICE;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.mx.antad.servicios.client.antad.Consulta;
import com.addcel.mx.antad.servicios.client.antad.ConsultaResponse;
import com.addcel.mx.antad.servicios.client.antad.ObjectFactory;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacion;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacionResponse;
import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.mx.antad.servicios.model.vo.AntadDetalle;
import com.addcel.mx.antad.servicios.model.vo.AntadInfo;
import com.addcel.mx.antad.servicios.model.vo.AprovisionamientoResponse;
import com.addcel.mx.antad.servicios.model.vo.BillPocketResponse;
import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.mx.antad.servicios.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class Claro360Service {

	private static final Logger LOGGER = LoggerFactory.getLogger(Claro360Service.class);

	private static final SimpleDateFormat SDF = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);
	
	@Autowired
	private AntadServiciosMapper mapper;
	
	@Autowired
	private UtilsService jsonUtils;
	
	@Autowired
    @Qualifier("webServiceAntadBridge")
    private transient WebServiceTemplate webServiceTemplate;
	
	private Gson GSON = new Gson();

	public BillPocketResponse aprovisiona(Integer idApp, Integer idPais, String idioma, String json) {
		BillPocketResponse authorization = new BillPocketResponse();
		MobilePaymentRequestVO data = null;
		AntadInfo antad = new AntadInfo();
		AprovisionamientoResponse antadResponse = null;
		try {
			LOGGER.info("JSON - {}", json);
			json = new String(Base64.getDecoder().decode(json), "utf-8");
			LOGGER.info("JSON - {}", json);
			data = GSON.fromJson(json, MobilePaymentRequestVO.class);
			data.setCargo(data.getMonto());
			insertaBitacoras(data);
			antad.setIdTransaccion(data.getIdTransaccion());
			antad.setIdPais("1");
			antadResponse = realizaAprovisionamiento(antad);
			authorization.setIdTransaccion(data.getIdTransaccion());
			authorization.setAmount(data.getCargo());
			authorization.setAuthNumber(antadResponse.getNumAuth());
			authorization.setCode(antadResponse.getIdError());
			authorization.setMessage(antadResponse.getMensajeCajero());
			authorization.setOpId(0);
			authorization.setMaskedPAN(data.getMaskedPan());
			authorization.setStatus(antadResponse.getIdError());
			authorization.setDateTime(new Date().getTime());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("OCURRIO UN ERROR AL APROVISIONAR - {}", e.getMessage());
			authorization.setAmount(data.getCargo());
			authorization.setAuthNumber(antadResponse.getNumAuth());
			authorization.setCode(-50);
			authorization.setMessage("No fue posible contactar al proveedor.");
			authorization.setOpId(0);
			authorization.setStatus(-50);
		}
		LOGGER.info("RESPUESTA DE APROVISIONAMIENTO - {}", GSON.toJson(authorization));
		return authorization;
	}
	
	
	public AprovisionamientoResponse realizaAprovisionamiento(AntadInfo antad) {
		ProcesaAutorizacion autorizacion = null;
		ProcesaAutorizacionResponse response = null;
		AprovisionamientoResponse respuesta = null;
		ObjectFactory factory = new ObjectFactory();
		String json = null;
		try {
			json = GSON.toJson(antad);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_PROCESA_PAGO + json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
			autorizacion = new ProcesaAutorizacion();
			autorizacion.setJson(json);
			response = (ProcesaAutorizacionResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createProcesaAutorizacion(autorizacion));
			json = procesaCifrado(response.getReturn());
			respuesta = (AprovisionamientoResponse) GSON.fromJson(json, AprovisionamientoResponse.class);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_PROCESA_PAGO+" ["+e.getCause()+"]");
			e.printStackTrace();
			respuesta = new AprovisionamientoResponse();
			respuesta.setIdError(-1000);
			respuesta.setMensajeError("Error con el proveedor - Intente nuevamente.");
		}
		return respuesta;
	}

	public String realizaConsulta(Integer idApp, String idioma, String json) {
		ObjectFactory factory = new ObjectFactory();
		Consulta consulta;
		ConsultaResponse response; 
		try {
			json = procesaCifrado(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_REALIZA_CONSULTA + json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
			consulta = new Consulta();
			consulta.setJson(json);
			response = (ConsultaResponse) webServiceTemplate.marshalSendAndReceive(factory.createConsulta(consulta));
			
			json = procesaCifrado("{\"idError\":0,\"mensajeError\":null,\"monto\":0.0,\"descripcion\":null,\"numAutorizacion\":\"403662\","
					+ "\"mensajeAntad\":\"0|SALDO A PAGAR|0|Nombre|CARLOS  EDUARDO CUPUL HAU\",\"referencia\":null,\"concepto\":null,"
					+ "\"idTransaccion\":0,\"comision\":0.0,\"eMail\":null,\"respCode\":\"00\",\"numAuth\":null,\"mensajeTicket\":null,"
					+ "\"mensajeCajero\":null,\"folioTransaccion\":null,\"folioComercio\":null,\"datosAdicionales\":null,\"montoUsd\":0.0,"
					+ "\"comisionUsd\":0.0,\"montoMxn\":0.0,\"comisionMxn\":0.0,\"totalUsd\":0.0,\"totalMxn\":0.0,\"tipoCambio\":0.0}");
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_REALIZA_CONSULTA+"["+e.getCause()+"]");
			json = "{\"idError\":0,\"mensajeError\":null,\"monto\":0.0,\"descripcion\":null,\"numAutorizacion\":\"403662\","
					+ "\"mensajeAntad\":\"0|SALDO A PAGAR|0|Nombre|CARLOS  EDUARDO CUPUL HAU\",\"referencia\":null,\"concepto\":null,"
					+ "\"idTransaccion\":0,\"comision\":0.0,\"eMail\":null,\"respCode\":\"00\",\"numAuth\":null,\"mensajeTicket\":null,"
					+ "\"mensajeCajero\":null,\"folioTransaccion\":null,\"folioComercio\":null,\"datosAdicionales\":null,\"montoUsd\":0.0,"
					+ "\"comisionUsd\":0.0,\"montoMxn\":0.0,\"comisionMxn\":0.0,\"totalUsd\":0.0,\"totalMxn\":0.0,\"tipoCambio\":0.0}";
		} finally{
			LOGGER.info(LOG_RESPUESTA_PREALIZA_CONSULTA + json);
			json = cifraRespuesta(json);
		}
		return json;
	}
	
	private void insertaBitacoras(MobilePaymentRequestVO pago) throws Exception{
		BitacoraVO tb = null;
		String concepto = null;
		try {
			pago.setTarjeta("");
			tb = new BitacoraVO(pago.getIdApp(), 0, 
					pago.getConcepto() + " "+( pago.getTipoTarjeta() == 3?"AMEX":"VISA"), 
					(pago.getSoftware() != null? pago.getSoftware() + " " + pago.getModelo(): "Pago "), 
					"Referencia: " + pago.getReferencia() + ", Importe: " + pago.getCargo(), 
					pago.getTarjeta(), pago.getTipo(), 
					pago.getSoftware(), pago.getModelo(), pago.getImei(), 
					pago.getCargo(), pago.getIdioma());
			tb.setIdProveedor(45);
			if("000033".equals(pago.getOperacion())) {
				concepto = "RECARGA TIEMPO AIRE ";
			} else {
				concepto = "PAGO DE SERVICIOS ";
			}
			tb.setConcepto(concepto);
			tb.setNoAutorizacion(pago.getNumAutorizacion());
			mapper.insertaBitacoraTransaccion(tb);
			pago.setIdTransaccion(tb.getIdBitacora());

			TBitacoraProsaVO tbProsa = new TBitacoraProsaVO(tb.getIdBitacora(),
					pago.getIdUsuario(),null, null, null, 
					"" +pago.getConcepto() + " "+ (pago.getTipoTarjeta() ==3?"AMEX":"VISA"),
					String.valueOf(pago.getCargo()), pago.getComision()+"", 
					pago.getLat(), pago.getLon());	
			tbProsa.setTarjeta("");
			tbProsa.setTransaccion("");
			mapper.addBitacoraProsa(tbProsa);
			AntadDetalle antadDetalle = new AntadDetalle();
			antadDetalle.setIdTransaccion(pago.getIdTransaccion());
			antadDetalle.setDescripcion(concepto);
			antadDetalle.setTipoTarjeta(pago.getTipoTarjeta());
			antadDetalle.setTotal(pago.getCargo());
			antadDetalle.setComision(pago.getComision());
			antadDetalle.setReferencia(pago.getReferencia());
			antadDetalle.setEmisor(String.valueOf(pago.getEmisor()));
			antadDetalle.setCodigoRespuesta("");
			antadDetalle.setOperacion(pago.getOperacion());
			antadDetalle.setIdApp(pago.getIdApp());
			mapper.insertaAntadDetalle(antadDetalle);		
			LOGGER.info("DETALLE ANTAD GUARDADO EXITOSAMENTE - ID TRANSACCION: "+pago.getIdTransaccion()+", LOGIN: "+pago.getUsuario());
			if(pago.getTarjeta() != null && !"".equals(pago.getTarjeta())){
				pago.setTarjeta(AddcelCrypto.decryptTarjeta(pago.getTarjeta()));
			}
		} catch (Exception e) {
			LOGGER.error("Error al guardar en bitacoras,  id_bitacora: {}", tb.getIdBitacora(), e);
			throw new Exception(e);
		}
	}
	
	private static String cifraRespuesta(String json){
		return AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
	}
	
	private static String procesaCifrado(String json){
		return AddcelCrypto.decryptSensitive(json);
	}
	
	public WebServiceTemplate getWebServiceTemplate() {
		return webServiceTemplate;
	}

	public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}
	
}
