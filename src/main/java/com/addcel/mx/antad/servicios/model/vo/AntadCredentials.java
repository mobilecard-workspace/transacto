package com.addcel.mx.antad.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AntadCredentials {

	private int comercio;
	
	private String sucursal;
	
	private String caja;
	
	private String modoIngreso;
	
	private String operacionConsulta;
	
	private String operacionAutorizacion;

	public int getComercio() {
		return comercio;
	}

	public void setComercio(int comercio) {
		this.comercio = comercio;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getCaja() {
		return caja;
	}

	public void setCaja(String caja) {
		this.caja = caja;
	}

	public String getModoIngreso() {
		return modoIngreso;
	}

	public void setModoIngreso(String modoIngreso) {
		this.modoIngreso = modoIngreso;
	}

	public String getOperacionConsulta() {
		return operacionConsulta;
	}

	public void setOperacionConsulta(String operacionConsulta) {
		this.operacionConsulta = operacionConsulta;
	}

	public String getOperacionAutorizacion() {
		return operacionAutorizacion;
	}

	public void setOperacionAutorizacion(String operacionAutorizacion) {
		this.operacionAutorizacion = operacionAutorizacion;
	}
	
}
