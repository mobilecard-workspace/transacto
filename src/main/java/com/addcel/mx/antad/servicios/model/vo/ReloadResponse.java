package com.addcel.mx.antad.servicios.model.vo;

public class ReloadResponse {

	private String ID_GRP;
    private String CARD_NUMBER;
    private String CHECK_DIGIT;
    private String LOCAL_DATE;
    private String AMOUNT;
    private String TRX_NO;
    private String AUTONO;
    private String RESPONSECODE;
    private String DESCRIPTIONCODE;
    
    private long idBitacora;

    public ReloadResponse() {
    }

    public ReloadResponse(String ID_GRP, String CARD_NUMBER, String CHECK_DIGIT, String LOCAL_DATE, String AMOUNT,
                          String TRX_NO, String AUTONO, String RESPONSECODE, String DESCRIPTIONCODE) {
        this.ID_GRP = ID_GRP;
        this.CARD_NUMBER = CARD_NUMBER;
        this.CHECK_DIGIT = CHECK_DIGIT;
        this.LOCAL_DATE = LOCAL_DATE;
        this.AMOUNT = AMOUNT;
        this.TRX_NO = TRX_NO;
        this.AUTONO = AUTONO;
        this.RESPONSECODE = RESPONSECODE;
        this.DESCRIPTIONCODE = DESCRIPTIONCODE;
    }

    public String getID_GRP() {
        return ID_GRP;
    }

    public String getCARD_NUMBER() {
        return CARD_NUMBER;
    }

    public String getCHECK_DIGIT() {
        return CHECK_DIGIT;
    }

    public String getLOCAL_DATE() {
        return LOCAL_DATE;
    }

    public String getAMOUNT() {
        return AMOUNT;
    }

    public String getTRX_NO() {
        return TRX_NO;
    }

    public String getAUTONO() {
        return AUTONO;
    }

    public String getRESPONSECODE() {
        return RESPONSECODE;
    }

    public String getDESCRIPTIONCODE() {
        return DESCRIPTIONCODE;
    }

	public void setID_GRP(String iD_GRP) {
		ID_GRP = iD_GRP;
	}

	public void setCARD_NUMBER(String cARD_NUMBER) {
		CARD_NUMBER = cARD_NUMBER;
	}

	public void setCHECK_DIGIT(String cHECK_DIGIT) {
		CHECK_DIGIT = cHECK_DIGIT;
	}

	public void setLOCAL_DATE(String lOCAL_DATE) {
		LOCAL_DATE = lOCAL_DATE;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public void setTRX_NO(String tRX_NO) {
		TRX_NO = tRX_NO;
	}

	public void setAUTONO(String aUTONO) {
		AUTONO = aUTONO;
	}

	public void setRESPONSECODE(String rESPONSECODE) {
		RESPONSECODE = rESPONSECODE;
	}

	public void setDESCRIPTIONCODE(String dESCRIPTIONCODE) {
		DESCRIPTIONCODE = dESCRIPTIONCODE;
	}

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	
}
