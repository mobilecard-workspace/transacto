package com.addcel.mx.antad.servicios.model.vo;

public class PagosAntadRequestVO {

	private String comercio;
	
	private String sucursal;
	
	private String caja;
	
	private String cajero;
	
	private String horario;
	
	private String ticket;
	
	private String folioComercio;
	
	private String operacion;
	
	private String referencia;
	
	private double monto;
	
	private String emisor;
	
	private String modoIngreso;
	
	private double comision;
	
	private String sku;
	
	private String referencia2;
	
	private String referencia3;

	public String getComercio() {
		return comercio;
	}

	public void setComercio(String comercio) {
		this.comercio = comercio;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getCaja() {
		return caja;
	}

	public void setCaja(String caja) {
		this.caja = caja;
	}

	public String getCajero() {
		return cajero;
	}

	public void setCajero(String cajero) {
		this.cajero = cajero;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getFolioComercio() {
		return folioComercio;
	}

	public void setFolioComercio(String folioComercio) {
		this.folioComercio = folioComercio;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getEmisor() {
		return emisor;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

	public String getModoIngreso() {
		return modoIngreso;
	}

	public void setModoIngreso(String modoIngreso) {
		this.modoIngreso = modoIngreso;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getReferencia2() {
		return referencia2;
	}

	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}

	public String getReferencia3() {
		return referencia3;
	}

	public void setReferencia3(String referencia3) {
		this.referencia3 = referencia3;
	}
	
}
