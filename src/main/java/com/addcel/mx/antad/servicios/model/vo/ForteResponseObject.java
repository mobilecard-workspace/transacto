package com.addcel.mx.antad.servicios.model.vo;

public class ForteResponseObject {

	private String environment;
	private String response_type;
	private String response_code;
	private String response_desc;
	private String avs_result;
	private String cvv_result;
	private String authorization_code;
	
	public String getEnvironment() {
		return environment;
	}
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	public String getResponse_type() {
		return response_type;
	}
	public void setResponse_type(String response_type) {
		this.response_type = response_type;
	}
	public String getResponse_code() {
		return response_code;
	}
	public void setResponse_code(String response_code) {
		this.response_code = response_code;
	}
	public String getResponse_desc() {
		return response_desc;
	}
	public void setResponse_desc(String response_desc) {
		this.response_desc = response_desc;
	}
	public String getAvs_result() {
		return avs_result;
	}
	public void setAvs_result(String avs_result) {
		this.avs_result = avs_result;
	}
	public String getCvv_result() {
		return cvv_result;
	}
	public void setCvv_result(String cvv_result) {
		this.cvv_result = cvv_result;
	}
	public String getAuthorization_code() {
		return authorization_code;
	}
	public void setAuthorization_code(String authorization_code) {
		this.authorization_code = authorization_code;
	}
	
}