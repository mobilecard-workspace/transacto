package com.addcel.mx.antad.servicios.controller;

import static com.addcel.mx.antad.servicios.utils.Constantes.TESTING_SERVICES_WEB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.antad.servicios.services.AntadServiciosService;

@Controller
public class AntadServiciosController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AntadServiciosController.class);
	
	private static final String PATH_REALIZA_PAGO = "/{idApp}/{idioma}/procesaPago";
	
	private static final String PATH_REALIZA_CONSULTA = "/{idApp}/{idioma}/procesaConsultaSaldos";
	
	private static final String PATH_TEST = "/test";
		
	private static final String PATH_GET_DIVISA = "/{idApp}/{idioma}/getDivisa";
	
	private static final String PATH_GET_TOKEN = "/getToken";
	
	private static final String VIEW_HOME = "home";
	
	private static final String REQ_PARAM_JSON = "json";
	
	@Autowired
	private AntadServiciosService antadService;
	
	@RequestMapping(value = PATH_TEST, method=RequestMethod.GET)
	public ModelAndView home() {	
		LOGGER.info(TESTING_SERVICES_WEB);
		ModelAndView mav = new ModelAndView(VIEW_HOME);
		return mav;	
	}
	
	@RequestMapping(value = PATH_REALIZA_PAGO, method=RequestMethod.POST)
	public @ResponseBody String realizaAprovisionamiento(@PathVariable Integer idApp, @PathVariable String idioma, @RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		return antadService.realizaAprovisionamiento(idApp, idioma, jsonEnc);
	}
	
	@RequestMapping(value = PATH_REALIZA_CONSULTA, method=RequestMethod.POST)
	public @ResponseBody String realizaConsulta(@PathVariable Integer idApp, @PathVariable String idioma, @RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		return antadService.realizaConsulta(idApp, idioma, jsonEnc);
	}
	
	@RequestMapping(value = PATH_GET_TOKEN, method=RequestMethod.POST)
	public @ResponseBody String getToken() {
		return antadService.getToken();
	}
	
	@RequestMapping(value = PATH_GET_DIVISA, method=RequestMethod.POST)
	public @ResponseBody String consultaDolar(@PathVariable Integer idApp, @PathVariable String idioma, @RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		return antadService.consultaDolar(idApp, idioma, jsonEnc);
	}	
	
}