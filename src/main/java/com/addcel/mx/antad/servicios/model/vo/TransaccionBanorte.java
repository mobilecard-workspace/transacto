package com.addcel.mx.antad.servicios.model.vo;

public class TransaccionBanorte {

	private String NUMERO_CONTROL;
	
	private String REFERENCIA;
	
	private String FECHA_RSP_CTE;
	
	private String CODIGO_PAYW; 
	
	private String RESULTADO_AUT; 
	
	private String TEXTO;
	
	private String RESULTADO_PAYW;
	
	private String BANCO_EMISOR;
	
	private String FECHA_REQ_CTE;
	
	private String CODIGO_AUT;
	
	private String ID_AFILIACION; 
	
	private String TIPO_TARJETA;
	
	private String MARCA_TARJETA;

	public String getNUMERO_CONTROL() {
		return NUMERO_CONTROL;
	}

	public void setNUMERO_CONTROL(String nUMERO_CONTROL) {
		NUMERO_CONTROL = nUMERO_CONTROL;
	}

	public String getREFERENCIA() {
		return REFERENCIA;
	}

	public void setREFERENCIA(String rEFERENCIA) {
		REFERENCIA = rEFERENCIA;
	}

	public String getFECHA_RSP_CTE() {
		return FECHA_RSP_CTE;
	}

	public void setFECHA_RSP_CTE(String fECHA_RSP_CTE) {
		FECHA_RSP_CTE = fECHA_RSP_CTE;
	}

	public String getCODIGO_PAYW() {
		return CODIGO_PAYW;
	}

	public void setCODIGO_PAYW(String cODIGO_PAYW) {
		CODIGO_PAYW = cODIGO_PAYW;
	}

	public String getRESULTADO_AUT() {
		return RESULTADO_AUT;
	}

	public void setRESULTADO_AUT(String rESULTADO_AUT) {
		RESULTADO_AUT = rESULTADO_AUT;
	}

	public String getTEXTO() {
		return TEXTO;
	}

	public void setTEXTO(String tEXTO) {
		TEXTO = tEXTO;
	}

	public String getRESULTADO_PAYW() {
		return RESULTADO_PAYW;
	}

	public void setRESULTADO_PAYW(String rESULTADO_PAYW) {
		RESULTADO_PAYW = rESULTADO_PAYW;
	}

	public String getBANCO_EMISOR() {
		return BANCO_EMISOR;
	}

	public void setBANCO_EMISOR(String bANCO_EMISOR) {
		BANCO_EMISOR = bANCO_EMISOR;
	}

	public String getFECHA_REQ_CTE() {
		return FECHA_REQ_CTE;
	}

	public void setFECHA_REQ_CTE(String fECHA_REQ_CTE) {
		FECHA_REQ_CTE = fECHA_REQ_CTE;
	}

	public String getCODIGO_AUT() {
		return CODIGO_AUT;
	}

	public void setCODIGO_AUT(String cODIGO_AUT) {
		CODIGO_AUT = cODIGO_AUT;
	}

	public String getID_AFILIACION() {
		return ID_AFILIACION;
	}

	public void setID_AFILIACION(String iD_AFILIACION) {
		ID_AFILIACION = iD_AFILIACION;
	}

	public String getTIPO_TARJETA() {
		return TIPO_TARJETA;
	}

	public void setTIPO_TARJETA(String tIPO_TARJETA) {
		TIPO_TARJETA = tIPO_TARJETA;
	}

	public String getMARCA_TARJETA() {
		return MARCA_TARJETA;
	}

	public void setMARCA_TARJETA(String mARCA_TARJETA) {
		MARCA_TARJETA = mARCA_TARJETA;
	}
	
}
