package com.addcel.mx.antad.servicios.model.vo;

import com.addcel.mx.antad.servicios.utils.Constantes;

public class ProcomVO {
	// Atributos obligatorios
	private String total;
	private String currency;
	private String address;
	private String orderId;
	private String merchant;
	private String store;
	private String term;
	private String digest;
	private String urlBack;

	private String monto;
	private String idServicio;

	// Atributos Opcionales
	private String usuario;
	private String password;
	private String idTramite;
	private String campos;

	private String tarjeta;
	private String mes = "01";
	private String anio = "00";
	private String tipo;

		
	public ProcomVO(String total, String currency, String address, String orderId, String merchant, String store,
			String term, String digest, String urlBack) {
		super();
		this.total = total;
		this.currency = currency;
		this.address = address;
		this.orderId = orderId;
		this.merchant = merchant;
		this.store = store;
		this.term = term;
		this.digest = digest;
		this.urlBack = urlBack;						
	}
	
	public ProcomVO(String total, String currency, String address, String orderId, String merchant, String store,
			String term, String digest, String urlBack, String usuario, String idTramite, String monto,
			String idServicio, String campos) {
		super();
		this.total = total;
		this.currency = currency;
		this.address = address;
		this.orderId = orderId;
		this.merchant = merchant;
		this.store = store;
		this.term = term;
		this.digest = digest;
		this.urlBack = urlBack;
		this.usuario = usuario;
		this.idTramite = idTramite;
		this.monto = monto;
		this.idServicio = idServicio;
		this.campos = campos;
	}

	public ProcomVO(String usuario, String password, String total, String currency, String orderId, String merchant,
			String urlBack, String monto, String idServicio, // String campos,
			String tarjeta, String vigencia, String tipo) {
		super();
		this.total = total;
		this.currency = currency;
		this.usuario = usuario;
		this.password = password;
		this.orderId = orderId;
		this.merchant = merchant;
		this.urlBack = urlBack;
		this.monto = monto;
		this.idServicio = idServicio;
		this.tarjeta = tarjeta;
		try {
			if (vigencia.length() == 5) {
				this.mes = vigencia.substring(0, 2);
				this.anio = vigencia.substring(3);
			}
		} catch (Exception e) {

		}

		if (tipo != null && ("1".equals(tipo) || "2".equals(tipo))) {
			this.tipo = "VISA";
		} else if (tipo != null && "3".equals(tipo)) {
			this.tipo = "MC";
		}
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getUrlBack() {
		return urlBack;
	}

	public void setUrlBack(String urlBack) {
		this.urlBack = urlBack;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getIdTramite() {
		return idTramite;
	}

	public void setIdTramite(String idTramite) {
		this.idTramite = idTramite;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getCampos() {
		return campos;
	}

	public void setCampos(String campos) {
		this.campos = campos;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}
}
