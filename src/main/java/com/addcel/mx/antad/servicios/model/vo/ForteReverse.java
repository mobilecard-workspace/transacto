package com.addcel.mx.antad.servicios.model.vo;

public class ForteReverse {

	private String transactionId;
	
	private Integer idApplicacion;
	
	private String idioma;
	
	private String software;
	
	private String modelo;
	
	private String imei;
	
	private double lat;
	
	private double lon;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Integer getIdApplicacion() {
		return idApplicacion;
	}

	public void setIdApplicacion(Integer idApplicacion) {
		this.idApplicacion = idApplicacion;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}
	
}