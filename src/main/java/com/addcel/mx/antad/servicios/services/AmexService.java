package com.addcel.mx.antad.servicios.services;

import java.text.DecimalFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.model.vo.RespuestaAmex;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class AmexService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AmexService.class);
	
	private static final String AMEX_WEB_URL = "http://localhost/AmexWeb/AmexAuthorization";

	private  Gson gson = new Gson();
	
	public RespuestaAmex enviaPagoAmex(MobilePaymentRequestVO pago, UsuarioVO usuarioVO) {
		RespuestaAmex respAmex = null;
		String json = null;
		try {
			json = solicitaAutorizacionAmex(pago, usuarioVO);
			respAmex = gson.fromJson(json, RespuestaAmex.class);
			LOGGER.debug("RESPUESTA AMEX - CODE: " + respAmex.getCode() + ", TRAN: " + respAmex.getTransaction() 
					+ ", DESCRIPT:  " + respAmex.getDsc()+", ERROR: "+respAmex.getError() +", ERROR DESC: "+respAmex.getErrorDsc());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return respAmex;
	}

	private String solicitaAutorizacionAmex(MobilePaymentRequestVO pago, UsuarioVO usuarioVO) {
		String json = null;
		double total = 0.0;
		String suma = null;
		DecimalFormat amount = new DecimalFormat("#.00");
		try {
			total = pago.getCargo() + pago.getComision();
			suma = amount.format(total).replace(",", ".");
			total = Double.valueOf(suma);
			LOGGER.info("SOLICITANDO AUTORIZACION AMEX - TARJETA: "+usuarioVO.getNumTarjeta()
					+",  VIGENCIA: "+usuarioVO.getVigenciaTarjeta()+", MONTO: "+total
					+", CID: "+usuarioVO.getCvv2()+",  DIR: "+usuarioVO.getDom_amex());
			RestTemplate restClient = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		    map.add("tarjeta", AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
		    map.add("vigencia", AddcelCrypto.decryptTarjeta(usuarioVO.getVigenciaTarjeta()).replace("/", ""));
		    map.add("producto", "MobileCard");
		    map.add("afiliacion", "9353192983");
		    map.add("cid", AddcelCrypto.decryptTarjeta(usuarioVO.getCvv2()));
		    map.add("monto", String.valueOf(total));
		    map.add("direccion", usuarioVO.getDireccion());
		    
		    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		    LOGGER.info("DATOS ENVIADOS A AMEX - {}", gson.toJson(request));	    
		    ResponseEntity<String> response  = restClient.postForEntity(AMEX_WEB_URL, request, String.class);
		    json = response.getBody();
			LOGGER.info("RESPUESTA DE AMEX: "+json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	

}
