package com.addcel.mx.antad.servicios.model.vo;

public class ForteResponse {

	private String transaction_id;
	private String original_transaction_id;
	private String location_id;
	private String action;
	private Double authorization_amount;
	private Integer authorization_code;
	private String entered_by;
	private BillingAddress billing_address;
	private ForteCard card;
	private ForteResponseObject response;
	private BitacoraForte bitacoraResponse;
	
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getOriginal_transaction_id() {
		return original_transaction_id;
	}
	public void setOriginal_transaction_id(String original_transaction_id) {
		this.original_transaction_id = original_transaction_id;
	}
	public String getLocation_id() {
		return location_id;
	}
	public void setLocation_id(String location_id) {
		this.location_id = location_id;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Double getAuthorization_amount() {
		return authorization_amount;
	}
	public void setAuthorization_amount(Double authorization_amount) {
		this.authorization_amount = authorization_amount;
	}
	public Integer getAuthorization_code() {
		return authorization_code;
	}
	public void setAuthorization_code(Integer authorization_code) {
		this.authorization_code = authorization_code;
	}
	public String getEntered_by() {
		return entered_by;
	}
	public void setEntered_by(String entered_by) {
		this.entered_by = entered_by;
	}
	public BillingAddress getBilling_address() {
		return billing_address;
	}
	public void setBilling_address(BillingAddress billing_address) {
		this.billing_address = billing_address;
	}
	public ForteCard getCard() {
		return card;
	}
	public void setCard(ForteCard card) {
		this.card = card;
	}
	public ForteResponseObject getResponse() {
		return response;
	}
	public void setResponse(ForteResponseObject response) {
		this.response = response;
	}
	public BitacoraForte getBitacoraResponse() {
		return bitacoraResponse;
	}
	public void setBitacoraResponse(BitacoraForte bitacoraResponse) {
		this.bitacoraResponse = bitacoraResponse;
	}
	
}
