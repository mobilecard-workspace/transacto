package com.addcel.mx.antad.servicios.model.vo;

public class DatosCorreoVO {
	private String descServicio;
	private String imgServicio;
	private String email;
	private String noAutorizacion;
	private Double monto;
	private Double comision;
	private String referenciaBanco;
	private String referenciaSat;
	private String fecha;
	private long idBitacora;
	private String ticket;
	private byte[] imagenPago;
	private String referenciaServicio;
	private String folioXcd;
	private double importe;
	
	private String nombre;
	private String usuario;
	private String autorizacionIave;
	private String autorizacion;
	private String tagIave;
	private double cargo;
	private String referencia;
	private String concepto;
	
	public byte[] getImagenPago() {
		return imagenPago;
	}
	public void setImagenPago(byte[] imagenPago) {
		this.imagenPago = imagenPago;
	}
	public String getReferenciaSat() {
		return referenciaSat;
	}
	public void setReferenciaSat(String referenciaSat) {
		this.referenciaSat = referenciaSat;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getImgServicio() {
		return imgServicio;
	}
	public void setImgServicio(String imgServicio) {
		this.imgServicio = imgServicio;
	}
	public String getDescServicio() {
		return descServicio;
	}
	public void setDescServicio(String descServicio) {
		this.descServicio = descServicio;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Double getComision() {
		return comision;
	}
	public void setComision(Double comision) {
		this.comision = comision;
	}
	public String getReferenciaBanco() {
		return referenciaBanco;
	}
	public void setReferenciaBanco(String referenciaBanco) {
		this.referenciaBanco = referenciaBanco;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getNoAutorizacion() {
		return noAutorizacion;
	}
	public void setNoAutorizacion(String noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
	}
	public String getReferenciaServicio() {
		return referenciaServicio;
	}
	public void setReferenciaServicio(String referenciaServicio) {
		this.referenciaServicio = referenciaServicio;
	}
	public String getFolioXcd() {
		return folioXcd;
	}
	public void setFolioXcd(String folioXcd) {
		this.folioXcd = folioXcd;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getAutorizacionIave() {
		return autorizacionIave;
	}
	public void setAutorizacionIave(String autorizacionIave) {
		this.autorizacionIave = autorizacionIave;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getTagIave() {
		return tagIave;
	}
	public void setTagIave(String tagIave) {
		this.tagIave = tagIave;
	}
	public double getCargo() {
		return cargo;
	}
	public void setCargo(double cargo) {
		this.cargo = cargo;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	
	
}
