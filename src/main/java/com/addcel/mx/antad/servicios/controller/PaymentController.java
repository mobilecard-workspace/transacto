package com.addcel.mx.antad.servicios.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.addcel.mx.antad.servicios.model.vo.BaseResponse;
import com.addcel.mx.antad.servicios.model.vo.BillPocketResponse;
import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.services.BillPocketService;
import com.addcel.mx.antad.servicios.services.Claro360Service;
import com.addcel.mx.antad.servicios.services.ValidationService;

@Controller
public class PaymentController {
	
	private static final String PROCESA_PAGO = "{idApp}/{idPais}/{idioma}/{accountId}/pagoBP";
	
	private static final String APROVISIONA_PAGO = "{idApp}/{idPais}/{idioma}/aprovisionamiento";
	
	private static final String CONSULTA_CLARO_PAGO = "{idApp}/{idPais}/{idioma}/consulta";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);
	
	@Autowired
	private BillPocketService service;
	
	@Autowired
	private ValidationService validateService;
	
	@Autowired
	private Claro360Service claroService;
	
	@RequestMapping(value = PROCESA_PAGO, method=RequestMethod.POST)
	public ResponseEntity<Object> paymentBillPocket(@PathVariable Integer idApp, @PathVariable Integer idPais, @PathVariable String idioma,
			@PathVariable String accountId, @RequestBody MobilePaymentRequestVO data, HttpServletRequest req) {
		ResponseEntity<Object> response = null;
		BillPocketResponse authorization = new BillPocketResponse();
		try {
			LOGGER.info("PROCESANDO PAGO PAIS - "+idPais+", idioma: "+idioma+", accountID: "+accountId);
			if(service.validaToken(idApp, req.getHeader("Authorization"), req.getRemoteAddr(), req.getSession().getId(), data, accountId)) {
				authorization = service.processPayment(idApp, idPais, idioma, data);
				/*
				 * LOGGER.info("VALIDANDO USUARIO WHITE LIST - {}", data.getIdUser());
				 * if(service.isUserWhiteList(data.getIdUser())) { authorization =
				 * service.processPayment(idApp, idPais, idioma, data); } else {
				 * authorization.setCode(-15000);
				 * authorization.setMessage("Servicio no disponible. Disculpe las molestias. ");
				 * }
				 */
//				if(data.getImei().equals("352153104943221") || data.getImei().equals("869426030883174")) {
//					authorization = service.processPayment(idApp, idPais, idioma, data);
//				} else {
//					if(idPais == 3) {
//						authorization = telefonicaService.proccessPayment(data, "", req, accountId);
//					} else {
//						authorization.setCode(-15000);
//						authorization.setMessage("Servicio no disponible. Disculpe las molestias. ");
//					}
//				}
				response = new ResponseEntity<>(authorization, HttpStatus.OK);
			} else {
				BaseResponse error = new BaseResponse();
	            error.setCode(-10);
	            error.setMessage("No esta autorizado para realizar esta operacion.");
	            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+ e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
            e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = APROVISIONA_PAGO, method=RequestMethod.POST)
	public ResponseEntity<Object> paymentBillPocket(@PathVariable Integer idApp, @PathVariable Integer idPais, @PathVariable String idioma,
			@RequestParam("json") String data, HttpServletRequest req) {
		ResponseEntity<Object> response = null;
		BillPocketResponse authorization = null;
		try {
			if(validateService.validaAccesoApplication(idApp)) {
				if(req.getHeader("Authorization") != null) {
					String token = req.getHeader("Authorization");
					if("ff39a0df-9d11-4c10-85a0-cfd16c67e518".equals(token)) {
						authorization = claroService.aprovisiona(idApp, idPais, idioma, data);
						response = new ResponseEntity<>(authorization, HttpStatus.OK);
					} else {
						BaseResponse error = new BaseResponse();
			            error.setCode(-10);
			            error.setMessage("No esta autorizado para realizar esta operacion.");
			            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
					}
				} else {
					BaseResponse error = new BaseResponse();
		            error.setCode(-10);
		            error.setMessage("No esta autorizado para realizar esta operacion.");
		            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
				}
			} else {
				BaseResponse error = new BaseResponse();
				error.setCode(-10);
	            error.setMessage("No estas autorizado para realizar esta operacion.");
	            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+ e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
            e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = CONSULTA_CLARO_PAGO, method=RequestMethod.POST)
	public ResponseEntity<Object> consultaSaldos(@PathVariable Integer idApp, @PathVariable Integer idPais, @PathVariable String idioma,
			@RequestParam("json") String data, HttpServletRequest req) {
		ResponseEntity<Object> response = null;
		BillPocketResponse authorization = null;
		try {
			if(validateService.validaAccesoApplication(idApp)) {
				if(req.getHeader("Authorization") != null) {
					String token = req.getHeader("Authorization");
					if("ff39a0df-9d11-4c10-85a0-cfd16c67e518".equals(token)) {
						authorization = claroService.aprovisiona(idApp, idPais, idioma, data);
						response = new ResponseEntity<>(authorization, HttpStatus.OK);
					} else {
						BaseResponse error = new BaseResponse();
			            error.setCode(-10);
			            error.setMessage("No esta autorizado para realizar esta operacion.");
			            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
					}
				} else {
					BaseResponse error = new BaseResponse();
		            error.setCode(-10);
		            error.setMessage("No esta autorizado para realizar esta operacion.");
		            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
				}
			} else {
				BaseResponse error = new BaseResponse();
				error.setCode(-10);
	            error.setMessage("No estas autorizado para realizar esta operacion.");
	            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			BaseResponse error = new BaseResponse();
            error.setCode(-10);
            error.setMessage("ERROR: "+ e.getLocalizedMessage());
            response = new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
            e.printStackTrace();
		}
		return response;
	}
	
}
