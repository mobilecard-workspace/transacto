package com.addcel.mx.antad.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DataPagoMovilesVO extends MobilePaymentRequestVO{
	private String fechaPago;
	private String autho;
	private String referenciaPW;
	private String authoCan;
	private String referenciaCanPW;
	private String xdi;
	private String cavv;
	private String eci;
	private String error;
	
	public DataPagoMovilesVO() {	
	}
	
	public DataPagoMovilesVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getAutho() {
		return autho;
	}

	public void setAutho(String autho) {
		this.autho = autho;
	}

	public String getReferenciaPW() {
		return referenciaPW;
	}

	public void setReferenciaPW(String referenciaPW) {
		this.referenciaPW = referenciaPW;
	}

	public String getAuthoCan() {
		return authoCan;
	}

	public void setAuthoCan(String authoCan) {
		this.authoCan = authoCan;
	}

	public String getReferenciaCanPW() {
		return referenciaCanPW;
	}

	public void setReferenciaCanPW(String referenciaCanPW) {
		this.referenciaCanPW = referenciaCanPW;
	}

	public String getXdi() {
		return xdi;
	}

	public void setXdi(String xdi) {
		this.xdi = xdi;
	}

	public String getCavv() {
		return cavv;
	}

	public void setCavv(String cavv) {
		this.cavv = cavv;
	}

	public String getEci() {
		return eci;
	}

	public void setEci(String eci) {
		this.eci = eci;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
		
}
