package com.addcel.mx.antad.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AbstractVO {

	private int idError;
	
	private String mensajeError;

	public AbstractVO() {
	}
	
	public AbstractVO(int idError, String mensajeError) {
		this.idError = idError;
		this.mensajeError = mensajeError;
	}
	
	
	
	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	
}
