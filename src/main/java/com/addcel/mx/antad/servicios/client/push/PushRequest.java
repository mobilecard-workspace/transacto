package com.addcel.mx.antad.servicios.client.push;

import java.util.List;

public class PushRequest {

	private long modulo;
	private long id_usuario;
	private String tipoUsuario;
	private String idioma;
	private int idPais;
	private int idApp;
	private List<PushParams> params;
	
	public long getModulo() {
		return modulo;
	}
	public void setModulo(long modulo) {
		this.modulo = modulo;
	}
	public long getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(long id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	public String getIdioma() {
		return idioma;
	}
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public int getIdApp() {
		return idApp;
	}
	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}
	public List<PushParams> getParams() {
		return params;
	}
	public void setParams(List<PushParams> params) {
		this.params = params;
	}
}
