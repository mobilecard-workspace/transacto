package com.addcel.mx.antad.servicios.services;

import java.io.File;

import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.addcel.mx.antad.servicios.model.vo.AntadInfo;
import com.addcel.mx.antad.servicios.model.vo.CorreoVO;

@Service
public class MailSenderService {
	private static final Logger logger = LoggerFactory.getLogger(MailSenderService.class);
		
	@Autowired
	private JavaMailSenderImpl mailSender;

	@Async
	public void enviarCorreo(CorreoVO correo) {
		if (correo.getTo() != null) {
			// plantilla para el env�o de email
			final MimeMessage message = mailSender.createMimeMessage();
			FileSystemResource res = null;
			ByteArrayDataSource bads=null;
			try {
				// el flag a true indica que va a ser multipart
				final MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
				helper.setTo(correo.getTo());
				helper.setSubject(correo.getSubject());
				if(correo.getFrom() == null){
					helper.setFrom("no-reply@addcel.com");
				}else{
					helper.setFrom(correo.getFrom());
				}
				if (correo.getBcc() != null) {
					helper.setBcc(correo.getBcc());
				}
				if(correo.getCc() != null){
					helper.setCc(correo.getCc());
				}
				helper.setText(correo.getBody(), true);
				if(correo.getCid() != null){
					try{
						for(int i = 0 ; i < correo.getCid().length; i++){
							if(correo.getCid()[i] instanceof String){
								logger.info("CID: identifierCID" + (i<10? "0" +i: i) + ":"+ correo.getCid()[i]);
								res = new FileSystemResource(new File((String)correo.getCid()[i]));
								helper.addInline("identifierCID" + (i<10? "0" +i: i), res);
							}else if(correo.getCid()[i] instanceof byte[]){
								logger.info("CID: identifierCID" + (i<10? "0" +i: i) + ":Arreglo de bytes");
								bads = new ByteArrayDataSource((byte[])(correo.getCid()[i]),"image/jpeg");
								helper.addInline("identifierCID" + (i<10? "0" +i: i), bads);
							}
						}
					}catch(Exception e){
						logger.error("Ocurrio un error al adjuntar el CID", e);
					}
					
				} else {
					logger.info("No se tiene ningun CID");
				}
				
				if (correo.getAttachments() != null) {
					try{
						for (String urlFile : correo.getAttachments()) {
							logger.info("File: "+ urlFile);
							res = new FileSystemResource(urlFile);
							helper.addAttachment(res.getFilename(), res);
						}
					}catch(Exception e){
						logger.error("Ocurrio un error al adjuntar un archivo", e);
					}
					
				} else {
					logger.info("No se tiene ningun archivo para adjuntar");
				}
				mailSender.send(message);
				logger.info("Correo enviado exitosamente");
			} catch (Exception e) {
				logger.error("Error al enviar email", e);
			}			
		} else {
			logger.error("No se recibio ningun destinatario");
		}

	}
	
	@Async
	public void enviarCorreoTicket(String body, String recipients, AntadInfo pagoInfo) {
		final MimeMessage message = mailSender.createMimeMessage();
		try {
			final MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
			helper.setTo(recipients);
			helper.setSubject(pagoInfo.getConcepto());
			helper.setFrom("no-reply@addcel.com");
			helper.setText(body, true);
			mailSender.send(message);
			logger.info("Correo enviado exitosamente");
		} catch (Exception e) {
			logger.error("Error al enviar email", e);
		}			
	}
}
