package com.addcel.mx.antad.servicios.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.mx.antad.servicios.model.vo.CorreoVO;
import com.addcel.mx.antad.servicios.model.vo.DatosCorreoVO;
import com.google.gson.Gson;


public class AddCelGenericMail {
	
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String urlString = "http://localhost/MailSenderAddcel/enviaCorreoAddcel";
		
	private static final DecimalFormat df = new DecimalFormat("#,###,##0.00");
    	
	public static boolean sendMail(String subject,String body, String email) {
		String json = null;
		String from = null;
		try {
			CorreoVO correo = new CorreoVO();
			from = "no-reply@addcel.com";
			String[]to = {email};
			correo.setCc(new String[]{});
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			Gson gson = new Gson();
			json = gson.toJson(correo);
			sendMail(json);
			logger.info("Fin proceso de envio email");
			logger.debug("Correo enviado exitosamente: " + email);
			return true;
		} catch (Exception e) {
			logger.error("ERROR - SEND MAIL: " + e);
			return false;
		}
	}
		
	public static CorreoVO generatedMail(DatosCorreoVO datosCorreoVO, String body, String asunto){
		logger.info("Genera objeto para envio de mail: " + datosCorreoVO.getEmail());
		String json = null;
		CorreoVO correo = new CorreoVO();
		try{
			logger.info("1");
			body = body.replaceAll("<#NOMBRE#>", datosCorreoVO.getNombre());
			logger.info("2");
			body = body.replaceAll("<#CONCEPTO#>",datosCorreoVO.getConcepto());
			logger.info("3");
			body = body.replaceAll("<#PRODUCTO#>", "Pago de servicios "+datosCorreoVO.getDescServicio());
			logger.info("4");
			body = body.replaceAll("<#FECHA#>", datosCorreoVO.getFecha());
			logger.info("5");
			body = body.replaceAll("<#REFE#>", datosCorreoVO.getReferenciaServicio());
			logger.info("6");
			body = body.replaceAll("<#FOLIO#>", String.valueOf(datosCorreoVO.getIdBitacora()));
			logger.info("7");
			body = body.replaceAll("<#AUTBAN#>", datosCorreoVO.getNoAutorizacion()!= null ? datosCorreoVO.getNoAutorizacion() : "");
			logger.info("8");
			body = body.replaceAll("<#FOLIOXCD#>", datosCorreoVO.getFolioXcd());
			logger.info("9");
			body = body.replaceAll("<#IMPORTE#>", "\\$ "+df.format(datosCorreoVO.getImporte()));
			logger.info("10");
			body = body.replaceAll("<#COMISION#>", "\\$ "+df.format(datosCorreoVO.getComision()));
			logger.info("11");
			body = body.replaceAll("<#MONTO#>", "\\$ "+df.format(datosCorreoVO.getMonto()));
			logger.info("12");
			body = body.replaceAll("<#MONEDA#>", "MXN");
			logger.info("13");
			body = body.replaceAll("<#SUCU#>", "Mobilecard");
			logger.info("14");
			body = body.replaceAll("<#CAJA#>", "1");
			logger.info("15");
			body = body.replaceAll("<#CAJERO#>", "Autoservicio");
			logger.info("16");
			body = body.replaceAll("<#FORMAPAGO#>", "Tarjeta de Credito");
			logger.info("17");
			body = body.replaceAll("<#TICKET#>",datosCorreoVO.getTicket());
			logger.info("18");
			String from = "no-reply@addcel.com";
			logger.info("19");
			String subject = asunto + datosCorreoVO.getDescServicio() + " - ReferenciaMC: " + datosCorreoVO.getReferenciaServicio();
			logger.info("20");
			String[] to = {datosCorreoVO.getEmail()};
			logger.info("21");
			correo.setCc(new String[]{});
			logger.info("22");
			correo.setBody(body);
			logger.info("23");
			correo.setFrom(from);
			logger.info("24");
			correo.setSubject(subject);
			logger.info("25");
			correo.setTo(to);
			logger.info("26");
			Gson gson = new Gson();
			logger.info("27");
			json = gson.toJson(correo);
			logger.info("28");
			sendMail(json);
			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
			e.printStackTrace();
		}
		return correo;
	}

	public static CorreoVO generatedMailCommons(DatosCorreoVO datosCorreoVO,
			String body, Properties props, String from, String host, int port,
			String username, String password) {
		logger.info("Genera objeto para envio de mail: "
				+ datosCorreoVO.getEmail());
		String json = null;
		CorreoVO correo = new CorreoVO();
		try {
			body = body.replaceAll("<#NOMBRE#>", datosCorreoVO.getNombre());
			body = body.replaceAll("<#CONCEPTO#>", datosCorreoVO.getConcepto());
			body = body.replaceAll("<#PRODUCTO#>", "Pago de servicios "
					+ datosCorreoVO.getDescServicio());
			body = body.replaceAll("<#FECHA#>", datosCorreoVO.getFecha());
			body = body.replaceAll("<#REFE#>",
					datosCorreoVO.getReferenciaServicio());
			body = body.replaceAll("<#FOLIO#>",
					String.valueOf(datosCorreoVO.getIdBitacora()));
			body = body.replaceAll(
					"<#AUTBAN#>",
					datosCorreoVO.getNoAutorizacion() != null ? datosCorreoVO
							.getNoAutorizacion() : "");
			body = body.replaceAll("<#FOLIOXCD#>", datosCorreoVO.getFolioXcd());
			body = body.replaceAll("<#IMPORTE#>",
					"\\$ " + df.format(datosCorreoVO.getImporte()));
			body = body.replaceAll("<#COMISION#>",
					"\\$ " + df.format(datosCorreoVO.getComision()));
			body = body.replaceAll("<#MONTO#>",
					"\\$ " + df.format(datosCorreoVO.getMonto()));
			body = body.replaceAll("<#MONEDA#>", "MXN");

			body = body.replaceAll("<#SUCU#>", "Mobilecard");
			body = body.replaceAll("<#CAJA#>", "1");
			body = body.replaceAll("<#CAJERO#>", "Autoservicio");
			body = body.replaceAll("<#FORMAPAGO#>", "Tarjeta de Credito");

			body = body.replaceAll("<#TICKET#>", datosCorreoVO.getTicket());

			String subject = ("Acuse de pago de ")
					+ datosCorreoVO.getDescServicio() + " - ReferenciaMC: "
					+ datosCorreoVO.getReferenciaServicio();
			Utils.sendMail(datosCorreoVO.getEmail(), subject, body, props, from, host, port, username, password);
			logger.info("Fin proceso de envio email");
		} catch (Exception e) {
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
