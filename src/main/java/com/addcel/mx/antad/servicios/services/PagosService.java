package com.addcel.mx.antad.servicios.services;

import static com.addcel.mx.antad.servicios.utils.Constantes.JSON_ERROR_INESPERADO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_SERVICE;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.mx.antad.servicios.client.antad.ObjectFactory;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacion;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacionResponse;
import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.mx.antad.servicios.model.vo.AfiliacionVO;
import com.addcel.mx.antad.servicios.model.vo.AntadDetalle;
import com.addcel.mx.antad.servicios.model.vo.AntadInfo;
import com.addcel.mx.antad.servicios.model.vo.AprovisionamientoResponse;
import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.PagosDirectos;
import com.addcel.mx.antad.servicios.utils.Constantes;
import com.addcel.mx.antad.servicios.utils.Utils;
import com.addcel.mx.antad.servicios.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class PagosService {
	
	private static final Logger logger =  LoggerFactory.getLogger(PagosService.class);

	@Autowired
	private AntadServiciosMapper mapper;
	
	@Autowired
	private UtilsService jsonUtils;
	
	@Autowired
    @Qualifier("webServiceAntadBridge")
    private transient WebServiceTemplate webServiceTemplate;
	
	public ModelAndView datos3DSecurePayWorks(String json,  ModelMap modelo) {
		ModelAndView mav = null;
		HashMap<String, String> resp = new HashMap<String, String>();
		double total = 0;
		AfiliacionVO afiliacion = null;
		PagosDirectos pago = null;
		try{
			mav = new ModelAndView("pagos/envia3DBanorte");
			pago = (PagosDirectos) jsonUtils.jsonToObject(json, PagosDirectos.class);
			afiliacion = mapper.buscaAfiliacion("9");
			insertaBitacoras(pago);
			logger.info("DATOS ENVIADOS [URL: https://eps.banorte.com/secure3d/Solucion3DSecure.htm, ID BITACORA: "+pago.getIdTransaccion()
					+", AFILIACION: "+afiliacion.getIdAfiliacion()
					+", USUARIO AFILIACION: "+afiliacion.getUsuario()
					+", ID TERMINAL: "+afiliacion.getIdTerminal()
					+", MERCHANT ID: "+afiliacion.getMerchantId()
					+", URL_BACK: "+afiliacion.getForwardPath()
					+", NOMBRE: "+pago.getNombre()
					+", TARJETA: "+pago.getTarjeta()
					+", VIGENCIA: "+pago.getVigencia()
					+", TIPO TARJETA: "+pago.getTipoTarjeta()
					+", EMISOR: "+pago.getEmisor()
					+", ID CLIENTE: "+pago.getIdCliente()
					+", TIPO APP: "+pago.getTipoApp()
					+"]");
			resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
			resp.put("USUARIO", afiliacion.getUsuario());
			resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
			resp.put("CMD_TRANS", "VENTA");
			resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
			resp.put("Reference3D", pago.getIdTransaccion() + "");
			resp.put("MerchantId", afiliacion.getMerchantId());
			resp.put("ForwardPath", afiliacion.getForwardPath());
			resp.put("Expires", pago.getVigencia());
			total = pago.getMonto() + pago.getComision();
			total = 1;
			resp.put("Total", Utils.formatoMontoPayworks(total + ""));
			resp.put("Card", pago.getTarjeta());
			resp.put("CardType", pago.getTipoTarjeta());
				
			mav.addObject("prosa", resp);
			mav.addObject("pagoRequest", pago);
		}catch(Exception e){
			logger.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav = new ModelAndView("pagos/resultadoPago");
			mav.addObject("autorizacion", "0");
			mav.addObject("status", "500");
			mav.addObject("total", 1);
			mav.addObject("idBitacora", "0");
			mav.addObject("access_token", pago.getToken());
			mav.addObject("mensaje", "El pago ha sido rechazado. Motivos: "+e.getLocalizedMessage());
		}
		return mav;
	}


	public ModelAndView respuesta3D(String cadena, ModelMap modelo) {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		ModelAndView mav = null;
		AfiliacionVO afiliacion = null;
		PagosDirectos pago = null;
		try {
			cadena = cadena.replace(" ", "+");
			logger.debug("Respuesta de 3D Secure: " + cadena);
			String[] spl = cadena.split("&");
			String[] data = null;
			for(String cad: spl){
				logger.debug("data:  " + cad);
				data = cad.split("=");
				resp.put(data[0], data.length >= 2? data[1]: "");
			}
			pago = mapper.getPago((String) resp.get("Reference3D"));
			if(resp.containsKey("Status") && "200".equals(resp.get("Status"))){
				mav = new ModelAndView("pagos/enviaPayworks");
				afiliacion = mapper.buscaAfiliacion("9");
				resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
				resp.put("USUARIO", afiliacion.getUsuario());
				resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
				resp.put("CMD_TRANS", "VENTA");
				resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
				resp.put("Total", Utils.formatoMontoPayworks((String)resp.get("Total")));
				resp.put("MODO", "PRD");
				resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
				resp.put("NUMERO_TARJETA", resp.get("Number"));
				resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
				resp.put("CODIGO_SEGURIDAD", pago.getCvv2());
				resp.put("MODO_ENTRADA", "MANUAL");
				resp.put("URL_RESPUESTA", afiliacion.getForwardPathPayworks());
				resp.put("IDIOMA_RESPUESTA", "ES");
				logger.info("DATOS ENVIADO A PAYWORK2: "+resp.toString());
				pago.setResp3DStatus((String) resp.get("Status"));
				pago.setTarjeta((String) resp.get("CardType"));
				mapper.actualizaPagoDirecto3D(pago);
				mav.addObject("prosa", resp);
			} else {
				mav = new ModelAndView("pagos/resultadoPago");
				mav.addObject("autorizacion", "0");
				mav.addObject("status", "500");
				mav.addObject("total", 1);
				mav.addObject("idBitacora", resp.get("Reference3D"));
				mav.addObject("access_token", pago.getToken());
				mav.addObject("mensaje", "El pago ha sido rechazado. Motivos: "+Constantes.errorPayW.get(resp.get("Status")));
			}
		} catch (Exception e) {
			e.printStackTrace();
			mav = new ModelAndView("pagos/resultadoPago");
			mav.addObject("autorizacion", "0");
			mav.addObject("status", "500");
			mav.addObject("total", 1);
			mav.addObject("idBitacora", resp.get("Reference3D"));
			mav.addObject("access_token", pago.getToken());
			mav.addObject("mensaje", "El pago ha sido rechazado. Motivos: "+e.getLocalizedMessage());
		}
		return mav;
	}

	public ModelAndView respuestaPayworks(String NUMERO_CONTROL,
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String CODIGO_PAYW, String RESULTADO_AUT, String BANCO_EMISOR,
			String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA,
			ModelMap modelo, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("pagos/resultadoPago");
		PagosDirectos pago = null;
		double total = 0;
		try {
			logger.info("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+" REFERENCIA: " + REFERENCIA
					+" FECHA_RSP_CTE: " + FECHA_RSP_CTE+" TEXTO: " + TEXTO+" RESULTADO_AUT: " + RESULTADO_AUT
					+" RESULTADO_PAYW: " + RESULTADO_PAYW+" FECHA_REQ_CTE: " + FECHA_REQ_CTE+" CODIGO_AUT: " + CODIGO_AUT
					+" CODIGO_PAYW: " + CODIGO_PAYW+" BANCO_EMISOR: " + BANCO_EMISOR+" TIPO_TARJETA: " + TIPO_TARJETA
					+" MARCA_TARJETA: " + MARCA_TARJETA);	
			pago = mapper.getPago(NUMERO_CONTROL);
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
//				mapper.actualizaPagoDirecto(NUMERO_CONTROL, RESULTADO_PAYW, TEXTO, CODIGO_AUT, CODIGO_PAYW, BANCO_EMISOR);
				mapper.actualizaPagoDirectoPayworks(pago);
				AntadInfo pagoInfo = mapper.buscarDetallePago(NUMERO_CONTROL);
//				AprovisionamientoResponse respuesta = notificacionAntad(pagoInfo);
//				total = pago.getMonto() + pago.getComision();
//				if(respuesta != null && respuesta.getIdError() == 0){
					mav.addObject("autorizacion", CODIGO_AUT);
					mav.addObject("status", "200");
					mav.addObject("total", 1);
					mav.addObject("idBitacora", NUMERO_CONTROL);
					mav.addObject("access_token", pago.getToken());
//				} else {
//					mav = new ModelAndView("payworks/exito");
//					mav.addObject("mensajeError", "Su transaccion no ha sido completada exitosamente. ERROR: "+respuesta.getMensajeAntad());
//				}
			} else {
				mav.addObject("autorizacion", "0");
				mav.addObject("status", "500");
				mav.addObject("total", 1);
				mav.addObject("idBitacora", NUMERO_CONTROL);
				mav.addObject("access_token", pago.getToken());
				mav.addObject("mensaje", "El pago ha sido rechazado Codigo Error: "+CODIGO_PAYW +"Motivos: "+TEXTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			mav.addObject("autorizacion", "0");
			mav.addObject("status", "500");
			mav.addObject("total", 1);
			mav.addObject("idBitacora", NUMERO_CONTROL);
			mav.addObject("access_token", pago.getToken());
			mav.addObject("mensaje", "El pago ha sido rechazado Codigo Error: "+CODIGO_PAYW +"Motivos: "+e.getLocalizedMessage());
		}
		return mav;
	}
	
	private AprovisionamientoResponse notificacionAntad(AntadInfo pagoInfo) {
		ProcesaAutorizacion autorizacion = null;
		ProcesaAutorizacionResponse response = null;
		ObjectFactory factory = new ObjectFactory();
		AprovisionamientoResponse respuesta = null;
		String json = null;
		try {
			json = jsonUtils.objectToJson(pagoInfo);
			json = Utils.encryptJson(json);
			autorizacion = new ProcesaAutorizacion();
			autorizacion.setJson(json);
			response = (ProcesaAutorizacionResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createProcesaAutorizacion(autorizacion));
			json = Utils.decryptJson(response.getReturn());
			respuesta = (AprovisionamientoResponse) jsonUtils.jsonToObject(json, AprovisionamientoResponse.class);
		} catch (Exception e) {
			logger.error(LOG_SERVICE+LOG_ERROR_PROCESA_PAGO+" ["+e.getCause()+"]");
			
			json = JSON_ERROR_INESPERADO;
		} finally{
			logger.info(LOG_RESPUESTA_PROCESA_PAGO+json);
		}
		return respuesta;
	}


	private void insertaBitacoras(PagosDirectos pago) throws Exception{
		BitacoraVO tb = null;
		AntadDetalle antadDetalle = new AntadDetalle();
		try {
			pago.setTarjeta(AddcelCrypto.encryptTarjeta(pago.getTarjeta()));
			tb = new BitacoraVO(pago.getIdApp(), Long.valueOf(1), ""+pago.getConcepto() + " "+pago.getTipoTarjeta(), 
					("Pago directo"), 
					"Referencia: " + pago.getReferencia() + ", Importe: " + pago.getMonto(), 
					pago.getTarjeta(), null, 
					null, null, null, 
					pago.getMonto(), pago.getIdioma());
			tb.setIdProveedor(53);
			mapper.insertaBitacoraTransaccion(tb);
			pago.setIdTransaccion(tb.getIdBitacora());
			pago.setTarjeta(AddcelCrypto.decryptTarjeta(pago.getTarjeta()));
			mapper.insertaPagoDirecto(pago);
			antadDetalle.setDescripcion(pago.getConcepto());
			antadDetalle.setEmisor(pago.getEmisor());
			antadDetalle.setIdTransaccion(pago.getIdTransaccion());
			antadDetalle.setReferencia(pago.getReferencia());
			antadDetalle.setTotal(pago.getMonto());
			antadDetalle.setCodigoRespuesta("");
			mapper.insertaAntadDetalle(antadDetalle);
		} catch (Exception e) {
			logger.error("Error al guardar en bitacoras,  id_bitacora: {}", tb.getIdBitacora(), e);
			throw new Exception(e);
		}
	}

}