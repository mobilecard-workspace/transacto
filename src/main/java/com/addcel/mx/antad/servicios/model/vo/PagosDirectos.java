package com.addcel.mx.antad.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PagosDirectos {

	private String tarjeta;
	
	private String cvv2;
	
	private String vigencia;
	
	private String concepto;
	
	private String nombre;
	
	private long idTransaccion;
	
	private double monto;
	
	private double comision;
		
	private String referencia;

	private String resp3DStatus;
	
	private String tipoTarjeta;
	
	private String autorizacionPayworks;
	
	private String textoPayworks;
	
	private String adicionales;
	
	private int idCliente;
	
	private String tipoApp;
	
	private String emisor;
	
	private String token;
	
	private int idApp;
	
	private String idioma;
	
	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getResp3DStatus() {
		return resp3DStatus;
	}

	public void setResp3DStatus(String resp3dStatus) {
		resp3DStatus = resp3dStatus;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public String getAutorizacionPayworks() {
		return autorizacionPayworks;
	}

	public void setAutorizacionPayworks(String autorizacionPayworks) {
		this.autorizacionPayworks = autorizacionPayworks;
	}

	public String getTextoPayworks() {
		return textoPayworks;
	}

	public void setTextoPayworks(String textoPayworks) {
		this.textoPayworks = textoPayworks;
	}

	public String getAdicionales() {
		return adicionales;
	}

	public void setAdicionales(String adicionales) {
		this.adicionales = adicionales;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public String getTipoApp() {
		return tipoApp;
	}

	public void setTipoApp(String tipoApp) {
		this.tipoApp = tipoApp;
	}

	public String getEmisor() {
		return emisor;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getIdApp() {
		return idApp;
	}

	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	
}
