package com.addcel.mx.antad.servicios.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UtilsService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsService.class);
	
	@Autowired
	private ObjectMapper mapperJk;	
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json=mapperJk.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			LOGGER.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			LOGGER.error("2.- Error al mapperar objeccto: {}",e);
		} catch (IOException e) {
			LOGGER.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (JsonParseException e) {
			LOGGER.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			LOGGER.error("Error al parsear cadena json 3: {}", e);
		}
		return obj;
	}
	
	private static final String patron_large = "yyyy/MM/dd HH:mm:ss";
	
	private static final SimpleDateFormat formato_large = new SimpleDateFormat(patron_large, new Locale("ES", "co"));
	
	public static String getFechaActual(){
		String resp = null;
		try{
			resp = formato_large.format(new Date());
		}catch(Exception e){
			LOGGER.error("Error en getFechaActual: {}",e );
		}
		return resp;
	}
	
}
