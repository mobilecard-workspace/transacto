package com.addcel.mx.antad.servicios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.mx.antad.servicios.client.push.PushClient;
import com.addcel.mx.antad.servicios.client.push.PushRequest;
import com.addcel.mx.antad.servicios.client.push.PushResponse;
import com.google.gson.Gson;

@Service
public class PushService implements PushClient{

	private static final String PATH_PUSH = "http://localhost/PushNotifications/sendMessage";
	private static final String AUTH = "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx";
	Gson GSON = new Gson();
	private static final Logger LOG = LoggerFactory.getLogger(PushService.class);

	@Override
	public PushResponse sendNotification(PushRequest request) {
		PushResponse response = null;
		LOG.debug("Enviando notificacion push");
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.add("AUTHORIZATION", AUTH);
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<PushRequest> requestEntity = new HttpEntity<>(request,headers);
	        ResponseEntity<String> respPush = restTemplate.exchange(PATH_PUSH, HttpMethod.POST, requestEntity, String.class);
	        
	        LOG.debug("Respuesta del servicio push: "+respPush);
	        response = GSON.fromJson(respPush.getBody(), PushResponse.class);
		} catch (Exception e) {
			LOG.error("Error al enviar la notificacion al usuario: "+e.getMessage());
		}
		return response;
	}
}
