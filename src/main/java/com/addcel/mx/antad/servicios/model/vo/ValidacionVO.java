package com.addcel.mx.antad.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ValidacionVO {

	private int idError;
	
	private String mensajeError;
	
	private String transaccionesPermitidas;
	
	private String numeroTransacciones;
	
	private String idUsuario;

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getTransaccionesPermitidas() {
		return transaccionesPermitidas;
	}

	public void setTransaccionesPermitidas(String transaccionesPermitidas) {
		this.transaccionesPermitidas = transaccionesPermitidas;
	}

	public String getNumeroTransacciones() {
		return numeroTransacciones;
	}

	public void setNumeroTransacciones(String numeroTransacciones) {
		this.numeroTransacciones = numeroTransacciones;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	
}