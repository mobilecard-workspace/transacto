package com.addcel.mx.antad.servicios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;

@Service
public class ValidationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationService.class);
	
	@Autowired
	private AntadServiciosMapper mapper;
		
	
	public boolean validaAccesoApplication(Integer idApp) {
		Integer valida = null;
		boolean resp = false;
		try {
			valida = mapper.validaAcceso(idApp);
			if(valida != 0) {
				resp = true;
			}
			LOGGER.info("VALIDATOR - VALIDA ACCESO ID APP - {} - ACESSO - {}", idApp, valida);
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar Reglas de Negocio.", e);
		}
		return resp;
	}
	
}
