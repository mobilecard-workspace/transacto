package com.addcel.mx.antad.servicios.model.vo;

import java.io.Serializable;

public class CorreoVO implements Serializable{

	private static final long serialVersionUID = -872632796290271188L;
	
	private String[] to;
	private String[] bcc;
	private String[] cc;
	private String[] attachments;
	private Object[] cid;
	private String from;
	private String subject; 
	private String body;
	public String[] getTo() {
		return to;
	}
	public void setTo(String[] to) {
		this.to = to;
	}
	public String[] getBcc() {
		return bcc;
	}
	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}
	public String[] getCc() {
		return cc;
	}
	public void setCc(String[] cc) {
		this.cc = cc;
	}	
	public String[] getAttachments() {
		return attachments;
	}
	public void setAttachments(String[] attachments) {
		this.attachments = attachments;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Object[] getCid() {
		return cid;
	}
	public void setCid(Object[] cid) {
		this.cid = cid;
	}		
}
