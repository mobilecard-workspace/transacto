package com.addcel.mx.antad.servicios.utils;

import static com.addcel.mx.antad.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;

import com.addcel.utils.AddcelCrypto;

public class Utils {

	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);
	
	private static final String URL_HEADER_ADDCEL = "/usr/java/resources/images/Addcel/MobileCard_Antad_header_600.PNG";
			
	public Utils() {
		
	}
	
	
	
	public static String digest(String text) {
		String digest = "";
		BigInteger bigIntDgst = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(text.getBytes(), 0, text.length());
			bigIntDgst = new BigInteger(1, md.digest());
			digest = bigIntDgst.toString(16);
			digest = String.format("%040x", bigIntDgst);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.info("Error al encriptar - digest", e);
		}
		return digest;
	}
	
	public static String getFecha(String formato){
        String dateString = null;
        SimpleDateFormat format = new SimpleDateFormat(formato);
        try {
        	Date now = new Date();
        	dateString = format.format(now);
        }
        catch(Exception pe) {
            System.out.println("ERROR: Cannot parse \"" + dateString + "\"");
        }
		return dateString;
	}
	
	public static String encryptJson(String json){
		return AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
	}
	
	public static String decryptJson(String json){
		return AddcelCrypto.decryptSensitive(json);
	}
	
	public static String decryptJson(String json, String key){
		String decryptedText = null;
		String salt = null;
		try {
			salt = "ff39a0df";
			LOGGER.info("DATA: "+json);
			LOGGER.info("KEY: "+key);
			LOGGER.info("SALT: "+salt);
//			TextEncryptor decryptor = Encryptors.text(key, salt);
//			decryptedText = decryptor.decrypt(json);
			TextEncryptor decryptor = Encryptors.text(key, salt);
	        decryptedText = decryptor.decrypt(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decryptedText;
	}

	public static String encryptHard(String json){
		return AddcelCrypto.encryptHard(json);
	}
	
	public static String formatoMontoPayworks(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+ "." + centavos;
        }else{
            varTotal=monto.concat(".00");
        } 
		return varTotal;		
	}
	
	public static String cambioAcento(String cadena){
		try{
			if(cadena != null){
				cadena = cadena.replaceAll("Ã¡", "á");
				cadena = cadena.replaceAll("Ã³", "ó");
			}
		}catch(Exception e){
			LOGGER.error("Error al cambiar acentos: " + e.getMessage());
		}
		return cadena;
	}
	
	public static boolean sendMail(String destinatario, String asunto,
			String cuerpo, Properties props, String from, String host, int port, String username, String password) {
		try {
			Session session = Session.getInstance(props);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(destinatario));
			message.setSubject(asunto);
			if (cuerpo.indexOf("cid:identifierCID00") > 0) {
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpo, "text/html");
				MimeMultipart multipart = new MimeMultipart("related");
				multipart.addBodyPart(messageBodyPart);
				messageBodyPart = new MimeBodyPart();
				DataSource fds = new FileDataSource(URL_HEADER_ADDCEL);
				messageBodyPart.setDataHandler(new DataHandler(fds));
				messageBodyPart.setHeader("Content-ID", "identifierCID00");
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
			} else {
				message.setContent(cuerpo, "text/html");
			}
			Transport transport = session.getTransport("smtp");
			transport.connect(host, port, username, password);
			message.saveChanges();
			transport.sendMessage(message, message.getAllRecipients());
			LOGGER.info("Correo enviado exitosamente: " + destinatario);
			return true;
		} catch (Exception e) {
			LOGGER.error("ERROR - SEND MAIL: " + e);
			return false;
		}
	}
	
	
	
}
