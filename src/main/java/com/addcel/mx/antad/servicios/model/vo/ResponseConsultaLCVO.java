package com.addcel.mx.antad.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ResponseConsultaLCVO extends AbstractVO {
	
	private String tranid;
    private String referen;
    private long fecha;
    private String hora;
	private int hora01;
    private int centro;
    private String referencia;
    private String llavpgo;
    private String usuario;
    private String estatus;
    private String plaza;
    private double cargo;
    private String banda;
    private int banco;
    
    public ResponseConsultaLCVO() {	
	}
    
    public ResponseConsultaLCVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}
    
	public String getReferen() {
		return referen;
	}
	public void setReferen(String referen) {
		this.referen = referen;
	}
	public long getFecha() {
		return fecha;
	}
	public void setFecha(long fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public int getHora01() {
		return hora01;
	}
	public void setHora01(int hora01) {
		this.hora01 = hora01;
	}
	public int getCentro() {
		return centro;
	}
	public void setCentro(int centro) {
		this.centro = centro;
	}
	public String getLlavpgo() {
		return llavpgo;
	}
	public void setLlavpgo(String llavpgo) {
		this.llavpgo = llavpgo;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getPlaza() {
		return plaza;
	}
	public void setPlaza(String plaza) {
		this.plaza = plaza;
	}
	public String getBanda() {
		return banda;
	}
	public void setBanda(String banda) {
		this.banda = banda;
	}
	public String getTranid() {
		return tranid;
	}
	public void setTranid(String tranid) {
		this.tranid = tranid;
	}

	public int getBanco() {
		return banco;
	}

	public void setBanco(int banco) {
		this.banco = banco;
	}
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public double getCargo() {
		return cargo;
	}

	public void setCargo(double cargo) {
		this.cargo = cargo;
	}
}
