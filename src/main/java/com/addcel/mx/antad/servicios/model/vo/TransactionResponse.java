package com.addcel.mx.antad.servicios.model.vo;

public class TransactionResponse {
        
    private String transaccion;
    
    private String seguridad;

    private String transactionId;
    
    private String bancoAdquirente;
    
    private String nombreComercio;
    
    private String direccionComercio;
    
    private String afiliacion;
    
    private String importe;
    
    private String moneda;
    
    private String fechaHora;
    
    private String claveOperacion;
    
    private String numeroCaja;
    
    private String producto;
    
    private String bancoEmisor;
    
    private String marca;
    
    private String autorizacion;
    
    private String poblacion;
    
    private String claveRechazo;
    
    private String descripcionRechazo;

	public String getTransaccion() {
		return transaccion;
	}

	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}

	public String getSeguridad() {
		return seguridad;
	}

	public void setSeguridad(String seguridad) {
		this.seguridad = seguridad;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getBancoAdquirente() {
		return bancoAdquirente;
	}

	public void setBancoAdquirente(String bancoAdquirente) {
		this.bancoAdquirente = bancoAdquirente;
	}

	public String getNombreComercio() {
		return nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}

	public String getDireccionComercio() {
		return direccionComercio;
	}

	public void setDireccionComercio(String direccionComercio) {
		this.direccionComercio = direccionComercio;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getClaveOperacion() {
		return claveOperacion;
	}

	public void setClaveOperacion(String claveOperacion) {
		this.claveOperacion = claveOperacion;
	}

	public String getNumeroCaja() {
		return numeroCaja;
	}

	public void setNumeroCaja(String numeroCaja) {
		this.numeroCaja = numeroCaja;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getBancoEmisor() {
		return bancoEmisor;
	}

	public void setBancoEmisor(String bancoEmisor) {
		this.bancoEmisor = bancoEmisor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public String getClaveRechazo() {
		return claveRechazo;
	}

	public void setClaveRechazo(String claveRechazo) {
		this.claveRechazo = claveRechazo;
	}

	public String getDescripcionRechazo() {
		return descripcionRechazo;
	}

	public void setDescripcionRechazo(String descripcionRechazo) {
		this.descripcionRechazo = descripcionRechazo;
	}
	
}
