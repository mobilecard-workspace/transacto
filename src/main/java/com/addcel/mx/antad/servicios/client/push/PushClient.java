package com.addcel.mx.antad.servicios.client.push;

public interface PushClient {

	PushResponse sendNotification(PushRequest request);
}
