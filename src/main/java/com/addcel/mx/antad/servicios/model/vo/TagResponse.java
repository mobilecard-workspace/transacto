package com.addcel.mx.antad.servicios.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TagResponse extends AbstractVO{
	
	  List<Tag> tags;
	  
	  public TagResponse() {
		// TODO Auto-generated constructor stub
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	  
	  
	
}
