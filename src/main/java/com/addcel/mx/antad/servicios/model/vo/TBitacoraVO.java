package com.addcel.mx.antad.servicios.model.vo;


public class TBitacoraVO {
	
	private long idBitacora;
	
	private String idUsuario;
	
	private int idProveedor;
	
	private int idProducto;
	
	private String bitFecha;
	
	private String bitHora;
	
	private String bitConcepto;
	
	private String  bitCargo;
	
	private String bitTicket;
	
	private String bitNoAutorizacion;
	
	private int bitCodigoError;
	
	private String bitCardId;
	
	private int bitStatus;
	
	private String imei;
	
	private String destino;
	
	private String tarjetaCompra;
	
	private String tipo;
	
	private String software;
	
	private String modelo;
	
	private String wkey;
	
	private int car_id;
	
	private double cargo;
	
	private String tarjeta_compra;
	
	private String concepto;
	
	private String afiliacion;
	
	private double comision;
	
	private String cx;
    
    private String cy;
    
    private String pin;
    
    private String folio;
    
    private String login;
    
    private String nombre;
    
    private String apellido;
    
    private String mail;
    
    private String TRX_NO;
    
    private String AUTONO;
    
    private String tarjetaIave;
    
    private String idTipoTarjeta;
    
	public TBitacoraVO(){}	
	
	public TBitacoraVO(String idUsuario, String bitConcepto, String imei,
			String destino, String tarjetaCompra, String tipo, String software,
			String modelo, String wkey, String bitCargo) {
		super();
		this.idUsuario = idUsuario;
		this.bitConcepto = bitConcepto;
		this.imei = imei;
		this.destino = destino;
		this.tarjetaCompra = tarjetaCompra;
		this.tipo = tipo;
		this.software = software;
		this.modelo = modelo;
		this.wkey = wkey;
		this.bitCargo = bitCargo;
	}
	public long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getBitFecha() {
		return bitFecha;
	}
	public void setBitFecha(String bitFecha) {
		this.bitFecha = bitFecha;
	}
	public String getBitHora() {
		return bitHora;
	}
	public void setBitHora(String bitHora) {
		this.bitHora = bitHora;
	}
	public String getBitConcepto() {
		return bitConcepto;
	}
	public void setBitConcepto(String bitConcepto) {
		this.bitConcepto = bitConcepto;
	}
	public String getBitCargo() {
		return bitCargo;
	}
	public void setBitCargo(String bitCargo) {
		this.bitCargo = bitCargo;
	}
	public String getBitTicket() {
		return bitTicket;
	}
	public void setBitTicket(String bitTicket) {
		this.bitTicket = bitTicket;
	}
	public String getBitNoAutorizacion() {
		return bitNoAutorizacion;
	}
	public void setBitNoAutorizacion(String bitNoAutorizacion) {
		this.bitNoAutorizacion = bitNoAutorizacion;
	}
	public int getBitCodigoError() {
		return bitCodigoError;
	}
	public void setBitCodigoError(int bitCodigoError) {
		this.bitCodigoError = bitCodigoError;
	}
	public String getBitCardId() {
		return bitCardId;
	}
	public void setBitCardId(String bitCardId) {
		this.bitCardId = bitCardId;
	}
	public int getBitStatus() {
		return bitStatus;
	}
	public void setBitStatus(int bitStatus) {
		this.bitStatus = bitStatus;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getTarjetaCompra() {
		return tarjetaCompra;
	}
	public void setTarjetaCompra(String tarjetaCompra) {
		this.tarjetaCompra = tarjetaCompra;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getWkey() {
		return wkey;
	}
	public void setWkey(String wkey) {
		this.wkey = wkey;
	}

	public int getCar_id() {
		return car_id;
	}

	public void setCar_id(int car_id) {
		this.car_id = car_id;
	}

	public double getCargo() {
		return cargo;
	}

	public void setCargo(double cargo) {
		this.cargo = cargo;
	}

	public String getTarjeta_compra() {
		return tarjeta_compra;
	}

	public void setTarjeta_compra(String tarjeta_compra) {
		this.tarjeta_compra = tarjeta_compra;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getCx() {
		return cx;
	}

	public void setCx(String cx) {
		this.cx = cx;
	}

	public String getCy() {
		return cy;
	}

	public void setCy(String cy) {
		this.cy = cy;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTRX_NO() {
		return TRX_NO;
	}

	public void setTRX_NO(String tRX_NO) {
		TRX_NO = tRX_NO;
	}

	public String getAUTONO() {
		return AUTONO;
	}

	public void setAUTONO(String aUTONO) {
		AUTONO = aUTONO;
	}

	public String getTarjetaIave() {
		return tarjetaIave;
	}

	public void setTarjetaIave(String tarjetaIave) {
		this.tarjetaIave = tarjetaIave;
	}

	public String getIdTipoTarjeta() {
		return idTipoTarjeta;
	}

	public void setIdTipoTarjeta(String idTipoTarjeta) {
		this.idTipoTarjeta = idTipoTarjeta;
	}	
	}
