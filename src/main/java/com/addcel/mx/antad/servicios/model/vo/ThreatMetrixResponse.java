package com.addcel.mx.antad.servicios.model.vo;

public class ThreatMetrixResponse extends BaseResponse {

	private String tmxStatus;

    private String tmxRisk;

    private Integer tmxScore;

    private TmxReasons[] tmxReasons;

    private String tmxError;

    public String getTmxStatus() {
        return tmxStatus;
    }

    public void setTmxStatus(String tmxStatus) {
        this.tmxStatus = tmxStatus;
    }

    public String getTmxRisk() {
        return tmxRisk;
    }

    public void setTmxRisk(String tmxRisk) {
        this.tmxRisk = tmxRisk;
    }

    public Integer getTmxScore() {
        return tmxScore;
    }

    public void setTmxScore(Integer tmxScore) {
        this.tmxScore = tmxScore;
    }

    public TmxReasons[] getTmxReasons() {
        return tmxReasons;
    }

    public void setTmxReasons(TmxReasons[] tmxReasons) {
        this.tmxReasons = tmxReasons;
    }

    public String getTmxError() {
        return tmxError;
    }

    public void setTmxError(String tmxError) {
        this.tmxError = tmxError;
    }
	
}
