package com.addcel.mx.antad.servicios.model.vo;

public class RespuestaTelefonica {

	private String codigoRespuesta;
	
	private String idAutorizacion;
	
	private String idTransaccion;
	
	private String descripcionBanco;
	
	private String idInstituto;
	
	private String network;
	
	private String isoEntrada;
	
	private String isoSalida;

	private String fecha;

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getIdAutorizacion() {
		return idAutorizacion;
	}

	public void setIdAutorizacion(String idAutorizacion) {
		this.idAutorizacion = idAutorizacion;
	}

	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getDescripcionBanco() {
		return descripcionBanco;
	}

	public void setDescripcionBanco(String descripcionBanco) {
		this.descripcionBanco = descripcionBanco;
	}

	public String getIdInstituto() {
		return idInstituto;
	}

	public void setIdInstituto(String idInstituto) {
		this.idInstituto = idInstituto;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getIsoEntrada() {
		return isoEntrada;
	}

	public void setIsoEntrada(String isoEntrada) {
		this.isoEntrada = isoEntrada;
	}

	public String getIsoSalida() {
		return isoSalida;
	}

	public void setIsoSalida(String isoSalida) {
		this.isoSalida = isoSalida;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}	
	
}
