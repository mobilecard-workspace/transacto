package com.addcel.mx.antad.servicios.services;

import static com.addcel.mx.antad.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_SERVICE;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.mx.antad.servicios.client.antad.ObjectFactory;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacion;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacionResponse;
import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.mx.antad.servicios.model.vo.AfiliacionVO;
import com.addcel.mx.antad.servicios.model.vo.AntadDetalle;
import com.addcel.mx.antad.servicios.model.vo.AntadInfo;
import com.addcel.mx.antad.servicios.model.vo.AprovisionamientoResponse;
import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.BlackstoneResponse;
import com.addcel.mx.antad.servicios.model.vo.DatosCorreoVO;
import com.addcel.mx.antad.servicios.model.vo.DatosDevolucion;
import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.model.vo.ReglasResponse;
import com.addcel.mx.antad.servicios.model.vo.ResponseConsultaLCVO;
import com.addcel.mx.antad.servicios.model.vo.ResponsePagoMovilesVO;
import com.addcel.mx.antad.servicios.model.vo.RespuestaAmex;
import com.addcel.mx.antad.servicios.model.vo.RespuestaTelefonica;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.TemplateCorreo;
import com.addcel.mx.antad.servicios.model.vo.ThreatMetrixRequest;
import com.addcel.mx.antad.servicios.model.vo.ThreatMetrixResponse;
import com.addcel.mx.antad.servicios.model.vo.Token;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.mx.antad.servicios.model.vo.ValidacionVO;
import com.addcel.mx.antad.servicios.utils.AddCelGenericMail;
import com.addcel.mx.antad.servicios.utils.Constantes;
import com.addcel.mx.antad.servicios.utils.Utils;
import com.addcel.mx.antad.servicios.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class PayworksService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PayworksService.class);
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);
	
	private static final String AMEX_AUTHORIZATION_CODE = "000";
	
	@Autowired
	private UtilsService jsonUtils;
	
	@Autowired
	private AntadServiciosMapper mapper;
	
	@Autowired
	private MailSenderService mailSenderService;
	
	@Autowired
    @Qualifier("webServiceAntadBridge")
    private transient WebServiceTemplate webServiceTemplate;
		
	@Autowired
	private BlackstoneServices blackstoneServices;
	
	@Autowired
	private TelefonicaService telefonicaService;
	
	@Autowired
	private AmexService amexService;
	
	@Autowired
	private ThreatMetrixClientImpl tmClient;
	
	private Gson gson = new Gson();
	
	public ModelAndView procesaPago(Integer idApp, String idioma, String json, 
				ModelMap modelo, String sessionId, String ip) {
		ModelAndView mav = null;
		MobilePaymentRequestVO pago = null;
		UsuarioVO usuarioVO = null;
		ReglasResponse response = null;
		String token = null;
		String key = null;
		try {
			LOGGER.info("JSON PAGO BANORTE - "+ json);
			if(idApp == 1) {
				json = Utils.decryptJson(json);
			} else {
				key = mapper.getKeyCipher(idApp);
				if(key != null) {
					json = Utils.decryptJson(json, key);
				} else {
					mav = new ModelAndView("payworks/pagina-error");
	    			mav.addObject("mensajeError", "Peticion no valida. Llave no permitida.");
	    			return mav;
				}
			}
			pago = (MobilePaymentRequestVO) jsonUtils.jsonToObject(json, MobilePaymentRequestVO.class);
			LOGGER.info("PAGO: "+json);
			if("en".equals(idioma)){
				mav = new ModelAndView("payworks/pagina-3DS_en");	
			} else {
				mav = new ModelAndView("payworks/pagina-3DS");
			}
    		if(pago.getIdCard() == 0){
    			mav = new ModelAndView("payworks/pagina-error");
    			mav.addObject("mensajeError", "Estimado usuario, hemos detectado que tiene una version antigua de Mobilecard, "
    					+ "por favor, actualicela para poder hacer uso de los servicios que le ofrece Mobilecard. Por su atencion, gracias. ");
    			return mav;
    		}
    		usuarioVO = mapper.getUsuario(Long.valueOf(pago.getIdUser()), pago.getIdCard());
    		LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+", TARJETA: "+usuarioVO.getNumTarjeta()
			+", VIGENCIA: "+usuarioVO.getVigencia()+", CVV: "+usuarioVO.getCvv2()+",  ID TARJETA: "+pago.getIdCard()+
			", "+",  ID APP: "+idApp);
    		if ("1".equals(usuarioVO.getTipoTarjeta()) || "2".equals(usuarioVO.getTipoTarjeta())
    				|| "3".equals(usuarioVO.getTipoTarjeta()) || "11".equals(usuarioVO.getTipoTarjeta())){
    			response = validaReglas(usuarioVO.getIdUsuario(), AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()), 
    					pago.getImei(), pago.getIdCard(), pago.getOperacion());
    			if(response == null){
    				pago.setNombre(usuarioVO.getNombre());
            		pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
            		pago.setIdCard(pago.getIdCard());
            		pago.setTarjeta("XXXX XXXX XXXX " + 
            				pago.getTarjeta().substring(pago.getTarjeta().length() - 4 ,pago.getTarjeta().length()));
            		if("1".equals(usuarioVO.getTipoTarjeta())){
            			pago.setTarjetaT("VISA");
            		} else if("2".equals(usuarioVO.getTipoTarjeta())){
            			pago.setTarjetaT("MASTERCARD");
            		} else if("3".equals(usuarioVO.getTipoTarjeta())){
            			pago.setTarjetaT("AMEX");
            		}
        			if(usuarioVO.getCvv2() != null){
        				pago.setVigencia(AddcelCrypto.decryptTarjeta(usuarioVO.getVigenciaTarjeta()));
        				pago.setCvv2(AddcelCrypto.decryptTarjeta(usuarioVO.getCvv2()));
        			}
        			token = mapper.getToken();
        			token = AddcelCrypto.encryptSensitive(SDF.format(new Date()), token);
        			token = token + "#" + pago.getIdUser() + "#" + sessionId + "#" + ip;
        			LOGGER.info("TOKEN GENERADO - {}", token);
        			token = AddcelCrypto.encryptSensitive(SDF.format(new Date()), token);
        			LOGGER.info("TOKEN GENERADO CIFRADO - {}", token);
        			pago.setToken(token);
        			pago.setIdioma(idioma);
        			pago.setIdApp(idApp);
        			pago.setAccountId(Utils.digest(String.valueOf(pago.getIdUser())));
        			modelo.put("pagoForm", pago);
        			mav.addObject("pagoForm", pago);
    			} else {
    				mav = new ModelAndView("payworks/pagina-error");
        			mav.addObject("mensajeError", response.getMensaje());
    			}
    		} else {
    			mav = new ModelAndView("payworks/pagina-error");
    			mav.addObject("mensajeError", "Tarjeta no valida");
    		}
		}catch (Exception pe) {
			LOGGER.error("Error General en el proceso de pago.: {}", pe.getMessage());
			pe.printStackTrace();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Error General en el proceso de pago: " + pe.getMessage());
		}
		return mav;
	}
	
	public ModelAndView procesaPago(MobilePaymentRequestVO pago, ModelMap modelo, String sessionId, 
			HttpServletRequest request, String accountId){
		ModelAndView mav = new ModelAndView("exito");
		AfiliacionVO afiliacion = null;
		UsuarioVO usuarioVO = null;
		BlackstoneResponse response = null; 
		String telefonicaResponse = null;
		try{
			if(!accountId.equals(Utils.digest(String.valueOf(pago.getIdUser())))) {
				return new ModelAndView("401");
			}
			if("2".equals(pago.getIdPais())){
				afiliacion = mapper.buscaAfiliacion("5");
			} else {
				afiliacion = mapper.buscaAfiliacion("1");
			}
			if(afiliacion == null){
				LOGGER.debug("NO hay afiliacion configurada" );
				mav = new ModelAndView("pagina-error");
				mav.addObject("mensajeError", "No existe una afiliacion configurada.");
			}else{
				
				usuarioVO = mapper.getUsuario(Long.valueOf(pago.getIdUser()), pago.getIdCard());
				LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+", TARJETA: "+usuarioVO.getNumTarjeta()
        				+", VIGENCIA: "+usuarioVO.getVigencia()+", CVV: "+usuarioVO.getCvv2() );
				pago.setAfiliacion(afiliacion);
				pago.setNombre(usuarioVO.getNombre() );
				pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
    			pago.setVigencia(AddcelCrypto.decryptTarjeta(usuarioVO.getVigenciaTarjeta()));
	    		if(usuarioVO.getCvv2() != null){
	    			pago.setCvv2(AddcelCrypto.decryptTarjeta(usuarioVO.getCvv2()));
	    		}
	    		
	    		pago.setTipoTarjeta(Integer.valueOf(usuarioVO.getTipoTarjeta()));
	    		LOGGER.info("DATOS  DE USUARIO: "+pago.getNombre()+", TARJETA: "+pago.getTarjeta()
        				+", VIGENCIA: "+pago.getVigencia()+", CVV: "+pago.getCvv2() );
				insertaBitacoras(pago);
				if("91".equals(String.valueOf(pago.getEmisor())) && "2".equals(String.valueOf(pago.getIdPais()))){
					LOGGER.info("RECARGA MOVISTAR - VALIDANDO SERVICIOS CON TELEFONICA: EMISOR: "+pago.getEmisor()
							+" REFERENCIA: "+pago.getReferencia()+", CODIGO PAIS: "+pago.getCodigoPais()+", MONTO: "+pago.getCargo());
					if(telefonicaService.echoRequest(sessionId, pago.getReferencia(), pago.getCargo(), pago.getCodigoPais())){
						LOGGER.info("RECARGA MOVISTAR - VALIDANDO SUSCRIBER - TELEFONO: "+pago.getReferencia()+", CODIGO PAIS: "+pago.getCodigoPais());
						if(telefonicaService.validateSuscriber(sessionId, pago.getReferencia(), pago.getCargo(), pago.getCodigoPais())){
							LOGGER.info("REALIZANDO COBRO CON BLACKSTONE TELEFONO: "+pago.getReferencia());
							response = blackstoneServices.blackstonePayment(pago);
							LOGGER.info("RESPUESTA DE BLACKSTONE -  ERROR CODE: "+response.getErrorCode()
									+", "+response.getMessage());
							if(response.getErrorCode() == 0){
								LOGGER.info("ENVIANDO RECARGA A TELEFONICA -  TELEFONO: "+pago.getReferencia()+", CODIGO PAIS: "+pago.getCodigoPais());
								telefonicaResponse = telefonicaService.sendRecharge(sessionId, pago.getReferencia(), pago.getCargo(), 
										pago.getCodigoPais(), pago.getIdTransaccion());
								RespuestaTelefonica respTel = (RespuestaTelefonica) jsonUtils.jsonToObject(telefonicaResponse, RespuestaTelefonica.class);
								LOGGER.info("RESPUESTA TELEFONICA -  TELEFONO: "+pago.getReferencia()+", CODIGO RESPUESTA: "+respTel.getCodigoRespuesta());
								if (respTel.getCodigoRespuesta().equals("00")){
									mav = new ModelAndView("payworks/pagina-error");
									mav.addObject("mensajeError", "Su recarga ha sido procesado con exito.");
								} else {
									mav = new ModelAndView("payworks/pagina-error");
									mav.addObject("mensajeError", "Su recarga no ha sido exitosa.");
									return mav;
								}
							} else {
								mav = new ModelAndView("payworks/pagina-error");
								mav.addObject("mensajeError", "Su pago no ha sido procesado.");
								return mav;
							}
						} else{
							mav = new ModelAndView("payworks/pagina-error");
							mav.addObject("mensajeError", "El numero ingresado no es valido.");
							return mav;
						}
					} else{
						mav = new ModelAndView("payworks/pagina-error");
						mav.addObject("mensajeError", "El operador no esta disponible en estos momentos. ");
						return mav;
					}
				} else if("AMEX".equals(pago.getTarjetaT())){ 
					try {
						RespuestaAmex respAmex = amexService.enviaPagoAmex(pago, usuarioVO);
						ResponsePagoMovilesVO datosResp = new ResponsePagoMovilesVO();
						AntadInfo pagoInfo = mapper.buscarDetallePago(String.valueOf(pago.getIdTransaccion()));
						if(AMEX_AUTHORIZATION_CODE.equals(respAmex.getCode())){
							AprovisionamientoResponse respuesta = null;
							respuesta = notificacionComercio(pagoInfo);
							if(respuesta.getIdError() == 0){
								double total = respuesta.getMonto() + respuesta.getComision();
								try {
									enviaCorreo(respuesta, total, respAmex.getTransaction(), String.valueOf(pagoInfo.getIdTransaccion()), pagoInfo.getNombre(), 
											pagoInfo.getConcepto(), pagoInfo.getIdApp(), pagoInfo.getIdioma(), pagoInfo.geteMail());
								} catch (Exception e) {
									LOGGER.error("No se pudo enviar el correo..");
								}
								datosResp.setIdError(0);
								updateBitacoras(pagoInfo, datosResp, pago.getReferencia(), respAmex.getTransaction(), null, null, "AMEX", "3", 
										respAmex.getDsc()+" "+pagoInfo.getConcepto());
								mav.addObject("mensajeError", "<BR>Estimado usuario, su pago ha sido exitoso." 
										+"<br><BR><BR> Pago: "+pagoInfo.getConcepto()
										+"<br>Fecha: "+ Utils.getFecha("yyyy-MM-dd HH:mm:ss")
										+"<br>Referencia: "+ pago.getReferencia()
										+"<br>Folio: "+respAmex.getTransaction()
										+"<br>Sucursal: 1"
										+"<br>Caja: 1"
										+"<br>Forma de pago: Tarjeta de credito"
										+"<br>Numero de Autorizacion: "+respAmex.getTransaction()
										+"<br>Folio MC: "+respuesta.getNumAuth()
										+"<br>Importe: "+respuesta.getMonto()
										+"<br>Comision: "+respuesta.getComision()
										+"<br>Total (incluida comision): "+total
										+"<BR><BR>Servicio operado por Plataforma MC. "
										+ "El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas. "
										+ "Para aclaraciones marque 01-800-925-5001. Favor de guardar este comprobante de pago para "
										+ "posibles aclaraciones.");
							} else {
								datosResp.setConcepto("Solicitud será aplicada en las próximas 24 hrs.");
								datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
								datosResp.setIdError(respuesta.getIdError());
								datosResp.setMensajeError(respuesta.getMensajeAntad());
								updateBitacoras(pagoInfo, datosResp, pago.getReferencia(), respAmex.getTransaction(), null, null, 
										"AMEX", "3", datosResp.getMensajeError());
								mav = new ModelAndView("payworks/error");
								mav.addObject("mensajeError", respuesta.getMensajeAntad());
								modelo.put("pagoForm", pagoInfo);
			        			mav.addObject("pagoForm", pagoInfo);
							}
						} else {
							mav = new ModelAndView("payworks/iaveError");
							mav.addObject("descError", "Pago Amex: " + respAmex.getCode() + " - Descripcion: " + respAmex.getDsc());
							LOGGER.error("Error Pago 3D Secure: " + respAmex.getError() + " - " + pago.getIdTransaccion());
							updateBitacoras(pagoInfo, datosResp, pago.getReferencia(), respAmex.getTransaction(), null, null, 
									"AMEX", "3", datosResp.getMensajeError());
						}
					} catch (Exception e) {
						LOGGER.error("ERROR AL PROCESAR EL PAGO CON AMEX...");
						mav = new ModelAndView("payworks/pagina-error");
						mav.addObject("mensajeError", "Ocurrio un error al procesar el pago con American Express");
						e.printStackTrace();
					}
			    }else {
					mav = procesaPago3DSecurePayWorks(pago, modelo);
				}
//				return payworks2Respuesta(pago.getIdTransaccion()+"", "100", "", 
//						"7627488", "A", "A", "000000067538", "766220", "A", "", "", "", "", modelo, request);
			}
		}catch(Exception e){
			LOGGER.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			e.printStackTrace();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Error General en el proceso de pago: " + e.getMessage());
		}
		return mav;
	}
	
	public ModelAndView procesaPago3DSecurePayWorks(MobilePaymentRequestVO pago, ModelMap modelo) {
		ModelAndView mav = null;
		HashMap<String, String> resp = new HashMap<String, String>();
		int respBD = 0;
		double total = 0;
		try{
			LOGGER.info("DATOS ENVIADOS [URL: https://eps.banorte.com/secure3d/Solucion3DSecure.htm, ID BITACORA: "+pago.getIdTransaccion()
					+", NOMBRE: "+pago.getNombre()+", AFILIACION: "+pago.getAfiliacion().getIdAfiliacion()
					+", USUARIO AFILIACION: "+pago.getAfiliacion().getUsuario()
					+", ID TERMINAL: "+pago.getAfiliacion().getIdTerminal()
					+", MERCHANT ID: "+pago.getAfiliacion().getMerchantId()
					+", URL_BACK: "+pago.getAfiliacion().getForwardPath()+"]");
			resp.put("ID_AFILIACION", pago.getAfiliacion().getIdAfiliacion());
			resp.put("USUARIO", pago.getAfiliacion().getUsuario());
			resp.put("CLAVE_USR", pago.getAfiliacion().getClaveUsuario());
			resp.put("CMD_TRANS", "VENTA");
			resp.put("ID_TERMINAL", pago.getAfiliacion().getIdTerminal());
			resp.put("Reference3D", pago.getIdTransaccion() + "");
			resp.put("MerchantId", pago.getAfiliacion().getMerchantId());
			resp.put("ForwardPath", pago.getAfiliacion().getForwardPath());
			resp.put("Expires", pago.getVigencia());
			total = pago.getCargo() + pago.getComision();
			resp.put("Total", Utils.formatoMontoPayworks(total + ""));
			resp.put("Card", pago.getTarjeta());
			resp.put("CardType", pago.getTipoTarjeta() == 1? "VISA": "MC");
			respBD = mapper.guardaTBitacoraPIN(pago.getIdTransaccion(), Utils.encryptHard(pago.getCvv2()));
			if(respBD == 1){
				mav = new ModelAndView("payworks/comercio-send");
				mav.addObject("prosa", resp);
				mav.addObject("pagoRequest", pago);
			}
		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	public ModelAndView procesaRespuesta3DPayworks(String cadena, ModelMap modelo) {
		ResponsePagoMovilesVO datosResp = null;
		ModelAndView mav = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		String pin = null;
		String msg = null;
		AfiliacionVO afiliacion = null;
		DatosDevolucion devolucion = null;
		
		try{
			cadena = cadena.replace(" ", "+");
			LOGGER.debug("Cadena procesaRespuesta3DPayworks: " + cadena);
			String[] spl = cadena.split("&");
			String[] data = null;
			for(String cad: spl){
				LOGGER.debug("data:  " + cad);
				data = cad.split("=");
				resp.put(data[0], data.length >= 2? data[1]: "");
			}
			AntadInfo pagoInfo = mapper.buscarDetallePago((String) resp.get("Reference3D"));
			if(resp.containsKey("Status") && "200".equals(resp.get("Status"))){
				mav = new ModelAndView("payworks/exito");
				mav.addObject("mensajeError", "<BR>Estimado usuario, su pago ha sido exitoso." 
						+"<br><BR><BR> Pago: "+pagoInfo.getConcepto()
						+"<br>Fecha: "+Utils.getFecha("yyyy-MM-dd HH:mm:ss")
						+"<br>Referencia: "+(String) resp.get("Reference3D")
						+"<br>Folio: Aprobado"
						+"<br>Sucursal: 1"
						+"<br>Caja: 1"
						+"<br>Forma de pago: Tarjeta de credito"
						+"<br>Numero de Autorizacion: 1FBAC47582"
						+"<br>Folio MC: "+(String) resp.get("Reference3D")
						+"<br>Importe: "+pagoInfo.getCargo()
						+"<br>Comision: "+pagoInfo.getComision()
						+"<br>Total (incluida comision): "+(String)resp.get("Total")
						+"<BR><BR>Servicio operado por Plataforma MC. "
						+ "El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas. "
						+ "Para aclaraciones marque 01-800-925-5001. Favor de guardar este comprobante de pago para "
						+ "posibles aclaraciones.");
				
//				afiliacion = mapper.buscaAfiliacion("1");
//				pin = mapper.buscarTBitacoraPIN((String) resp.get("Reference3D"));
//				pin = AddcelCrypto.decryptHard(pin);
//				mav=new ModelAndView("payworks/comercioPAYW2");
//				mav.addObject("prosa", resp);
//				resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
//				resp.put("USUARIO", afiliacion.getUsuario());
//				resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
//				resp.put("CMD_TRANS", "VENTA");
//				resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
//				resp.put("Total", Utils.formatoMontoPayworks((String)resp.get("Total")));
//				resp.put("MODO", "PRD");
//				resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
//				resp.put("NUMERO_TARJETA", resp.get("Number"));
//				resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
//				resp.put("CODIGO_SEGURIDAD", pin);
//				resp.put("MODO_ENTRADA", "MANUAL");
//				resp.put("URL_RESPUESTA", Constantes.VAR_PAYW_URL_BACK_W2);
//				resp.put("IDIOMA_RESPUESTA", "ES");
//				devolucion = new DatosDevolucion();
//				if(resp.get("XID") != null && resp.get("CAVV") != null){
//					resp.put("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
//					resp.put("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
//					devolucion.setXid((String)resp.get("XID"));
//					devolucion.setCavv((String)resp.get("CAVV"));
//				} else {
//					mav = new ModelAndView("payworks/pagina-error");
//					datosResp = new ResponsePagoMovilesVO();
//					msg = "PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D RECHAZADA: " + resp.get("Status") ;
//					datosResp.setIdError(Integer.parseInt((String)resp.get("Status")));
//					datosResp.setMensajeError("El pago fue rechazado. Ya que no se encontro autenticacion 3D Secure." );
//					datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
//					datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
//					updateBitacoras(pagoInfo, datosResp, null, null, null, null, null, null, msg);
//					mav.addObject("mensajeError", datosResp.getMensajeError());
//				}
//				resp.put("CODIGO_SEGURIDAD", pin);
//				LOGGER.info("DATOS ENVIADO A PAYWORK2: "+resp.toString());
			} else {
				mav = new ModelAndView("payworks/pagina-error");
				datosResp = new ResponsePagoMovilesVO();
				msg = "PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D RECHAZADA: " + resp.get("Status") ;
				datosResp.setIdError(Integer.parseInt((String)resp.get("Status")));
				datosResp.setMensajeError("El pago fue rechazado. " + 
						(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + Constantes.errorPayW.get(resp.get("Status")) );
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
				updateBitacoras(pagoInfo, datosResp, null, null, null, null, null, null, msg);
				mav.addObject("mensajeError", datosResp.getMensajeError());
			}
		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav=new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}

	public ModelAndView payworks2Respuesta(String NUMERO_CONTROL,
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String CODIGO_PAYW, String RESULTADO_AUT, String BANCO_EMISOR,
			String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA,
			ModelMap modelo, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("exito");
		ResponsePagoMovilesVO datosResp = null;
		ResponseConsultaLCVO respServ = new ResponseConsultaLCVO();
		DatosCorreoVO datosCorreoVO=null;
		String msg = null;
		AprovisionamientoResponse respuesta = null;
		try{
			LOGGER.info("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+" REFERENCIA: " + REFERENCIA
					+" FECHA_RSP_CTE: " + FECHA_RSP_CTE+" TEXTO: " + TEXTO+" RESULTADO_AUT: " + RESULTADO_AUT
					+" RESULTADO_PAYW: " + RESULTADO_PAYW+" FECHA_REQ_CTE: " + FECHA_REQ_CTE+" CODIGO_PAYW: " + CODIGO_PAYW
					+" CODIGO_PAYW: " + CODIGO_PAYW+" BANCO_EMISOR: " + BANCO_EMISOR+" TIPO_TARJETA: " + TIPO_TARJETA
					+" MARCA_TARJETA: " + MARCA_TARJETA);			
			if(MARCA_TARJETA != null){
				MARCA_TARJETA = MARCA_TARJETA.replaceAll("-", "");
				TIPO_TARJETA = MARCA_TARJETA + "/" + TIPO_TARJETA;
			}
			//Comentar datos cuando en PROD, cuando paywork regrese informacion
			if (StringUtils.equals(Constantes.MODO, Constantes.AUT)) {
				BANCO_EMISOR = "BANORTE";
				TIPO_TARJETA = "VISA/CREDITO";
			}
			TEXTO = Utils.cambioAcento(TEXTO);
			
			AntadInfo pagoInfo = mapper.buscarDetallePago(NUMERO_CONTROL);
			LOGGER.info("Consulta detalle pago: "+pagoInfo.toTrace());
			datosResp = new ResponsePagoMovilesVO();
			datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
			datosResp.setReferencia(pagoInfo.getReferencia());
			datosResp.setCargo(pagoInfo.getMonto());
			pagoInfo.setTarjeta(AddcelCrypto.decryptTarjeta(pagoInfo.getTarjeta()));
			datosResp.setTarjeta(pagoInfo.getTarjeta().substring(0, 6) + " XXXXXX " + pagoInfo.getTarjeta().substring(pagoInfo.getTarjeta().length() - 4));
			datosResp.setBancoTarjeta(BANCO_EMISOR);
			
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				msg = "EXITO PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": pagoInfo.getTipoTarjeta() == 2? "MASTER": "VISA") 
						+ " "+BANCO_EMISOR+", "+TIPO_TARJETA;
				datosResp.setAutorizacion(CODIGO_AUT);
	//			mapper.borrarTBitacoraPIN(NUMERO_CONTROL);
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setUsuario(String.valueOf(pagoInfo.getIdUsuario()));
				datosResp.setFecha(respServ.getFecha());
				datosResp.setHora(respServ.getHora());
				datosResp.setReferen(pagoInfo.getReferencia());
				datosResp.setMedioPresentacion("Internet");
				datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
				datosResp.setTipo(TIPO_TARJETA);
				mav = new ModelAndView("payworks/exito");
				datosCorreoVO = new DatosCorreoVO();
				datosCorreoVO.setEmail(pagoInfo.geteMail());
				try {
					respuesta = notificacionComercio(pagoInfo);
					if(respuesta.getIdError() == 0){
						double total = respuesta.getMonto() + respuesta.getComision();
						try {
							enviaCorreo(respuesta, total, CODIGO_AUT, String.valueOf(pagoInfo.getIdTransaccion()), pagoInfo.getNombre(), 
									pagoInfo.getConcepto(), pagoInfo.getIdApp(), pagoInfo.getIdioma(), pagoInfo.geteMail());
						} catch (Exception e) {
							LOGGER.error("No se pudo enviar el correo..");
						}
						datosResp.setIdError(0);
						updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, 
									msg+" "+pagoInfo.getConcepto());
						mav.addObject("mensajeError", "<BR>Estimado usuario, su pago ha sido exitoso." 
								+"<br><BR><BR> Pago: "+pagoInfo.getConcepto()
								+"<br>Fecha: "+Utils.getFecha("yyyy-MM-dd HH:mm:ss")
								+"<br>Referencia: "+REFERENCIA
								+"<br>Folio: "+RESULTADO_PAYW
								+"<br>Sucursal: 1"
								+"<br>Caja: 1"
								+"<br>Forma de pago: Tarjeta de credito"
								+"<br>Numero de Autorizacion: "+CODIGO_AUT
								+"<br>Folio MC: "+respuesta.getNumAuth()
								+"<br>Importe: "+respuesta.getMonto()
								+"<br>Comision: "+respuesta.getComision()
								+"<br>Total (incluida comision): "+total
								+"<BR><BR>Servicio operado por Plataforma MC. "
								+ "El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas. "
								+ "Para aclaraciones marque 01-800-925-5001. Favor de guardar este comprobante de pago para "
								+ "posibles aclaraciones.");
					} else {
						datosResp.setConcepto("Solicitud será aplicada en las próximas 24 hrs.");
						datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
						datosResp.setIdError(respuesta.getIdError());
						datosResp.setMensajeError(respuesta.getMensajeAntad());
						updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, datosResp.getMensajeError());
						mav = new ModelAndView("payworks/error");
						mav.addObject("mensajeError", respuesta.getMensajeAntad());
						modelo.put("pagoForm", pagoInfo);
	        			mav.addObject("pagoForm", pagoInfo);
					}
				} catch (Exception e) {
					LOGGER.error("ERROR AL NOTIFICAR A TRANSACTO:");
				}
			} else {
//				mapper.borrarTBitacoraPIN(NUMERO_CONTROL);
				msg = "PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
						+ ": " + TEXTO ;
				
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setIdError(-9);
				datosResp.setMensajeError("El pago fue " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADO": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADO":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida") +BANCO_EMISOR+", "+TIPO_TARJETA+
					" . " + (CODIGO_PAYW != null? "Clave: " +CODIGO_PAYW:RESULTADO_AUT != null? RESULTADO_AUT: "") + ", Descripcion: " + TEXTO  );
				
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError", datosResp.getMensajeError());
				updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, msg);
			}
		}catch(Exception e){
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
					+ "Por favor vuelva a intentarlo en unos minutos.");
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			datosResp = new ResponsePagoMovilesVO(100, 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
							+ "Por favor vuelva a intentarlo en unos minutos.");
		}finally{
			modelo.put("datosResp", datosResp);
			mav.addObject("datosResp", datosResp);
		}
		return mav;	
	}
	
	public ModelAndView enviaDevolucion(AntadInfo pagoInfo, ModelMap modelo, String referenciaPW) {
		ModelAndView mav = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		String pin = null;
		AfiliacionVO afiliacion = null;
		DatosDevolucion devolucion = null;
		try{				
			afiliacion = mapper.buscaAfiliacion("1");
			devolucion = mapper.getDatosDevolucion(pagoInfo.getIdTransaccion());
			pin = mapper.buscarTBitacoraPIN(String.valueOf(pagoInfo.getIdTransaccion()));
			pin = AddcelCrypto.decryptHard(pin);
			mav=new ModelAndView("payworks/comercioPAYWDevo");
			resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
			resp.put("USUARIO", afiliacion.getUsuario());
			resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
			resp.put("CMD_TRANS", "DEVOLUCION");
			resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
			resp.put("Total", Utils.formatoMontoPayworks(pagoInfo.getMonto()+""));
			resp.put("MODO", "PRD");
			resp.put("REFERENCIA", referenciaPW);
			resp.put("MODO_ENTRADA", "MANUAL");
			resp.put("URL_RESPUESTA", Constantes.VAR_PAYW_URL_BACK_DEV);
			resp.put("IDIOMA_RESPUESTA", "ES");
			resp.put("XID", devolucion.getXid());
			resp.put("CAVV", devolucion.getCavv());
			resp.put("ECI", devolucion.getEci());
			resp.put("ESTATUS_3D", "200");
			mav.addObject("prosa", resp);

		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav=new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	public ModelAndView payworks2DevoRespuesta(/**String NUMERO_CONTROL, **/
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String ID_AFILIACION, 
			ModelMap modelo, HttpServletRequest request) {
		
		ModelAndView mav = new ModelAndView("payworks/exito");
		ResponsePagoMovilesVO datosResp = null;
//		ResponseConsultaLCVO respServ = new ResponseConsultaLCVO();
		String msg = null;
		String NUMERO_CONTROL = null;
		try{
			if(NUMERO_CONTROL == null){
				NUMERO_CONTROL = "156124";
			}
			LOGGER.debug("DEVO NUMERO_CONTROL: " + NUMERO_CONTROL);
			LOGGER.debug("DEVO REFERENCIA: " + REFERENCIA);
			LOGGER.debug("DEVO FECHA_RSP_CTE: " + FECHA_RSP_CTE);
			LOGGER.debug("DEVO TEXTO: " + TEXTO);
			LOGGER.debug("DEVO RESULTADO_PAYW: " + RESULTADO_PAYW);
			LOGGER.debug("DEVO FECHA_REQ_CTE: " + FECHA_REQ_CTE);
			LOGGER.debug("DEVO CODIGO_AUT: " + CODIGO_AUT);
			LOGGER.debug("DEVO ID_AFILIACION: " + ID_AFILIACION);
			TEXTO = Utils.cambioAcento(TEXTO);
			AntadInfo pagoInfo = mapper.buscarDetallePago(NUMERO_CONTROL);
			LOGGER.info("Consulta detalle pago: "+pagoInfo.toTrace());
			datosResp = new ResponsePagoMovilesVO();
			try{
				NUMERO_CONTROL = NUMERO_CONTROL.replaceAll("-D", ""); 
			}catch(Exception e){
				LOGGER.error("Error en parseo NUMERO_CONTROL: " + e.getMessage());
			}
			datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
			datosResp.setReferencia(pagoInfo.getReferencia());
			datosResp.setCargo(pagoInfo.getMonto());
			datosResp.setTarjeta(pagoInfo.getTarjeta().substring(0, 6) + " XXXXXX " + pagoInfo.getTarjeta().substring(pagoInfo.getTarjeta().length() - 4));
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				msg = "DEVOLUCION EXITO PAGO - RECHAZADA POR EL PROVEEDOR";
				datosResp.setAutorizacion(CODIGO_AUT);
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setMedioPresentacion("Internet");
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError", "<BR>Estimado usuario, detectamos un error al aprovisionar su servicio."
						+"<BR><BR><BR> Se procedio a realizar la devolucion de su pago. " 
						+"<br><BR><BR> Estado de la devolucion: "+msg
						+"<BR><BR>Clave: " +RESULTADO_PAYW 
						+"<BR><BR>Autorizacion del reverso: "+CODIGO_AUT);
				
			}else{
				msg = "DEVOLUCION " +
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
						+ ": " + TEXTO ;
				
				datosResp.setFechaTran(new SimpleDateFormat(Constantes.FORMATO_FECHA_COMPRA).format(new Date()));
				datosResp.setIdError(-1);
				datosResp.setMensajeError("Devolucion: " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADO": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADO":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida") +
					".<BR> " +
					"Descripcion: " + TEXTO  );
				
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError", "<BR><BR>   " + datosResp.getMensajeError());
			}
			datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
			updateBitacoras(pagoInfo, datosResp, null, null, REFERENCIA, CODIGO_AUT, null, null, msg);
		}catch(Exception e){
			mav=new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", 
					"Error General al realizar el pago : " + (e.getMessage()!= null && e.getMessage().length() >100? 
							e.getMessage().substring(0,100): e.getMessage()));
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			datosResp = new ResponsePagoMovilesVO(100, "Error General al realizar el pago : " + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			LOGGER.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
		}finally{
			modelo.put("datosResp", datosResp);
			mav.addObject("datosResp", datosResp);
		}
		return mav;	
	}
	
	private AprovisionamientoResponse notificacionComercio(AntadInfo pagoInfo) {
		ProcesaAutorizacion autorizacion = null;
		ProcesaAutorizacionResponse response = null;
		ObjectFactory factory = new ObjectFactory();
		AprovisionamientoResponse respuesta = null;
		String json = null;
		try {
			json = jsonUtils.objectToJson(pagoInfo);
			json = Utils.encryptJson(json);
			autorizacion = new ProcesaAutorizacion();
			autorizacion.setJson(json);
			response = (ProcesaAutorizacionResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createProcesaAutorizacion(autorizacion));
			json = Utils.decryptJson(response.getReturn());
			respuesta = (AprovisionamientoResponse) jsonUtils.jsonToObject(json, AprovisionamientoResponse.class);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_PROCESA_PAGO+" ["+e.getCause()+"]");
			respuesta = new AprovisionamientoResponse();
			respuesta.setIdError(-1);
			respuesta.setMensajeError("Error al realizar servicio.");
		}
		respuesta.setIdError(0);
		
		return respuesta;
	}

	private void insertaBitacoras(MobilePaymentRequestVO pago) throws Exception{
		BitacoraVO tb = null;
		try {
			pago.setTarjeta(pago.getTarjeta()!= null  && !"".equals(pago.getTarjeta())? AddcelCrypto.encryptTarjeta(pago.getTarjeta()): "");
			tb = new BitacoraVO(pago.getIdApp(), Long.valueOf(pago.getIdUser()), ""+pago.getConcepto() + " "+( pago.getTipoTarjeta() == 3?"AMEX":"VISA"), 
					(pago.getSoftware() != null? pago.getSoftware() + " " + pago.getModelo(): "Pago "), 
					"Referencia: " + pago.getReferencia() + ", Importe: " + pago.getCargo(), 
					pago.getTarjeta(), pago.getTipo(), 
					pago.getSoftware(), pago.getModelo(), pago.getImei(), 
					pago.getCargo(), pago.getIdioma());
			tb.setIdProveedor(45);
			mapper.insertaBitacoraTransaccion(tb);
			pago.setIdTransaccion(tb.getIdBitacora());

			TBitacoraProsaVO tbProsa = new TBitacoraProsaVO(tb.getIdBitacora(),
					String.valueOf(pago.getIdUser()),null, null, null, 
					"" +pago.getConcepto() + " "+ (pago.getTipoTarjeta() ==3?"AMEX":"VISA"),
					String.valueOf(pago.getCargo()), pago.getComision()+"", 
					pago.getLat(), pago.getLon());	
			tbProsa.setTarjeta("");
			tbProsa.setTransaccion("");
			mapper.addBitacoraProsa(tbProsa);
			AntadDetalle antadDetalle = new AntadDetalle();
			antadDetalle.setIdTransaccion(pago.getIdTransaccion());
			antadDetalle.setDescripcion(pago.getConcepto());
			antadDetalle.setTipoTarjeta(pago.getTipoTarjeta());
			antadDetalle.setTotal(pago.getCargo());
			antadDetalle.setComision(pago.getComision());
			antadDetalle.setReferencia(pago.getReferencia());
			antadDetalle.setEmisor(String.valueOf(pago.getEmisor()));
			antadDetalle.setCodigoRespuesta("");
			antadDetalle.setOperacion(pago.getOperacion());
			antadDetalle.setIdApp(pago.getIdApp());
			mapper.insertaAntadDetalle(antadDetalle);		
			LOGGER.info("DETALLE ANTAD GUARDADO EXITOSAMENTE - ID TRANSACCION: "+pago.getIdTransaccion()+", LOGIN: "+pago.getUsuario());
			if(pago.getTarjeta() != null && !"".equals(pago.getTarjeta())){
				pago.setTarjeta(AddcelCrypto.decryptTarjeta(pago.getTarjeta()));
			}
		} catch (Exception e) {
			LOGGER.error("Error al guardar en bitacoras,  id_bitacora: {}", tb.getIdBitacora(), e);
			throw new Exception(e);
		}
	}
	
	private void updateBitacoras(AntadInfo pago, ResponsePagoMovilesVO datosResp,  String REFERENCIA, String CODIGO_AUT, 
			String REFERENCIA_CAN, String CODIGO_AUT_CAN, 
			String BANCO_EMISOR, String TIPO_TARJETA, String msg){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		TBitacoraVO tbitacoraVO = new TBitacoraVO();
		try{
        	LOGGER.debug("Inicio Update TBitacora.");
        	tbitacoraVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraVO.setBitConcepto(msg);
        	tbitacoraVO.setBitTicket(msg);
        	tbitacoraVO.setBitNoAutorizacion(datosResp.getAutorizacion());
        	if(datosResp.getIdError() == 0){
        		tbitacoraVO.setBitStatus(1);
        	} else {
        		tbitacoraVO.setBitStatus(datosResp.getIdError());
        	}
        	tbitacoraVO.setBitCodigoError(datosResp.getIdError() != 0? datosResp.getIdError(): 0);
        	tbitacoraVO.setDestino("Referencia: " + pago.getReferencia() + "-" + TIPO_TARJETA);
        	mapper.updateBitacora(tbitacoraVO);
        	LOGGER.debug("Fin Update TBitacora.");
        	LOGGER.debug("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraProsaVO.setConcepto(msg);
        	tbitacoraProsaVO.setAutorizacion(datosResp.getAutorizacion());
        	tbitacoraProsaVO.setReferenciaPayw(REFERENCIA);
        	mapper.updateBitacoraProsa(tbitacoraProsaVO);
        	LOGGER.debug("Fin Update TBitacoraProsa.");
        } catch(Exception e){
        	LOGGER.error("Error al actualizar las bitacoras: ", e);
        }
	}
	
	private void enviaCorreo(AprovisionamientoResponse response, double total,
			String eM_Auth, String eM_OrderID, String nombre, String concepto, 
			int idApp, String idioma, String email) {
		DatosCorreoVO datosCorreoVO = new DatosCorreoVO();
		String ticket = null;
		try {
			datosCorreoVO.setDescServicio(response.getConcepto());
			datosCorreoVO.setFecha(UtilsService.getFechaActual());
			datosCorreoVO.setReferenciaServicio(response.getReferencia());
			datosCorreoVO.setIdBitacora(Long.valueOf(eM_OrderID));
			datosCorreoVO.setNoAutorizacion(eM_Auth);
			if(response.getNumAuth() != null){
				datosCorreoVO.setFolioXcd(response.getNumAuth());
			} else {
				datosCorreoVO.setFolioXcd("Pendiente por Aprobar.");
			}
			datosCorreoVO.setImporte(response.getMonto());
			datosCorreoVO.setComision(response.getComision());
			datosCorreoVO.setMonto(total);
			datosCorreoVO.setNombre(nombre);
			datosCorreoVO.setConcepto(concepto);
			ticket = "Plataforma MC. El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas.";
			datosCorreoVO.setTicket(ticket);
			datosCorreoVO.setEmail(email);
			TemplateCorreo correo = mapper.getTemplateMail("@COMPRA_TRANSACTO", idioma, idApp);
			AddCelGenericMail.generatedMail(datosCorreoVO, correo.getBody(), correo.getAsunto());
		} catch (Exception e) {
			LOGGER.error("Error al mandar el correo: "+e.getCause());
		}	
	}

	private ReglasResponse validaReglas(long idUsuario, String tarjeta, String imei, int idTarjeta, String operacion) {
		ReglasResponse response = null;
		ValidacionVO validacion = null;
		String resp = null;
		try {
			resp = mapper.validaReglas(idUsuario, imei, idTarjeta, tarjeta, operacion);
			LOGGER.info("PAYWORKS - VALIDACION REGLAS - {}", resp);
			validacion = (ValidacionVO) jsonUtils.jsonToObject(resp, ValidacionVO.class);
			if(validacion.getIdError() != 0){
				response = new ReglasResponse(-1, "920",validacion.getMensajeError());
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar Reglas de Negocio.", e);
			response = new ReglasResponse(-1, "920", "Ocurrio un error al validar Reglas de Negocio.");
		}
		return response;
	}
	
	public boolean validateAuthorization(String header, String sessionId) {
		String idUsuario = null;
		String token = null;
		String ip = null;
		String id = null;
		String []tokenArray = null;
		String session = null;
		int countUsuario = 0;
		try {
			LOGGER.info("VALIDANDO TOKEN CIFRADO - {}", header);
			header = AddcelCrypto.decryptSensitive(header);
			LOGGER.info("VALIDANDO TOKEN - {}", header);
			tokenArray = header.split("#");
			if(tokenArray.length == 3) {
				token = tokenArray[0];
				token = AddcelCrypto.decryptSensitive(token);
				LOGGER.info("VALIDANDO TOKEN - {}", token);
				idUsuario = tokenArray[1];
				LOGGER.info("VALIDANDO ID USUARIO - {}", idUsuario);
				session = tokenArray[2];
				LOGGER.info("VALIDANDO SESSION - {}", session);
				ip = tokenArray[3];
				if (mapper.difFechaMin(token) > 3000000) {
					LOGGER.info("TOKEN TIME IS VALID - {}", token);
					if(session.equals(sessionId)) {
						LOGGER.info("SESSION ID IS VALID - SESSION TOKEN - {} - SESSION REQUEST{}", 
								session, sessionId);
						countUsuario = mapper.validaUsuario(idUsuario);
						LOGGER.info("ID USUARIO IS VALID - {}", idUsuario);
						if(countUsuario > 0 ) {
							return true;
						}
					}
				} 
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("ERROR AL VALIDAR TOKEN  - {} - ERROR - {}", token, e.getMessage());
			return false;
		}
		return false;
	}
	
	public Token getToken(Integer idApp, String data, String profileId, String sessionId, String ip) {
		Token respuesta = null;
		String json = null; 
		MobilePaymentRequestVO pago = null;
		ThreatMetrixResponse validate = null;
		String key = null;
		try {
			if(StringUtils.isNotEmpty(profileId)) {
				LOGGER.info("JSON: {}", data);
				if(idApp == 1) {
					json = Utils.decryptJson(data);
				} else {
					if(idApp == 4 ) {
						json = new String(Base64.getDecoder().decode(data), "utf-8");
					} else {
						key = mapper.getKeyCipher(idApp);
						if(key != null) {
							json = new String(Base64.getDecoder().decode(data), "utf-8");
						} else {
							respuesta = new Token();
							respuesta.setMensajeError("Peticion no valida. Llave no permitida.");
							respuesta.setIdError(600);
						}
					}
				}
				pago = (MobilePaymentRequestVO) jsonUtils.jsonToObject(json, MobilePaymentRequestVO.class);
				LOGGER.debug("PAGO: "+jsonUtils.objectToJson(pago));
				if(idApp != 4) {
					validate = validatePayment(pago, sessionId);
					if("high".equals(validate.getTmxRisk())) {
						respuesta.setMensajeError("Se ha detectado actividad inusual en la transaccion. No se puede realizar el pago.");
						respuesta.setIdError(900);
					} else {
						respuesta = generaToken(String.valueOf(pago.getIdUser()), pago.getIdCard(), sessionId, ip);
						if("neutral".equals(validate.getTmxRisk())) {
							respuesta.setSecure(false);
						}
					}
				} else {
					respuesta = generaToken(String.valueOf(pago.getIdUser()), pago.getIdCard(), sessionId, ip);
				}
			} else {
				LOGGER.error("ERROR - LA PETICION NO INCLUYE SESSION ID TMX");
				if(idApp == 4 || idApp == 5) {
					if(idApp == 4) {
						json = new String(Base64.getDecoder().decode(data), "utf-8");
					} else {
						key = mapper.getKeyCipher(idApp);
						LOGGER.info("ID APP - {} - KEY - {}", idApp, key);
						LOGGER.info("DATA - {}", data);
						if(key != null) {
							json = Utils.decryptJson(data, key);
							LOGGER.info("ID APP - {} - DATA - {}", idApp, json);
						} else {
							respuesta = new Token();
							respuesta.setMensajeError("Peticion no valida. Llave no permitida.");
							respuesta.setIdError(600);
						}
						pago = (MobilePaymentRequestVO) jsonUtils.jsonToObject(json, MobilePaymentRequestVO.class);
						respuesta = generaToken(String.valueOf(pago.getIdUser()), pago.getIdCard(), sessionId, ip);
					}
				} else {
					respuesta = new Token();
					respuesta.setMensajeError("Peticion incompleta.");
					respuesta.setIdError(400);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL CONSULTAR EL TOKEN EN BD - CAUSA: ["+e.getCause()+"]");
			respuesta.setMensajeError("No fue posible consultar el token en BD");
			respuesta.setIdError(-1);
		}
		LOGGER.info("CONSULTA DE TOKEN FINALIZADA");
		return respuesta;
	}

	private Token generaToken(String idUsuario, int idTarjeta, String sessionId, String ip) {
		String token = null;
		Token respuesta = new Token();
		try {
			token = mapper.getToken();
			LOGGER.info("TOKEN BD: "+token);
			token = AddcelCrypto.encryptSensitive(SDF.format(new Date()), token);
			LOGGER.info("TOKEN BD: "+token);
			token = token + "#" + idUsuario + "#" + idTarjeta + "#" 
					+ sessionId + "#" + ip;
			LOGGER.info("TOKEN GENERADO: "+token);
			token = AddcelCrypto.encryptSensitive(SDF.format(new Date()), token);
			LOGGER.info("TOKEN GENERADO: "+token);
			respuesta.setAccountId(Utils.digest(idUsuario));
			respuesta.setToken(token);
			respuesta.setMensajeError("Success");
		} catch (Exception e) {
			e.printStackTrace();
			respuesta.setMensajeError("No fue posible consultar el token en BD");
			respuesta.setIdError(-1);
		}
		return respuesta;
	}

	private ThreatMetrixResponse validatePayment(MobilePaymentRequestVO pago, String sessionId) {
		ThreatMetrixRequest request = new ThreatMetrixRequest();
		ThreatMetrixResponse validate = null;
		UsuarioVO usuarioVO = null;
		try {
			usuarioVO = mapper.getUsuario(Long.valueOf(pago.getIdUser()), pago.getIdCard());
			LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+", TARJETA: "+usuarioVO.getNumTarjeta()
			+", VIGENCIA: "+usuarioVO.getVigencia()+", CVV: "+usuarioVO.getCvv2()+",  ID TARJETA: "+pago.getIdCard());
			request.setApplicationName("mobilecardmx");
			request.setCantidad(pago.getCargo());
			request.setCardNumber(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()).substring(0, 6));
			request.setCorreo(usuarioVO.getNombreUsuario());
			request.setNombre(usuarioVO.getNombre());
			request.setSessionId(sessionId);
			request.setTelefono(usuarioVO.getNumCelular());
			request.setUsername(usuarioVO.getNombreUsuario());
			validate = tmClient.investigatePayment(request);
		} catch (Exception e) {
			e.printStackTrace();
			validate = new ThreatMetrixResponse();
			validate.setCode(500);
			validate.setMessage("No se puedo realizar la validacion, intente de nuevo. ");
		}
		LOGGER.info("RESPONSE VALIDATE USER THREATMETRIX - {}", gson.toJson(validate));
		return validate;
	}
	
	public boolean validaToken(String header, String remoteAddr, String id, String json, 
			String accountId, Integer idApp) {
		String idUsuario = null;
		String idTarjeta = null;
		String token = null;
		String ip = null;
		String sessionId = null;
		String headerArray[] = null;
		int tokenBd = 0;
		MobilePaymentRequestVO pago = null;
		String key = null;
		try {
			if(header != null) {
				header = Utils.decryptJson(header);
				LOGGER.info("HEADER: {} - IP - {}", header, remoteAddr + " - SESSION ID  - "+id);
				if(idApp == 4 || idApp == 5) {
					if(idApp == 4) {
						json = new String(Base64.getDecoder().decode(json), "utf-8");
					} else {
						key = mapper.getKeyCipher(idApp);
						LOGGER.info("ID APP - {} - KEY - {}", idApp, key);
						LOGGER.info("DATA - {}", json);
						if(key != null) {
							json = Utils.decryptJson(json, key);
							LOGGER.info("ID APP - {} - DATA - {}", idApp, json);
						} else {
							return false;
						}
					}
				} else {
					json = Utils.decryptJson(json);
				}
				pago = (MobilePaymentRequestVO) jsonUtils.jsonToObject(json, MobilePaymentRequestVO.class);
				LOGGER.info("JSON: {}", json);
				LOGGER.info("VALIDANDO TOKEN AUTHORIZATION...");
				
				if(!accountId.equals(Utils.digest(String.valueOf(pago.getIdUser())))) {
					return false;
				}
				headerArray = header.split("#");
				if(headerArray.length == 5) {
					LOGGER.info("VALIDANDO TIME STAMP... ");
					token = headerArray[0];
					token = AddcelCrypto.decryptSensitive(token);
					tokenBd = mapper.difFechaMin(token);
					if(tokenBd < 30000) {
						LOGGER.info("VALIDANDO TIME STAMP - OK - H: {} - BD:{}", token, tokenBd);
						idUsuario = headerArray[1];
						if(idUsuario.equals(pago.getIdUser())) {
							LOGGER.info("VALIDANDO USUARIO - OK - H: {} - P: {}", idUsuario, pago.getIdUser());
							idTarjeta = headerArray[2];
							if(idTarjeta.equals(String.valueOf(pago.getIdCard()))) {
								LOGGER.info("VALIDANDO ID TARJETA - OK - H: {} - P: {}", idTarjeta, pago.getIdCard());
								ip = headerArray[4];
//								if(ip.equals(remoteAddr)) {
									LOGGER.info("VALIDANDO IP - OK - H: {} - P: {}", ip, remoteAddr);
									return true;
//								} else {
//									LOGGER.info("ERROR AL VALIDAR IP - NO COINCIDEN - H: {} - BD:{}", ip, remoteAddr);
//								}
							} else {
								LOGGER.info("ERROR AL VALIDAR TARJETAS  - NO COINCIDEN - H: {} - BD:{}", idTarjeta, pago.getIdCard());
							}
						} else {
							LOGGER.info("ERROR AL VALIDAR USUARIO  - NO COINCIDEN - H: {} - BD:{}", idUsuario, pago.getIdUser());
						}
					} else {
						LOGGER.info("ERROR AL VALIDAR TIME STAMP  - EXCEDIO TIEMPO LIMITE - H: {} - BD:{}", token, tokenBd);
					}
				} else {
					LOGGER.info("ERROR AL VALIDAR HEADER  - DATOS INCOMPLETOS - {}", header);
				}
			}else {
				LOGGER.info("ERROR AL VALIDAR HEADER  - NO HEADER - {}", header);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL CONSULTAR EL TOKEN EN BD - CAUSA: ["+e.getCause()+"]");
		}
		return false;
	}
	
	

	
}