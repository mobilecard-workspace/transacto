package com.addcel.mx.antad.servicios.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.antad.servicios.model.vo.AbstractVO;
import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.model.vo.Token;
import com.addcel.mx.antad.servicios.services.PayworksService;
import com.google.gson.Gson;

@Controller
public class PayworksController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PayworksController.class);
	
	private static final String PROCESA_PAGO = "{idApp}/{idioma}/{accountId}/pagoProsa3DS";
	
	private static final String ENVIA_PAGO_3DS = "{accountId}/enviaPago3DS";
	
	private static final String TREEDSECURE_RESP = "/payworksRec3DRespuesta";
	
	private static final String PAYWORKS_RESP = "/payworks2RecRespuesta";
	
	private static final String PAYWORKS_DEVOLUCION_RESP = "/payworksDevoRespuesta";
	
	private static final String PATH_GET_TOKEN = "/{idApp}/{idioma}/getToken";
	
	private static final String HEADER_TOKEN = "Authorization";
	
	private static final Gson GSON = new Gson();
	
	@Autowired
	private PayworksService payworksService;
	
	@RequestMapping(value = PROCESA_PAGO)
	public ModelAndView procesaPago(@PathVariable Integer idApp, @PathVariable String idioma, 
			@PathVariable String accountId, @RequestParam("json") String data,  ModelMap modelo, HttpServletRequest req) {
		if(payworksService.validaToken(req.getHeader("Authorization"), req.getRemoteAddr(),
				req.getSession().getId(), data, accountId, idApp)) {
			return payworksService.procesaPago(idApp, idioma, data, modelo, 
					req.getSession().getId(), req.getRemoteAddr());
		} else {
			return new ModelAndView("401");
		}
	}
	
	@RequestMapping(value = ENVIA_PAGO_3DS, method = RequestMethod.POST)
	public ModelAndView enviaPago3DS(@PathVariable String accountId, @ModelAttribute MobilePaymentRequestVO pago,  ModelMap modelo, 
			HttpSession session, HttpServletRequest request) {
		if (payworksService.validateAuthorization(pago.getToken(), 
				request.getSession().getId())) {
			return new ModelAndView("401");
		} else {
			return payworksService.procesaPago(pago, modelo, session.getId(), request, accountId);
		}
	}
	
	@RequestMapping(value = TREEDSECURE_RESP)
	public ModelAndView payworksRec3DRespuesta(@RequestBody String cadena, ModelMap modelo) {
		return payworksService.procesaRespuesta3DPayworks(cadena, modelo);	
	}
	
	@RequestMapping(value = PAYWORKS_RESP)
	public ModelAndView payworks2RecRespuesta(
			@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW, @RequestParam(required = false) String BANCO_EMISOR,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, @RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA, 
			ModelMap modelo,HttpServletRequest request) {
		return payworksService.payworks2Respuesta(NUMERO_CONTROL,REFERENCIA, FECHA_RSP_CTE, 
				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW, RESULTADO_AUT, 
				BANCO_EMISOR, ID_AFILIACION, TIPO_TARJETA, MARCA_TARJETA, modelo, request);	
	}

	@RequestMapping(value = PAYWORKS_DEVOLUCION_RESP)
	public ModelAndView payworksDevoRespuesta(
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION,
			ModelMap modelo,HttpServletRequest request) {
		return payworksService.payworks2DevoRespuesta(REFERENCIA, FECHA_RSP_CTE, 
				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT,
				ID_AFILIACION, modelo, request);	
	}
	
	@RequestMapping(value = PATH_GET_TOKEN, method=RequestMethod.POST)
	public ResponseEntity<AbstractVO> getToken(@PathVariable Integer idApp, @PathVariable String idioma,
			HttpServletRequest req) {
		try {
			LOGGER.debug("Authorization: "+req.getHeader("Authorization"));
			LOGGER.debug("Profile: "+req.getHeader("Profile"));
			if(req.getHeader("Authorization").isEmpty()) {
				return new ResponseEntity<AbstractVO>(new AbstractVO(-1, "No estas autorizado para realizar esta operacion."), HttpStatus.UNAUTHORIZED);
			} else {
				return new ResponseEntity<AbstractVO>(payworksService.getToken(idApp, req.getHeader("Authorization"), req.getHeader("Profile"), 
						req.getSession().getId(), req.getRemoteAddr()), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<AbstractVO>(new AbstractVO(-1, "Error interno, favor de contactar a soporte@addcel.com"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
}
