package com.addcel.mx.antad.servicios.model.vo;

public class Proveedor {

	private String clave;
    
	private String claveWS;
    
	private String path;
    
	private String descripcion;
    
	private int id_categoria;

    private long idusuario;
    
    private long compatible;
    
    private long tipotarjeta;
    
    private long plataforma;

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getClaveWS() {
		return claveWS;
	}

	public void setClaveWS(String claveWS) {
		this.claveWS = claveWS;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getId_categoria() {
		return id_categoria;
	}

	public void setId_categoria(int id_categoria) {
		this.id_categoria = id_categoria;
	}

	public long getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(long idusuario) {
		this.idusuario = idusuario;
	}

	public long getCompatible() {
		return compatible;
	}

	public void setCompatible(long compatible) {
		this.compatible = compatible;
	}

	public long getTipotarjeta() {
		return tipotarjeta;
	}

	public void setTipotarjeta(long tipotarjeta) {
		this.tipotarjeta = tipotarjeta;
	}

	public long getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(long plataforma) {
		this.plataforma = plataforma;
	}
	
}
