package com.addcel.mx.antad.servicios.services;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.mx.antad.servicios.model.vo.AntadDetalle;
import com.addcel.mx.antad.servicios.model.vo.BillPocketResponse;
import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.BlackstoneRequest;
import com.addcel.mx.antad.servicios.model.vo.BlackstoneResponse;
import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.model.vo.RespuestaTelefonica;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.mx.antad.servicios.model.vo.TelefonicaRequest;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.mx.antad.servicios.utils.Utils;
import com.addcel.mx.antad.servicios.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class TelefonicaService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TelefonicaService.class);
	
	@Autowired
	private UtilsService jsonUtils;
	
	@Autowired
	private AntadServiciosMapper mapper;
	
	@Autowired
	private BlackstoneServices blackstoneServices;
	
	private static String URL_TELEFONICA_ECHO = "http://localhost/TelefonicaServices/send/echoRequest";
	
	private static String URL_TELEFONICA_VALIDATE = "http://localhost/TelefonicaServices/validate/suscriber";
	
	private static String URL_TELEFONICA_RECHARGE = "http://localhost/TelefonicaServices/send/recharge";
	
	private static String URL_TELEFONICA_REVERSE = "http://localhost/TelefonicaServices/send/reverse";
	
	public boolean echoRequest(String sessionId, String numero, double monto, String codigoPais){
		String telefonicaResp = null;
		RespuestaTelefonica resp = null;
		TelefonicaRequest request = new TelefonicaRequest();
		try {
			LOGGER.info("ECHO REQUEST - SESSION ID - {} - NUMERO - {}", sessionId, numero);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			request.setNumero(numero);
			request.setCodigoPais(codigoPais);
			request.setMonto(monto);
			request.setAmbiente("1");
			telefonicaResp = jsonUtils.objectToJson(request);
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("json", telefonicaResp);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(URL_TELEFONICA_ECHO, map, String.class);
			resp = (RespuestaTelefonica) jsonUtils.jsonToObject(response.getBody(), RespuestaTelefonica.class);
			LOGGER.info("ECHO REQUEST - RESPUESTA - {}", response.getBody());
			if("00".equals(resp.getCodigoRespuesta())){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("ERROR AL REALIZAR EL ECHO - SESSION ID - {} - ERROR {}", sessionId +" - "+numero, e.getLocalizedMessage());
			resp  = new RespuestaTelefonica ();
			resp.setCodigoRespuesta("-1");
			resp.setDescripcionBanco("ERROR AL VALIDAR EL SUSCRIBER ");
		} 
		return false;
	}
	
	public boolean validateSuscriber(String sessionId, String numero, double monto, String codigoPais){
		String telefonicaResp = null;
		RespuestaTelefonica resp = null;
		TelefonicaRequest request = new TelefonicaRequest();
		try {
			LOGGER.info("VALIDANDO NUMERO DE RECARGA CON TELEFONICA - SESSION ID - {} - NUMERO - {}", sessionId, numero);
			request.setNumero(numero);
			request.setCodigoPais(codigoPais);
			request.setMonto(monto);
			request.setAmbiente("1");
			telefonicaResp = jsonUtils.objectToJson(request);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("json", telefonicaResp);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(URL_TELEFONICA_VALIDATE, map, String.class);
			resp = (RespuestaTelefonica) jsonUtils.jsonToObject(response.getBody(), RespuestaTelefonica.class);
			LOGGER.info("VALIDATE SUSCRIBER - RESPUESTA - {}", response.getBody());
			
			resp = (RespuestaTelefonica) jsonUtils.jsonToObject(response.getBody(), RespuestaTelefonica.class);
			if("00".equals(resp.getCodigoRespuesta())){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("ERROR AL VALIDAR SUSCRIBER - SESSION ID - {} - ERROR {}", sessionId +" - "+numero, e.getLocalizedMessage());
			resp  = new RespuestaTelefonica ();
			resp.setCodigoRespuesta("-1");
			resp.setDescripcionBanco("ERROR AL VALIDAR EL SUSCRIBER ");
		} 
		return false;
	}
	
	public String sendRecharge(String sessionId, String numero, double monto, String codigoPais, long idBitacora){
		String telefonicaResp = null;
		RespuestaTelefonica resp = null;
		TelefonicaRequest request = new TelefonicaRequest();
		String montoD = null; 
		try {
			LOGGER.info("ENVIANDO RECARGA CON TELEFONICA - SESSION ID - {} - NUMERO - {}", sessionId, numero);
			montoD = String.valueOf(monto);
			montoD = montoD.replace(".", "");
			request.setNumero(numero);
			request.setCodigoPais(codigoPais);
			request.setMonto(Integer.valueOf(montoD));
			request.setAmbiente("1");
			request.setIdBitacora(idBitacora);
			telefonicaResp = jsonUtils.objectToJson(request);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("json", telefonicaResp);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(URL_TELEFONICA_RECHARGE, map, String.class);
			resp = (RespuestaTelefonica) jsonUtils.jsonToObject(response.getBody(), RespuestaTelefonica.class);
			LOGGER.info("RECHARGE - RESPUESTA - {}", response.getBody());
			resp = (RespuestaTelefonica) jsonUtils.jsonToObject(response.getBody(), RespuestaTelefonica.class);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("ERROR AL ENVIAR RECARGA - SESSION ID - {} - ERROR {}", sessionId +" - "+numero, e.getLocalizedMessage());
			resp  = new RespuestaTelefonica ();
			resp.setCodigoRespuesta("-1");
			resp.setDescripcionBanco("ERROR AL VALIDAR EL SUSCRIBER ");
		} 
		return jsonUtils.objectToJson(resp);
	}
	
	public BillPocketResponse proccessPayment(MobilePaymentRequestVO pago, String sessionId, 
			HttpServletRequest request, String accountId) {
		BillPocketResponse response = new BillPocketResponse();
		UsuarioVO usuarioVO = null;
		BlackstoneResponse blackResp = null;
		String telefonicaResponse = null;
		try {
			usuarioVO = mapper.getUsuario(Long.valueOf(pago.getIdUser()), pago.getIdCard());
			LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+", TARJETA: "+usuarioVO.getNumTarjeta()
    				+", VIGENCIA: "+usuarioVO.getVigencia()+", CVV: "+usuarioVO.getCvv2() );
			//pago.setAfiliacion();
			pago.setNombre(usuarioVO.getNombre() );
			pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
			pago.setVigencia(AddcelCrypto.decryptTarjeta(usuarioVO.getVigenciaTarjeta()));
    		if(usuarioVO.getCvv2() != null){
    			pago.setCvv2(AddcelCrypto.decryptTarjeta(usuarioVO.getCvv2()));
    		}
    		
    		pago.setTipoTarjeta(Integer.valueOf(usuarioVO.getTipoTarjeta()));
    		LOGGER.info("DATOS  DE USUARIO: "+pago.getNombre()+", TARJETA: "+pago.getTarjeta()
    				+", VIGENCIA: "+pago.getVigencia()+", CVV: "+pago.getCvv2() );
			insertaBitacoras(pago);
			if("96".equals(String.valueOf(pago.getEmisor())) && "2".equals(String.valueOf(pago.getIdPais()))){
				LOGGER.info("RECARGA MOVISTAR - VALIDANDO SERVICIOS CON TELEFONICA: EMISOR: "+pago.getEmisor()
						+" REFERENCIA: "+pago.getReferencia()+", CODIGO PAIS: "+pago.getCodigoPais()+", MONTO: "+pago.getCargo());
				if(echoRequest(sessionId, pago.getReferencia(), pago.getCargo(), pago.getCodigoPais())){
					LOGGER.info("RECARGA MOVISTAR - VALIDANDO SUSCRIBER - TELEFONO: "+pago.getReferencia()+", CODIGO PAIS: "+pago.getCodigoPais());
					if(validateSuscriber(sessionId, pago.getReferencia(), pago.getCargo(), pago.getCodigoPais())){
						LOGGER.info("REALIZANDO COBRO CON BLACKSTONE TELEFONO: "+pago.getReferencia());
						blackResp = blackstoneServices.blackstonePayment(pago);
						LOGGER.info("RESPUESTA DE BLACKSTONE -  ERROR CODE: "+blackResp.getErrorCode()
								+", "+blackResp.getMessage());
						if(blackResp.getErrorCode() == 0){
							LOGGER.info("ENVIANDO RECARGA A TELEFONICA -  TELEFONO: "+pago.getReferencia()+", CODIGO PAIS: "+pago.getCodigoPais());
							telefonicaResponse = sendRecharge(sessionId, pago.getReferencia(), pago.getCargo(), 
									pago.getCodigoPais(), pago.getIdTransaccion());
							RespuestaTelefonica respTel = (RespuestaTelefonica) jsonUtils.jsonToObject(telefonicaResponse, RespuestaTelefonica.class);
							LOGGER.info("RESPUESTA TELEFONICA -  TELEFONO: "+pago.getReferencia()+", CODIGO RESPUESTA: "+respTel.getCodigoRespuesta());
							if (respTel.getCodigoRespuesta().equals("00")){
								response.setAmount(blackResp.getAmount());
								response.setAuthNumber("");
								response.setCode(0);
								response.setDateTime(new java.util.Date().getTime());
								response.setIdTransaccion(pago.getIdTransaccion());
								response.setMaskedPAN("");
								response.setMessage(blackResp.getMessage());
								response.setOpId(Integer.valueOf(String.valueOf(pago.getIdTransaccion())));
								response.setStatus(1);
								response.setTicketUrl("Recharge successful");
								response.setTxnISOCode("");
//								mav = new ModelAndView("payworks/pagina-error");
//								mav.addObject("mensajeError", "Su recarga ha sido procesado con exito.");
							} else {
								response.setAmount(blackResp.getAmount());
								response.setAuthNumber("0");
								response.setCode(-9000);
								response.setDateTime(new java.util.Date().getTime());
								response.setIdTransaccion(pago.getIdTransaccion());
								response.setMaskedPAN("");
								response.setMessage(blackResp.getMessage());
								response.setOpId(Integer.valueOf(String.valueOf(pago.getIdTransaccion())));
								response.setStatus(1);
								response.setTicketUrl("Recharge successful");
								response.setTxnISOCode("");
//								mav = new ModelAndView("payworks/pagina-error");
//								mav.addObject("mensajeError", "Su recarga no ha sido exitosa.");
//								return mav;
							}
						} else {
							response.setCode(-9000);
							response.setMessage(blackResp.getMessage());
//							mav = new ModelAndView("payworks/pagina-error");
//							mav.addObject("mensajeError", "Su pago no ha sido procesado.");
//							return mav;
						}
					} else{
						response.setCode(-9000);
						response.setMessage("El numero ingresado no es valido. / Phone number not valid.");
//						mav = new ModelAndView("payworks/pagina-error");
//						mav.addObject("mensajeError", "El numero ingresado no es valido.");
//						return mav;
					}
				} else{
					response.setCode(-9000);
					response.setMessage("El operador no esta disponible en estos momentos. / The carrier is not available.");
//					mav = new ModelAndView("payworks/pagina-error");
//					mav.addObject("mensajeError", "El operador no esta disponible en estos momentos. ");
//					return mav;
				}
			} else {
				response.setCode(-9000);
				response.setMessage("Servicio no disponible / Service unavailable.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(-9000);
			response.setMessage("Servicio no disponible / Service unavailable.");
		}
		return response;
	}
	
	private void insertaBitacoras(MobilePaymentRequestVO pago) throws Exception{
		BitacoraVO tb = null;
		try {
			pago.setTarjeta(pago.getTarjeta()!= null  && !"".equals(pago.getTarjeta())? AddcelCrypto.encryptTarjeta(pago.getTarjeta()): "");
			tb = new BitacoraVO(pago.getIdApp(), Long.valueOf(pago.getIdUser()), ""+pago.getConcepto() + " "+( pago.getTipoTarjeta() == 3?"AMEX":"VISA"), 
					(pago.getSoftware() != null? pago.getSoftware() + " " + pago.getModelo(): "Pago "), 
					"Referencia: " + pago.getReferencia() + ", Importe: " + pago.getCargo(), 
					pago.getTarjeta(), pago.getTipo(), 
					pago.getSoftware(), pago.getModelo(), pago.getImei(), 
					pago.getCargo(), pago.getIdioma());
			tb.setIdProveedor(45);
			mapper.insertaBitacoraTransaccion(tb);
			pago.setIdTransaccion(tb.getIdBitacora());

			TBitacoraProsaVO tbProsa = new TBitacoraProsaVO(tb.getIdBitacora(),
					String.valueOf(pago.getIdUser()),null, null, null, 
					"" +pago.getConcepto() + " "+ (pago.getTipoTarjeta() ==3?"AMEX":"VISA"),
					String.valueOf(pago.getCargo()), pago.getComision()+"", 
					pago.getLat(), pago.getLon());	
			tbProsa.setTarjeta("");
			tbProsa.setTransaccion("");
			mapper.addBitacoraProsa(tbProsa);
			AntadDetalle antadDetalle = new AntadDetalle();
			antadDetalle.setIdTransaccion(pago.getIdTransaccion());
			antadDetalle.setDescripcion(pago.getConcepto());
			antadDetalle.setTipoTarjeta(pago.getTipoTarjeta());
			antadDetalle.setTotal(pago.getCargo());
			antadDetalle.setComision(pago.getComision());
			antadDetalle.setReferencia(pago.getReferencia());
			antadDetalle.setEmisor(String.valueOf(pago.getEmisor()));
			antadDetalle.setCodigoRespuesta("");
			antadDetalle.setOperacion(pago.getOperacion());
			antadDetalle.setIdApp(pago.getIdApp());
			mapper.insertaAntadDetalle(antadDetalle);		
			LOGGER.info("DETALLE ANTAD GUARDADO EXITOSAMENTE - ID TRANSACCION: "+pago.getIdTransaccion()+", LOGIN: "+pago.getUsuario());
			if(pago.getTarjeta() != null && !"".equals(pago.getTarjeta())){
				pago.setTarjeta(AddcelCrypto.decryptTarjeta(pago.getTarjeta()));
			}
		} catch (Exception e) {
			LOGGER.error("Error al guardar en bitacoras,  id_bitacora: {}", tb.getIdBitacora(), e);
			throw new Exception(e);
		}
	}
		
}
