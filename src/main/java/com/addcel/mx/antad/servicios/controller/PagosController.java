package com.addcel.mx.antad.servicios.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.antad.servicios.services.PagosService;

@Controller
public class PagosController {

	@Autowired
	PagosService pagoService;
	
	@RequestMapping(value = "/pagoDirecto/iniciaSolicitud")
	public ModelAndView procesaPago(@RequestParam("json") String data,  ModelMap modelo) {
		return pagoService.datos3DSecurePayWorks(data, modelo);
	}
	
	@RequestMapping(value = "/pagoDirecto/respuesta/3D")
	public ModelAndView respuesta3D(@RequestBody String cadena, ModelMap modelo) {
		return pagoService.respuesta3D(cadena, modelo);	
	}
	
	@RequestMapping(value = "/pagoDirecto/respuesta/payworks")
	public ModelAndView respuestaPayworks(
			@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW, @RequestParam(required = false) String BANCO_EMISOR,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, @RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA, 
			ModelMap modelo,HttpServletRequest request) {
		return pagoService.respuestaPayworks(NUMERO_CONTROL,REFERENCIA, FECHA_RSP_CTE, 
				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW, RESULTADO_AUT, 
				BANCO_EMISOR, ID_AFILIACION, TIPO_TARJETA, MARCA_TARJETA, modelo, request);	
	}
	
}
