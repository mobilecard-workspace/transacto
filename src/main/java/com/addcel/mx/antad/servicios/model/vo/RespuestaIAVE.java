package com.addcel.mx.antad.servicios.model.vo;

public class RespuestaIAVE {

	private String id_grp;
    private String card_number;
    private String check_digit;
    private String local_date;
    private String amount;
    private String autono;
    private String responsecode;
    private String descriptioncode;
    private String trx_no;
    private long idBitacora;
    
    public String getTrx_no() {
        return trx_no;
    }

    public void setTrx_no(String trx_no) {
        this.trx_no = trx_no;
    }

    
        
    
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAutono() {
        return autono;
    }

    public void setAutono(String autono) {
        this.autono = autono;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCheck_digit() {
        return check_digit;
    }

    public void setCheck_digit(String check_digit) {
        this.check_digit = check_digit;
    }

    public String getDescriptioncode() {
        return descriptioncode;
    }

    public void setDescriptioncode(String descriptioncode) {
        this.descriptioncode = descriptioncode;
    }

    public String getId_grp() {
        return id_grp;
    }

    public void setId_grp(String id_grp) {
        this.id_grp = id_grp;
    }

    public String getLocal_date() {
        return local_date;
    }

    public void setLocal_date(String local_date) {
        this.local_date = local_date;
    }

    public String getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	
}