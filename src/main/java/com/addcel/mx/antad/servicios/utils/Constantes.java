package com.addcel.mx.antad.servicios.utils;

import java.util.HashMap;


public class Constantes {

	public static final String APP_NAME = "[MOBILECARD SERVICIOS] - ";
	
	public static final String FORMATO_FECHA_ENCRIPT = "ddhhmmssSSS";
	
	public static final String FORMATO_FECHA_COMPRA = "dd/MM/yyyy HH:mm";
	
	public static final String LOG_PROCESO = APP_NAME + "[CONTROLLER] ";
	
	public static final String LOG_SERVICE = APP_NAME + "[SERVICE] ";
	
	public static final String LOG_LINE = " - ";
	
	public static final String JSON_VACIO = "";
	
	public static final String TESTING_SERVICES_WEB = "***** Testing Mobilecard Mexico - Antad Services *****";	
	
	public static final String LOG_PROCESO_CONSULTA_CATALOGOS_SERVICIOS = APP_NAME + " INICIANDO CONSULTA DE SERVICIOS ANTAD - ";
	
	public static final String LOG_ERROR_CONSULTA_SERVICIOS = APP_NAME + "";
	
	public static final String LOG_RESPUESTA_CONSULTA_SERVICIOS = LOG_SERVICE + " RESPUESTA DE LA CONSULTA DE SERVICIOS ANTAD - ";
	
	public static final String LOG_PROCESO_CONSULTA_SERVICIOS = APP_NAME + " CONSULTA DE SERVICIOS ANTAD - JSON ";
	
	public static final String LOG_PROCESO_PROCESA_PAGO = APP_NAME + " INICIANDO PAGO DE SERVICIOS ANTAD - ";
	
	public static final String LOG_ERROR_PROCESA_PAGO = APP_NAME + " CAUSA: ";
	
	public static final String LOG_RESPUESTA_PROCESA_PAGO = LOG_SERVICE + " RESPUESTA DE APROVISIONAMIENTO DE SERVICIOS ANTAD - ";	
	
	public static final String LOG_PROCESO_PAGO_SERVICIOS = APP_NAME + "";
	
	public static final String LOG_PROCESO_CONSULTA_SALDO = APP_NAME + "";
	
	public static final String LOG_PROCESO_REALIZA_CONSULTA = APP_NAME + " INICIANDO CONSULTA DE SALDOS ANTAD";
	
	public static final String LOG_ERROR_REALIZA_CONSULTA = APP_NAME + "OCURRIO UN ERROR AL REALIZAR LA CONSULTA DE SALDOS ANTAD";
	
	public static final String LOG_RESPUESTA_PREALIZA_CONSULTA = LOG_SERVICE + " RESPUESTA DE LA CONSULTA DE SALDOS ANTAD";
	
	public static final String LOG_PROCESO_GET_TOKEN = APP_NAME + " INICIANDO CONSULTA DE TOKEN - ";
	
	public static final String LOG_PROCESO_GET_RECARGAS = APP_NAME + " INICIANDO CONSULTA DE GET RECARGAS - SESSION ID: ";
	
	public static final String LOG_PROCESO_GET_RECARGAS_SERVICIOS = APP_NAME + " INICIANDO CONSULTA DE GET RECARGAS SERVICIOS - SESSION ID: ";
	
	public static final String LOG_SERVICE_GET_RECARGAS = APP_NAME + LOG_SERVICE + " CONSULTA DE SERVICIOS ANTAD - SESSION ID {} - JSON {}";
	
	public static final String LOG_ERROR_GET_RECARGAS_SERVICIOS = APP_NAME + " ERROR AL CONSULTAR LAS RECARGAS SERVICIOS - SESSION ID {} - JSON {} - ERROR {}";
	
	public static final String LOG_SERVICE_GET_RECARGAS_SERVICIOS = APP_NAME + LOG_SERVICE + " CONSULTA DE GET RECARGAS SERVICIOS ANTAD - SESSION ID {} - JSON {}";
	
	public static final String LOG_RESPUESTA_GET_RECARGAS_SERVICIOS = LOG_SERVICE + " RESPUESTA DE LA CONSULTA DE RECARGAS SERVICIOS ANTAD - JSON {}";
	
	public static final String JSON_ERROR_INESPERADO = "{\"idError\": -1, \"mensajeError\": \"Ocurrio un error al consultar el servicio.\"}";
	
	// CONSTANTES PARA METODO DE CONSULTA RECARGAS MONTOS
	public static final String LOG_SERVICE_GET_RECARGAS_MONTOS = APP_NAME + LOG_SERVICE + " CONSULTA DE GET RECARGAS MONTOS ANTAD - SESSION ID {} - JSON {}";
	
	public static final String LOG_ERROR_GET_RECARGAS_MONTOS = APP_NAME + " ERROR AL CONSULTAR LAS RECARGAS MONTOS - SESSION ID {} - ERROR {}";
	
	public static final String LOG_RESPUESTA_GET_RECARGAS_MONTOS = LOG_SERVICE + " RESPUESTA DE LA CONSULTA DE RECARGAS MONTOS ANTAD - JSON {}";
	
	// CONSTANTES PARA METODO DE CONSULTA CATEGORIAS
	public static final String LOG_SERVICE_GET_CATEGORIAS = APP_NAME + LOG_SERVICE + " CONSULTA DE GET RECARGAS MONTOS ANTAD - SESSION ID {} - JSON {}";
	
	public static final String LOG_ERROR_GET_CATEGORIAS = APP_NAME + " ERROR AL CONSULTAR LAS RECARGAS MONTOS - SESSION ID {} - ERROR {}";
	
	public static final String LOG_RESPUESTA_GET_CATEGORIAS = LOG_SERVICE + " RESPUESTA DE LA CONSULTA DE RECARGAS MONTOS ANTAD - JSON {}";
	
	//Constantes PROSA
	public static final String CURRENCY = "484"; //Moneda MXN
	
	public static final String ADDRESS = "PROSA"; //domicilio del negocio
	
	public static final String MERCHANT = "8039159"; //afiliacion bancaria	
	
	public static final String STORE = "1234"; //tienda
	
	public static final String TERM = "001"; //terminal
	
	public static final String URLBACK = "https://199.231.160.203/Transacto/iave/3dsecure/respuestaProsa"; //url de respuesta
	
	// Constantes Payworks
	public static String VAR_PAYW_MERCHANT = "8039618";
	
	
	public static String VAR_PAYW_USER = "a8039618";
	
	public static String VAR_PAYW_PASS = "teso9618";
	
	public static String VAR_PAYW_TERMINAL_ID = "80396181";
	
	public static final String VAR_PAYW_URL_BACK = "https://www.mobilecard.mx/Transacto/payworksRec3DRespuesta";
	
	public static final String VAR_PAYW_URL_BACK_W2 = "https://www.mobilecard.mx/Transacto/payworks2RecRespuesta";
	
	public static final String VAR_USA_PAYW_URL_BACK_W2 = "https://www.mobilecard.mx/Transacto/payworks/banorte/usa/respuesta";
	
	public static final String VAR_IAVE_PAYW_URL_BACK_W2 = "https://www.mobilecard.mx/Transacto/iave/payworks/respuesta";
	
	public static final String VAR_PAYW_URL_BACK_DEV = "https://www.mobilecard.mx/Transacto/payworksDevoRespuesta";

	public static final String AUT = "AUT";
	
	public static final String PRD = "PRD";
	
	public static final String MODO = PRD;
	
	public static HashMap<String, String> errorPayW = new HashMap<String, String>();
	
	static{			
		errorPayW.put("200","Indica que la transacción es segura y se puede enviar a Payworks.");
		errorPayW.put("201","Se detecto un error general en el sistema de Visa o Master Card, favor de esperar unos momentos para reintentar la transacción.");
		errorPayW.put("421","El servicio 3D Secure no está disponible, favor de esperar unos momentos para reintentar la transacción.");
		errorPayW.put("422","Se produjo un problema genérico al momento de realizar la Autenticación.");
		errorPayW.put("423","La Autenticación de la Tarjeta no fue exitosa.");
		errorPayW.put("424","Autenticación 3D Secure no fue completada, no está ingresando correctamente la contraseña 3D Secure.");
		errorPayW.put("425","Autenticación Inválida, no está ingresando correctamente la contraseña3D Secure.");
		errorPayW.put("430","Tarjeta de Crédito nulo, la numero de Tarjeta se envió vacía.");
		errorPayW.put("431","Fecha de expiración nulo, la fecha de expricacion se envió vacía.");
		errorPayW.put("432","Monto nulo, la variable Total se envió vacía.");
		errorPayW.put("433","Id del comercio nulo, la variable MerchantId se envió vacía.");
		errorPayW.put("434","Liga de retorno nula, la variable ForwardPath se envió vacía.");
		errorPayW.put("435","Nombre del comercio nulo, la variable MerchantName se envió vacía.");
		errorPayW.put("436","Formato de TC incorrecto, la variable Card debe ser de 16 dígitos.");
		errorPayW.put("437","Formato de Fecha de Expiración incorrecto, la variable Expires debe tener el siguiente formato: YY/MM donde YY se refiere al año y MM se refiere al mes de vencimiento de la tarjeta.");
		errorPayW.put("438","Fecha de Expiración incorrecto, indica que el plástico esta vencido.");
		errorPayW.put("439","Monto incorrecto, la variable Total debe ser un número menor a 999,999,999,999.## con la fracción decimal opcional, esta debe ser a lo más de 2 décimas.");
		errorPayW.put("440","Formato de nombre del comercio incorrecto, debe ser una cadena de máximo 25 caracteres alfanuméricos.");
		errorPayW.put("441","Marca de Tarjeta nulo, la variable CardType se envió vacía.");
		errorPayW.put("442","Marca de Tarjeta incorrecta, debe ser uno de los siguientes valores: VISA (para tarjetas Visa) o MC (para tarjetas Master Card).");
		errorPayW.put("443","CardType incorrecto, se ha especificado el CardType como VISA, sin embargo, el Bin de la tarjeta indica que esta no es Visa.");
		errorPayW.put("444","CardType incorrecto, se ha especificado el CardType como MC, sin embargo, el Bin de la tarjeta indica que esta no es Master Card.");
		errorPayW.put("446","Monto incorrecto, la variable Total debe ser superior a 1.0 pesos.");
		errorPayW.put("498","Transacción expirada. Indica que la transacción sobre paso el límite de tiempo de respuesta esperado.");
		errorPayW.put("499","Usuario excedió en tiempo de respuesta. Indica que el usuario tardo en capturar la información de 3D Secure mayor al tiempo esperado.");
	}
	
}
