package com.addcel.mx.antad.servicios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.mx.antad.servicios.model.vo.ThreatMetrixRequest;
import com.addcel.mx.antad.servicios.model.vo.ThreatMetrixResponse;
import com.google.gson.Gson;

@Service
public class ThreatMetrixClientImpl implements ThreatMetrixClient {
	
	private static final String THREATHMETRIX_URL_PAYMENT = "http://localhost/ThreatMetrix/api/investigarPago";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ThreatMetrixClientImpl.class);
	
	private Gson GSON = new Gson();

	@Override
	public ThreatMetrixResponse investigatePayment(ThreatMetrixRequest request) {
		ThreatMetrixResponse validate = null;
		ResponseEntity<String> responseEntity = null;
		try {
			RestTemplate rest = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
			HttpEntity<ThreatMetrixRequest> entity = new HttpEntity<ThreatMetrixRequest>(request, headers);
			LOGGER.info("PETICION A TMX  - {}", GSON.toJson(request));
			responseEntity = rest.exchange(THREATHMETRIX_URL_PAYMENT, HttpMethod.POST, entity, String.class);
			LOGGER.info("RESPUESTA DE TMX - {}", responseEntity.getBody());
			validate = GSON.fromJson(responseEntity.getBody(), ThreatMetrixResponse.class);
		} catch (Exception e) {
			e.printStackTrace();
			validate = new ThreatMetrixResponse();
			validate.setCode(-250);
			validate.setMessage("Su transaccion no ha podido ser analizado para procesar su pago.");
		}
		return validate;
	}

}
