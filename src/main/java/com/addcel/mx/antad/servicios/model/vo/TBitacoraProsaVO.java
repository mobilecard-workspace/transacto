package com.addcel.mx.antad.servicios.model.vo;

public class TBitacoraProsaVO{	
	private long idBitacoraProsa;
	private long idBitacora;
	private String idUsuario;
	private String tarjeta;
	private String transaccion;
	private String autorizacion;
	private String fecha;
	private String bit_hora;
	private String concepto;
	private String cargo;
	private String comision;
	private double cx;
	private double cy;
	private String referenciaPayw;
	
	public TBitacoraProsaVO(){};
	
	public TBitacoraProsaVO( long idBitacora,
		 String idUsuario,
		 String tarjeta,
		 String transaccion,
		 String autorizacion,
		 String concepto,
		 String cargo,
		 String comision,
		 double cx,
		 double cy){
		
		this.idBitacora = idBitacora;
		this.idUsuario = idUsuario;
		this.tarjeta = tarjeta;
		this.transaccion = transaccion;
		this.autorizacion = autorizacion;
		this.concepto = concepto;
		this.cargo = cargo;
		this.comision = comision;
		this.cx = cx;
		this.cy = cy;
	}
	
	public long getIdBitacoraProsa() {
		return idBitacoraProsa;
	}
	public void setIdBitacoraProsa(long idBitacoraProsa) {
		this.idBitacoraProsa = idBitacoraProsa;
	}
	public long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getTransaccion() {
		return transaccion;
	}
	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getBit_hora() {
		return bit_hora;
	}
	public void setBit_hora(String bit_hora) {
		this.bit_hora = bit_hora;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getComision() {
		return comision;
	}
	public void setComision(String comision) {
		this.comision = comision;
	}
	public double getCx() {
		return cx;
	}
	public void setCx(double cx) {
		this.cx = cx;
	}
	public double getCy() {
		return cy;
	}
	public void setCy(double cy) {
		this.cy = cy;
	}

	public String getReferenciaPayw() {
		return referenciaPayw;
	}

	public void setReferenciaPayw(String referenciaPayw) {
		this.referenciaPayw = referenciaPayw;
	}
	
		
}
