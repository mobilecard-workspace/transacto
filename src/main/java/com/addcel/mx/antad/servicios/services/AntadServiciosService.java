package com.addcel.mx.antad.servicios.services;

import static com.addcel.mx.antad.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;
import static com.addcel.mx.antad.servicios.utils.Constantes.JSON_ERROR_INESPERADO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_REALIZA_CONSULTA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_REALIZA_CONSULTA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_PREALIZA_CONSULTA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_SERVICE;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.mx.antad.servicios.client.antad.Consulta;
import com.addcel.mx.antad.servicios.client.antad.ConsultaResponse;
import com.addcel.mx.antad.servicios.client.antad.ObjectFactory;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacion;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacionResponse;
import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.mx.antad.servicios.model.vo.Token;
import com.addcel.mx.antad.servicios.utils.DivisaUtils;
import com.addcel.mx.antad.servicios.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class AntadServiciosService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AntadServiciosService.class);

	private static final SimpleDateFormat SDF = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);
	
	@Autowired
	private AntadServiciosMapper mapper;
	
	@Autowired
	private UtilsService jsonUtils;
	
	@Autowired
    @Qualifier("webServiceAntadBridge")
    private transient WebServiceTemplate webServiceTemplate;


	public String realizaAprovisionamiento(Integer idApp, String idioma, String json) {
		ProcesaAutorizacion autorizacion = null;
		ProcesaAutorizacionResponse response = null;
		ObjectFactory factory = new ObjectFactory();
		try {
			json = procesaCifrado(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_PROCESA_PAGO + json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
			autorizacion = new ProcesaAutorizacion();
			autorizacion.setJson(json);
			response = (ProcesaAutorizacionResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createProcesaAutorizacion(autorizacion));
			json = procesaCifrado(response.getReturn());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_PROCESA_PAGO+" ["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_PROCESA_PAGO+json);
			json = cifraRespuesta(json);
		}
		return json;
	}

	public String realizaConsulta(Integer idApp, String idioma, String json) {
		ObjectFactory factory = new ObjectFactory();
		Consulta consulta;
		ConsultaResponse response; 
		try {
			if(4 == idApp) {
				json = new String(Base64.getDecoder().decode(json), "utf-8");
			} else {
				json = procesaCifrado(json);
			}
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_REALIZA_CONSULTA + json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
			consulta = new Consulta();
			consulta.setJson(json);
			response = (ConsultaResponse) webServiceTemplate.marshalSendAndReceive(factory.createConsulta(consulta));
			
			json = procesaCifrado(response.getReturn());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_REALIZA_CONSULTA+"["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_PREALIZA_CONSULTA + json);
			if(4 != idApp) {
				json = cifraRespuesta(json);
			}
		}
		return json;
	}
		
	public String getToken() {
		Token respuesta = new Token();
		String token;
		String json = null; 
		try {
			LOGGER.info("TOKEN NO GENERADO...");
			respuesta.setIdError(-1);
			respuesta.setMensajeError("Actualiza tu app a la version mas actual.");
		} catch (Exception e) {
			LOGGER.error("ERROR AL CONSULTAR EL TOKEN EN BD - CAUSA: ["+e.getCause()+"]");
			respuesta.setMensajeError("No fue posible consultar el token en BD");
			respuesta.setIdError(-1);
		} finally{
			json = (String) jsonUtils.objectToJson(respuesta);
			json = cifraRespuesta(json);
			LOGGER.info("CONSULTA DE TOKEN FINALIZADA");
		}
		return json;
	}
	
	private static String cifraRespuesta(String json){
		return AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
	}
	
	private static String procesaCifrado(String json){
		return AddcelCrypto.decryptSensitive(json);
	}
	
	public WebServiceTemplate getWebServiceTemplate() {
		return webServiceTemplate;
	}

	public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}

	public String consultaDolar(Integer idApp, String idioma, String json) {
		double dolar = 0;
		DivisaUtils divisa = new DivisaUtils();
		try {
			LOGGER.info("CONSULTADO VALOR DEL DOLAR - {}", json);
//			dolar = divisa.consultaDivisa();
			if(dolar != 0) {
				mapper.updateDivisa(dolar, "1");
			} else {
				dolar = mapper.obtenDivisa("1");
			}
			
		} catch (Exception e) {
			dolar = mapper.obtenDivisa("1");
			LOGGER.error("ERROR AL CONSULTAR LA DIVISA - {}", "["+e.getCause()+"]");
		} finally{
			json = "{\"valorDolar\": "+dolar+"}";
			LOGGER.info("VALOR DEL DOLAR: {}", json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}
	
}
