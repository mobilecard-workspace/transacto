package com.addcel.mx.antad.servicios.model.vo;


public class Sender {

	private String Name;
	
	private Contact Contact;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public Contact getContact() {
		return Contact;
	}

	public void setContact(Contact contact) {
		Contact = contact;
	}
	
	
	
}
