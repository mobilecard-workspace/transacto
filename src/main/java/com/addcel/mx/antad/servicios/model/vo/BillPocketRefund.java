package com.addcel.mx.antad.servicios.model.vo;

public class BillPocketRefund {

	private long idTransaccion;

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	
}
