package com.addcel.mx.antad.servicios.services;

import com.addcel.mx.antad.servicios.model.vo.ThreatMetrixRequest;
import com.addcel.mx.antad.servicios.model.vo.ThreatMetrixResponse;

public interface ThreatMetrixClient {

	public ThreatMetrixResponse investigatePayment(ThreatMetrixRequest request);
	
}