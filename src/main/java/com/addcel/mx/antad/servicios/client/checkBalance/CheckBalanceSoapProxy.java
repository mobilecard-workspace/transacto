package com.addcel.mx.antad.servicios.client.checkBalance;

public class CheckBalanceSoapProxy implements com.addcel.mx.antad.servicios.client.checkBalance.CheckBalanceSoap {
  private String _endpoint = null;
  private com.addcel.mx.antad.servicios.client.checkBalance.CheckBalanceSoap checkBalanceSoap = null;
  
  public CheckBalanceSoapProxy() {
    _initCheckBalanceSoapProxy();
  }
  
  public CheckBalanceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initCheckBalanceSoapProxy();
  }
  
  private void _initCheckBalanceSoapProxy() {
    try {
      checkBalanceSoap = (new com.addcel.mx.antad.servicios.client.checkBalance.CheckBalanceLocator()).getCheckBalanceSoap();
      if (checkBalanceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)checkBalanceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)checkBalanceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (checkBalanceSoap != null)
      ((javax.xml.rpc.Stub)checkBalanceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.mx.antad.servicios.client.checkBalance.CheckBalanceSoap getCheckBalanceSoap() {
    if (checkBalanceSoap == null)
      _initCheckBalanceSoapProxy();
    return checkBalanceSoap;
  }
  
  public java.math.BigDecimal consultarSaldoTag(java.lang.String tagId) throws java.rmi.RemoteException{
    if (checkBalanceSoap == null)
      _initCheckBalanceSoapProxy();
    return checkBalanceSoap.consultarSaldoTag(tagId);
  }
  
  
}