package com.addcel.mx.antad.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ResponsePagoMovilesVO extends MobilePaymentRequestVO{
	
	private String fechaTran;
	
	private String medioPresentacion;
	
	private String autorizacion;
	
	private String bancoTarjeta;
	
	public ResponsePagoMovilesVO(){
	}
	
	public ResponsePagoMovilesVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}
	
	public String getFechaTran() {
		return fechaTran;
	}
	public void setFechaTran(String fechaTran) {
		this.fechaTran = fechaTran;
	}
	public String getMedioPresentacion() {
		return medioPresentacion;
	}
	public void setMedioPresentacion(String medioPresentacion) {
		this.medioPresentacion = medioPresentacion;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getBancoTarjeta() {
		return bancoTarjeta;
	}
	public void setBancoTarjeta(String bancoTarjeta) {
		this.bancoTarjeta = bancoTarjeta;
	}
		
}
