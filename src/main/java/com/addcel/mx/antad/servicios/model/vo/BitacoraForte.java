package com.addcel.mx.antad.servicios.model.vo;

public class BitacoraForte {

	private String httpStatus;
	private Long idBitacora;
	private Long idBitacoraForte;
	
	public String getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}
	public Long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(Long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public Long getIdBitacoraForte() {
		return idBitacoraForte;
	}
	public void setIdBitacoraForte(Long idBitacoraForte) {
		this.idBitacoraForte = idBitacoraForte;
	}
}