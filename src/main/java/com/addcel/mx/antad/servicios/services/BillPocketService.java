package com.addcel.mx.antad.servicios.services;

import static com.addcel.mx.antad.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_SERVICE;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ws.client.core.WebServiceTemplate;
import com.addcel.mx.antad.servicios.client.antad.ObjectFactory;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacion;
import com.addcel.mx.antad.servicios.client.antad.ProcesaAutorizacionResponse;
import com.addcel.mx.antad.servicios.client.push.PushClient;
import com.addcel.mx.antad.servicios.client.push.PushParams;
import com.addcel.mx.antad.servicios.client.push.PushRequest;
import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.mx.antad.servicios.model.vo.AntadDetalle;
import com.addcel.mx.antad.servicios.model.vo.AntadInfo;
import com.addcel.mx.antad.servicios.model.vo.AprovisionamientoResponse;
import com.addcel.mx.antad.servicios.model.vo.BillPocketAuthorization;
import com.addcel.mx.antad.servicios.model.vo.BillPocketRefund;
import com.addcel.mx.antad.servicios.model.vo.BillPocketResponse;
import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.DatosCorreoVO;
import com.addcel.mx.antad.servicios.model.vo.ForteRequest;
import com.addcel.mx.antad.servicios.model.vo.ForteResponse;
import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.model.vo.ReglasResponse;
import com.addcel.mx.antad.servicios.model.vo.ResponsePagoMovilesVO;
import com.addcel.mx.antad.servicios.model.vo.RespuestaAmex;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.TemplateCorreo;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.mx.antad.servicios.model.vo.ValidacionVO;
import com.addcel.mx.antad.servicios.utils.AddCelGenericMail;
import com.addcel.mx.antad.servicios.utils.Utils;
import com.addcel.mx.antad.servicios.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class BillPocketService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BillPocketService.class);
	
	private static final String AMEX_AUTHORIZATION_CODE = "000";
	
	private Gson GSON = new Gson();
	
	@Autowired
	private AntadServiciosMapper mapper;
	
	@Autowired
	private ForteClientImpl forteClient;
	
	@Autowired
    @Qualifier("webServiceAntadBridge")
    private transient WebServiceTemplate webServiceTemplate;
	
	@Autowired
	private PushClient pushClient;
	
	@Autowired
	private AmexService amexService;
	
	public BillPocketResponse processPayment(Integer idApp, Integer idPais, String idioma, MobilePaymentRequestVO data) {
		BillPocketResponse authorization = null;
		UsuarioVO usuarioVO = null;
		ReglasResponse response = null;
		AprovisionamientoResponse respuesta = null;
		try {
			usuarioVO = mapper.getUsuario(Long.valueOf(data.getIdUser()), data.getIdCard());
    		LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+", TARJETA: "+usuarioVO.getNumTarjeta()
			+", VIGENCIA: "+usuarioVO.getVigencia()+", CVV: "+usuarioVO.getCvv2()+",  ID TARJETA: "+data.getIdCard()+
			", "+",  ID APP: "+idApp);
    		if ("1".equals(usuarioVO.getTipoTarjeta()) || "2".equals(usuarioVO.getTipoTarjeta())
    			 || "11".equals(usuarioVO.getTipoTarjeta())){
    			response = validaReglas(usuarioVO.getIdUsuario(), AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()), 
    					data.getImei(), data.getIdCard(), data.getOperacion());
    			if(response == null){
    				if(data.isDebug()) {
    					if(idPais == 3) {
    						data.setCargo(1);
        					data.setComision(0);
    					} else {
    						data.setCargo(10);
        					data.setComision(1);
    					}
    				}
    				
    				if(idPais == 3) {
    					double cargoOriginal = data.getCargo();
    					double comisionOriginal = data.getComision();
    					double dolar = mapper.obtenDivisa("1");
    					if("000033".equals(data.getOperacion())){
        					double cargoDolar = data.getCargo() / dolar;
        					double comisionDolar = data.getComision() / dolar;
        					data.setCargo(cargoDolar);
        					data.setComision(comisionDolar);
        					LOGGER.info("CALCULANDO COSTO EN DOLARES - CARGO: {} - COMISION: {}", cargoDolar, comisionDolar);
        					LOGGER.info("VALOR ORIGINAL PESOS - CARGO: {} - COMISION: {}", cargoDolar, comisionDolar);
    					} else {
    						data.setConcepto("Pago de servicios");
    					}
    					authorization = authorizationForte(idApp, idPais, idioma, data, usuarioVO);
    					if(!"000033".equals(data.getOperacion())){
    						cargoOriginal = cargoOriginal * dolar;
    						comisionOriginal = comisionOriginal * dolar;
    						LOGGER.info("VALOR ORIGINAL PESOS - CARGO: {} - COMISION: {}", cargoOriginal, comisionOriginal);
    					}
    					AntadDetalle antadDetalle = new AntadDetalle();
    					antadDetalle.setTotal(cargoOriginal);
    					antadDetalle.setComision(comisionOriginal);
    					antadDetalle.setIdTransaccion(authorization.getIdTransaccion());
    					antadDetalle.setDescripcion(data.getConcepto());
    					antadDetalle.setTipoTarjeta(data.getTipoTarjeta());
    					antadDetalle.setReferencia(data.getReferencia());
    					antadDetalle.setEmisor(String.valueOf(data.getEmisor()));
    					antadDetalle.setCodigoRespuesta("");
    					antadDetalle.setOperacion(data.getOperacion());
    					antadDetalle.setIdApp(idApp);
    					mapper.insertaAntadDetalle(antadDetalle);		
    				} else {
    					authorization = authorization(idApp, idPais, idioma, data);
    				}
    				if(authorization.getCode() == 0) {
    					AntadInfo pagoInfo = mapper.buscarDetallePago(String.valueOf(authorization.getIdTransaccion()));
    					if(!"800".equals(data.getIdProveedor())) {
//    						if("000033".equals(data.getOperacion())){
//    							if(idPais == 3) {
//    								double montoMXN = 0;
//    								double comision = 0;
//    								if(data.getCargo() > 10) {
//    									pagoInfo.setCargo(String.valueOf(data.getCargo()));
//                						pagoInfo.setComision(data.getComision());
//    								} else {
//    									montoMXN = mapper.getMontoUsdMXN(data.getCargo());
//                						LOGGER.info("MONTO OBTENIDO DE BD {} -", montoMXN);
//                						comision = montoMXN * 0.05;
//                						pagoInfo.setCargo(String.valueOf(montoMXN));
//                						pagoInfo.setComision(comision);
//    								}
//            						mapper.updateAntadDetalle(montoMXN, comision, pagoInfo.getIdTransaccion());
//            					}
//    						}
        					LOGGER.info("Consulta detalle pago: "+pagoInfo.toTrace());
        					
        					respuesta = notificacionComercio(pagoInfo);
        					authorization.setMessage(authorization.getMessage() + " - " +data.getConcepto());
        					if(respuesta.getIdError() == 0){
        						try {
        							enviaCorreo(respuesta, authorization.getAmount(), authorization.getAuthNumber(), String.valueOf(pagoInfo.getIdTransaccion()), 
        									pagoInfo.getNombre(), pagoInfo.getConcepto(), pagoInfo.getIdApp(), idioma, pagoInfo.geteMail());
        						} catch (Exception e) {
        							LOGGER.error("No se pudo enviar el correo..");
        						}
        					} else {
        						if(idPais == 3) {
        							authorization = reverseForte(authorization.getTransactionId(), data.getImei(), data.getIdUser(), data.getLat(), 
        									data.getLon(), data.getModelo(), data.getSoftware());
        						} else {
        							authorization = reverse(authorization.getIdTransaccion(), idApp, idPais, idioma);
            						authorization.setCode(-9000);
            						authorization.setMessage("Su operacion no pudo ser procesada. Se ha intentado reversar la transaccion: Respuesta del reverso: "
            								+authorization.getMessage());
        						}
        					}
    					} else {
    						LOGGER.info("PAGO PROCESADO CORRECTAMENTE IAVE....");
    						try {
    							respuesta = new AprovisionamientoResponse();
    							respuesta.setConcepto("Recarga MobileTag");
    							respuesta.setReferencia(data.getReferencia());
    							respuesta.setMonto(data.getCargo());
    							respuesta.setComision(data.getComision());
    							respuesta.setNumAuth(authorization.getAuthNumber());
    							LOGGER.info("Enviando el correo a la referencia - {}",  data.getReferencia());
    							/* enviaCorreo(respuesta, authorization.getAmount(), authorization.getAuthNumber(), String.valueOf(pagoInfo.getIdTransaccion()), 
    									pagoInfo.getNombre(), pagoInfo.getConcepto(), pagoInfo.getIdApp(), idioma, data.getReferencia());
    							LOGGER.info("Enviando correo a mobiletag@mobilecardmx.com");
    							enviaCorreo(respuesta, authorization.getAmount(), authorization.getAuthNumber(), String.valueOf(pagoInfo.getIdTransaccion()), 
    									pagoInfo.getNombre(), pagoInfo.getConcepto(), pagoInfo.getIdApp(), idioma, "mobiletag@mobilecardmx.com");
    							LOGGER.info("Enviando notificacion push al usuario");*/
    							sendNotification(authorization.getAmount(), idioma, idPais, idApp, pagoInfo.getIdUsuario(), "USUARIO");
    						} catch (Exception e) {
    							LOGGER.error("No se pudo enviar el correo..");
    							e.printStackTrace();
    						}
    						
    					}
    				} else {
    					authorization = new BillPocketResponse();
        				authorization.setCode(-1000);
        				authorization.setMessage("RECHAZADA. SU COMPRA NO HA SIDO AUTORIZADA POR LA ENTIDAD BANCARIA.");
        				LOGGER.error("RESPUESTA PROCESADOR PAGO - {}", authorization.getMessage());
    				}
    			} else {
    				authorization = new BillPocketResponse();
    				authorization.setCode(response.getResultado());
    				authorization.setMessage(response.getDescripcionRechazo());
    			}
    		} else {
    			try {
    				authorization = new BillPocketResponse();
    				insertaBitacoras(data);
					RespuestaAmex respAmex = amexService.enviaPagoAmex(data, usuarioVO);
					ResponsePagoMovilesVO datosResp = new ResponsePagoMovilesVO();
					AntadInfo pagoInfo = mapper.buscarDetallePago(String.valueOf(data.getIdTransaccion()));
					if(AMEX_AUTHORIZATION_CODE.equals(respAmex.getCode())){
						respuesta = notificacionComercio(pagoInfo);
						if(respuesta.getIdError() == 0){
							double total = respuesta.getMonto() + respuesta.getComision();
							/*try {
								enviaCorreo(respuesta, total, respAmex.getTransaction(), String.valueOf(pagoInfo.getIdTransaccion()), pagoInfo.getNombre(), 
										pagoInfo.getConcepto(), pagoInfo.getIdApp(), pagoInfo.getIdioma(), pagoInfo.geteMail());
							} catch (Exception e) {
								LOGGER.error("No se pudo enviar el correo - {}", pagoInfo.geteMail());
							}*/
							datosResp.setIdError(0);
							authorization.setAuthNumber(respAmex.getTransaction());
							authorization.setIdTransaccion(data.getIdTransaccion());
							authorization.setAmount(data.getCargo() + data.getComision());	
							usuarioVO.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
							authorization.setMaskedPAN("**** - **** - **** - "+usuarioVO.getTarjeta().substring(usuarioVO.getTarjeta().length() - 4 ,
									usuarioVO.getTarjeta().length()));
							authorization.setDateTime(new Date().getTime());
							datosResp.setAutorizacion(respAmex.getTransaction());
							datosResp.setConcepto("Pago de servicios - "+data.getConcepto());
							datosResp.setMensajeError("EXITOSA");
							datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
							updateBitacoras(pagoInfo, datosResp, pagoInfo.getReferencia(), respAmex.getTransaction(), null, null, "AMEX", "3", 
									respAmex.getDsc()+" "+pagoInfo.getConcepto());
						} else {
							datosResp.setConcepto("Solicitud será aplicada en las próximas 24 hrs.");
							datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
							datosResp.setIdError(respuesta.getIdError());
							datosResp.setAutorizacion(respAmex.getTransaction());
							datosResp.setMensajeError(respuesta.getMensajeAntad());
							updateBitacoras(pagoInfo, datosResp, pagoInfo.getReferencia(), respAmex.getTransaction(), null, null, 
									"AMEX", "3", datosResp.getMensajeError());
		        			authorization = new BillPocketResponse();
							authorization.setCode(-500);
							authorization.setMessage("Disculpe las molestias,su pago ha sido rechazado por American Express");
						}
					} else {
						authorization = new BillPocketResponse();
						authorization.setCode(-500);
						authorization.setMessage("Disculpe las molestias,su pago ha sido rechazado por American Express");
						LOGGER.error("Error Pago American Express: " + respAmex.getError() + " - " + pagoInfo.getIdTransaccion());
						updateBitacoras(pagoInfo, datosResp, pagoInfo.getReferencia(), respAmex.getTransaction(), null, null, 
								"AMEX", "3", datosResp.getMensajeError());
					}
				} catch (Exception e) {
					LOGGER.error("ERROR AL PROCESAR EL PAGO CON AMEX...");
					authorization = new BillPocketResponse();
					authorization.setCode(-500);
					authorization.setMessage("Disculpe las molestias, Ocurrio un error al procesar el pago con American Express");
					e.printStackTrace();
				}
    		}
		} catch (Exception e) {
			e.printStackTrace();
			authorization = new BillPocketResponse();
			authorization.setCode(-500);
			authorization.setMessage("Disculpe las molestias, ha ocurrido un error al procesar su pago. Codigo error: "+e.getLocalizedMessage());
		}
		LOGGER.info("RESPUESTA DEL PAGO: {}", GSON.toJson(authorization));
		return authorization;
	}

	private void enviaCorreo(AprovisionamientoResponse response, double total,
			String eM_Auth, String eM_OrderID, String nombre, String concepto, 
			int idApp, String idioma, String email) {
		DatosCorreoVO datosCorreoVO = new DatosCorreoVO();
		String ticket = null;
		try {
			datosCorreoVO.setDescServicio(response.getConcepto());
			datosCorreoVO.setFecha(UtilsService.getFechaActual());
			datosCorreoVO.setReferenciaServicio(response.getReferencia());
			datosCorreoVO.setIdBitacora(Long.valueOf(eM_OrderID));
			datosCorreoVO.setNoAutorizacion(eM_Auth);
			if(response.getNumAuth() != null){
				datosCorreoVO.setFolioXcd(response.getNumAuth());
			} else {
				datosCorreoVO.setFolioXcd("Pendiente por Aprobar.");
			}
			datosCorreoVO.setImporte(response.getMonto());
			datosCorreoVO.setComision(response.getComision());
			datosCorreoVO.setMonto(total);
			datosCorreoVO.setNombre(nombre);
			datosCorreoVO.setConcepto(concepto);
			ticket = "Plataforma MC. El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas.";
			datosCorreoVO.setTicket(ticket);
			datosCorreoVO.setEmail(email);
			TemplateCorreo correo = mapper.getTemplateMail("@COMPRA_TRANSACTO", "es", 1);
			AddCelGenericMail.generatedMail(datosCorreoVO, correo.getBody(), correo.getAsunto());
		} catch (Exception e) {
			LOGGER.error("Error al mandar el correo: "+e.getCause());
			e.printStackTrace();
		}	
	}
	
	private AprovisionamientoResponse notificacionComercio(AntadInfo pagoInfo) {
		ProcesaAutorizacion autorizacion = null;
		ProcesaAutorizacionResponse response = null;
		ObjectFactory factory = new ObjectFactory();
		AprovisionamientoResponse respuesta = null;
		String json = null;
		try {
			json = GSON.toJson(pagoInfo);
			json = Utils.encryptJson(json);
			autorizacion = new ProcesaAutorizacion();
			autorizacion.setJson(json);
			response = (ProcesaAutorizacionResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createProcesaAutorizacion(autorizacion));
			json = Utils.decryptJson(response.getReturn());
			respuesta = (AprovisionamientoResponse) GSON.fromJson(json, AprovisionamientoResponse.class);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_PROCESA_PAGO+" ["+e.getCause()+"]");
			respuesta = new AprovisionamientoResponse();
			respuesta.setIdError(-1);
			respuesta.setMensajeError("Error al realizar servicio.");
		}
		LOGGER.info("RESPUESTA DE TRANSACTO: {}", GSON.toJson(respuesta));
		return respuesta;
	}
	
	private void updateBitacoras(AntadInfo pago, ResponsePagoMovilesVO datosResp,  String REFERENCIA, String CODIGO_AUT, 
			String REFERENCIA_CAN, String CODIGO_AUT_CAN, 
			String BANCO_EMISOR, String TIPO_TARJETA, String msg){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		TBitacoraVO tbitacoraVO = new TBitacoraVO();
		try{
        	LOGGER.debug("Inicio Update TBitacora.");
        	tbitacoraVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraVO.setBitConcepto(msg);
        	tbitacoraVO.setBitTicket(msg);
        	tbitacoraVO.setBitNoAutorizacion(datosResp.getAutorizacion());
        	if(datosResp.getIdError() == 0){
        		tbitacoraVO.setBitStatus(1);
        	} else {
        		tbitacoraVO.setBitStatus(datosResp.getIdError());
        	}
        	tbitacoraVO.setBitCodigoError(datosResp.getIdError() != 0? datosResp.getIdError(): 0);
        	tbitacoraVO.setDestino("Referencia: " + pago.getReferencia() + "-" + TIPO_TARJETA);
        	mapper.updateBitacora(tbitacoraVO);
        	LOGGER.debug("Fin Update TBitacora.");
        	LOGGER.debug("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraProsaVO.setConcepto(msg);
        	tbitacoraProsaVO.setAutorizacion(datosResp.getAutorizacion());
        	tbitacoraProsaVO.setReferenciaPayw(REFERENCIA);
        	mapper.updateBitacoraProsa(tbitacoraProsaVO);
        	LOGGER.debug("Fin Update TBitacoraProsa.");
        } catch(Exception e){
        	LOGGER.error("Error al actualizar las bitacoras: ", e);
        }
	}
	
	private void insertaBitacoras(MobilePaymentRequestVO pago) throws Exception{
		BitacoraVO tb = null;
		try {
			pago.setTarjeta(pago.getTarjeta()!= null  && !"".equals(pago.getTarjeta())? AddcelCrypto.encryptTarjeta(pago.getTarjeta()): "");
			tb = new BitacoraVO(pago.getIdApp(), Long.valueOf(pago.getIdUser()), ""+pago.getConcepto() + " "+( pago.getTipoTarjeta() == 3?"AMEX":"VISA"), 
					(pago.getSoftware() != null? pago.getSoftware() + " " + pago.getModelo(): "Pago "), 
					"Referencia: " + pago.getReferencia() + ", Importe: " + pago.getCargo(), 
					pago.getTarjeta(), pago.getTipo(), 
					pago.getSoftware(), pago.getModelo(), pago.getImei(), 
					pago.getCargo(), pago.getIdioma());
			tb.setIdProveedor(45);
			mapper.insertaBitacoraTransaccion(tb);
			pago.setIdTransaccion(tb.getIdBitacora());

			TBitacoraProsaVO tbProsa = new TBitacoraProsaVO(tb.getIdBitacora(),
					String.valueOf(pago.getIdUser()),null, null, null, 
					"" +pago.getConcepto() + " "+ (pago.getTipoTarjeta() ==3?"AMEX":"VISA"),
					String.valueOf(pago.getCargo()), pago.getComision()+"", 
					pago.getLat(), pago.getLon());	
			tbProsa.setTarjeta("");
			tbProsa.setTransaccion("");
			mapper.addBitacoraProsa(tbProsa);
			AntadDetalle antadDetalle = new AntadDetalle();
			antadDetalle.setIdTransaccion(pago.getIdTransaccion());
			antadDetalle.setDescripcion(pago.getConcepto());
			antadDetalle.setTipoTarjeta(pago.getTipoTarjeta());
			antadDetalle.setTotal(pago.getCargo());
			antadDetalle.setComision(pago.getComision());
			antadDetalle.setReferencia(pago.getReferencia());
			antadDetalle.setEmisor(String.valueOf(pago.getEmisor()));
			antadDetalle.setCodigoRespuesta("");
			antadDetalle.setOperacion(pago.getOperacion());
			antadDetalle.setIdApp(pago.getIdApp());
			mapper.insertaAntadDetalle(antadDetalle);		
			LOGGER.info("DETALLE ANTAD GUARDADO EXITOSAMENTE - ID TRANSACCION: "+pago.getIdTransaccion()+", LOGIN: "+pago.getUsuario());
			if(pago.getTarjeta() != null && !"".equals(pago.getTarjeta())){
				pago.setTarjeta(AddcelCrypto.decryptTarjeta(pago.getTarjeta()));
			}
		} catch (Exception e) {
			LOGGER.error("Error al guardar en bitacoras,  id_bitacora: {}", tb.getIdBitacora(), e);
			throw new Exception(e);
		}
	}
	
	private BillPocketResponse authorizationForte(Integer idApp, Integer idPais, String idioma,
			MobilePaymentRequestVO data, UsuarioVO usuarioVO) {
		BillPocketResponse response = new BillPocketResponse();
		ForteRequest forteRequest = new ForteRequest();
		ForteResponse forteResponse = null;
		double valorDolar = 0;
		double totalDolar = 0;
		double comisionDolar = 0;
		String valorDolarS = null;
		String valorComisionS = null;
		DecimalFormat amount = new DecimalFormat("#.00");
		DecimalFormat comision = new DecimalFormat("#.00");
		try {
			valorDolar = mapper.obtenDivisa("1");
			if("000033".equals(data.getOperacion())){
				if(data.getCargo() > 10) {
					LOGGER.info("VALOR DOLAR {}", valorDolar);
					totalDolar = data.getCargo() / valorDolar ;
					LOGGER.info("CARGO DOLAR {} - {}", data.getCargo(), totalDolar);
					comisionDolar = data.getComision() / valorDolar;
					LOGGER.info("COMISION DOLAR {} - {}", data.getComision(), comisionDolar);
					LOGGER.info("CALCULO TOTAL {} - COMISION {}", amount.format(totalDolar), 
								amount.format(comisionDolar));
				} else {
					totalDolar = data.getCargo();
					comisionDolar = data.getComision();
				}
			} else {
				totalDolar = data.getCargo();
				comisionDolar = data.getComision();
			}
			valorDolarS = amount.format(totalDolar).replace(",", ".");
			valorComisionS = comision.format(comisionDolar).replace(",", ".");
			LOGGER.info("VALOR EN STRING - CARGO: {] - valorComisionS: {}", valorDolarS, valorComisionS);
			forteRequest.setAmount(Double.valueOf(valorDolarS));
			forteRequest.setComision(Double.valueOf(valorComisionS));
			forteRequest.setConcept(data.getConcepto());
			forteRequest.setEmail(usuarioVO.getEmail());
			forteRequest.setIdCard(Long.valueOf(data.getIdCard()));
			forteRequest.setIdioma(idioma);
			forteRequest.setImei(data.getImei());
			forteRequest.setLat(data.getLat());
			forteRequest.setLon(data.getLon());
			forteRequest.setModelo(data.getModelo());
			forteRequest.setMsi(0);
			forteRequest.setNombre(usuarioVO.getNombre());
			forteRequest.setPropina(0d);
			forteRequest.setSoftware(data.getSoftware());
			forteRequest.setApellido(usuarioVO.getApellidoP());
			forteRequest.setCelular(usuarioVO.getNumCelular());
			forteRequest.setTarjeta("NA");
			forteRequest.setTipoTarjeta("NA");
			forteRequest.setVigencia("NA");
			forteRequest.setCt("NA");
			forteRequest.setIdAplicacion(idApp);
			
			forteResponse = forteClient.authorize(forteRequest, data.getIdUser());
			
			if(StringUtils.isNotEmpty(forteResponse.getTransaction_id())) {
				response.setAmount(forteResponse.getAuthorization_amount());
				response.setAuthNumber(forteResponse.getResponse().getAuthorization_code());
				response.setCode(0);
				response.setDateTime(new Date().getTime());
				response.setIdTransaccion(forteResponse.getBitacoraResponse().getIdBitacora());
				response.setMaskedPAN(forteResponse.getCard().getMasked_account_number());
				response.setMessage("Payment "+data.getConcepto()+" Succesfull");
				response.setOpId(forteResponse.getBitacoraResponse().getIdBitacoraForte().intValue());
				response.setTicketUrl("");
				response.setTxnISOCode("");
				response.setTransactionId(forteResponse.getTransaction_id());
			} else {
				response.setCode(-9000);
				response.setMessage("Payment declined. Please contact your Bank.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(-9000);
			response.setMessage("Payment declined. Please contact your Bank.");
		}
		return response;
	}
	
	private BillPocketResponse reverseForte(String transactionId, String imei, Long idUsuario, double lat, double lon, String modelo, 
			String software) {
		BillPocketResponse response = new BillPocketResponse();
		ForteResponse forteResponse = null;
		try {
			forteResponse = forteClient.reverse(transactionId, imei, idUsuario, lat, lon, modelo, software);
			if(forteResponse != null) {
				response.setCode(-500);
				response.setMessage("Transaccion fallida, se ha realizado el reverso de su operacion.");
				response.setDateTime(new Date().getTime());
				response.setAuthNumber(forteResponse.getResponse().getAuthorization_code());
				response.setIdTransaccion(forteResponse.getBitacoraResponse().getIdBitacora());
				response.setMaskedPAN(forteResponse.getCard().getMasked_account_number());
				response.setOpId(forteResponse.getBitacoraResponse().getIdBitacoraForte().intValue());
				response.setTicketUrl("");
				response.setTxnISOCode("");
				response.setTransactionId(forteResponse.getTransaction_id());
			} else {
				response.setCode(-9000);
				response.setMessage("Transaccion fallida, contacte a soporte.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(-9000);
			response.setMessage("Transaccion fallida, contacte a soporte.");
		}
		return response;
	}
	
	public BillPocketResponse authorization(Integer idApp, Integer idPais, String idioma, 
			MobilePaymentRequestVO data) {
		BillPocketAuthorization authorization = new BillPocketAuthorization();
		BillPocketResponse response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			authorization.setIdUsuario(Long.valueOf(data.getIdUser()));
			authorization.setIdProveedor(Integer.valueOf(data.getIdProveedor()));
			authorization.setIdProducto(data.getEmisor());
			authorization.setConcepto(data.getConcepto());
			authorization.setImei(data.getImei());
			authorization.setSoftware(data.getSoftware());
			authorization.setModelo(data.getModelo());
			authorization.setLat(data.getLat());
			authorization.setLon(data.getLon());
			authorization.setIdTarjeta(data.getIdCard());
			authorization.setCargo(data.getCargo());
			authorization.setComision(data.getComision());
			authorization.setReferencia(data.getReferencia());
			authorization.setEmisor(String.valueOf(data.getEmisor()));
			authorization.setOperacion(data.getOperacion());
			HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<BillPocketAuthorization> request = new HttpEntity<>(authorization, headers);
            ResponseEntity<String> respBP = restTemplate.exchange("http://localhost/BillPocket/"+idApp+"/"+idPais+"/"+idioma+"/authorization",
                    HttpMethod.POST, request, String.class);
            LOGGER.info("RESPUESTA DE BILL POCKET - {}", respBP.getBody());
            response = GSON.fromJson(respBP.getBody(), BillPocketResponse.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	public BillPocketResponse reverse(long idTransaccion, Integer idApp, Integer idPais, String idioma) {
		BillPocketResponse response = null;
		BillPocketRefund refund = new BillPocketRefund();
		try {
			LOGGER.info("REALIZANDO LA REVERSA DEL CARGO - {}",idTransaccion);
			refund.setIdTransaccion(idTransaccion);
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<BillPocketRefund> request = new HttpEntity<>(refund, headers);
            ResponseEntity<String> respBP = restTemplate.exchange("http://localhost/BillPocket/"+idApp+"/"+idPais
            		+"/"+idioma+"/refund?idTransaccion="+idTransaccion,
                    HttpMethod.GET, request, String.class);
            LOGGER.info("RESPUESTA DE REVERSO BP - {}", respBP.getBody());
            response = GSON.fromJson(respBP.getBody(), BillPocketResponse.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	public boolean validaToken(Integer idApp, String header, String remoteAddr, String id, MobilePaymentRequestVO data, String accountId) {
		String idUsuario = null;
		String idTarjeta = null;
		String token = null;
		String ip = null;
		String sessionId = null;
		String headerArray[] = null;
		int tokenBd = 0;
		String key = null;
		try {
			if(header != null) {
				header = Utils.decryptJson(header);
				LOGGER.info("HEADER: {} - IP - {}", header, remoteAddr + " - SESSION ID  - "+id);
				LOGGER.info("DATA: {}", GSON.toJson(data));
				LOGGER.info("VALIDANDO TOKEN AUTHORIZATION...");
				
				if(!accountId.equals(Utils.digest(String.valueOf(data.getIdUser())))) {
					return false;
				}
				headerArray = header.split("#");
				if(headerArray.length == 5) {
					LOGGER.info("VALIDANDO TIME STAMP... ");
					token = headerArray[0];
					token = AddcelCrypto.decryptSensitive(token);
//					tokenBd = mapper.difFechaMin(token);
					if(tokenBd < 30000) {
						LOGGER.info("VALIDANDO TIME STAMP - OK - H: {} - BD:{}", token, tokenBd);
						idUsuario = headerArray[1];
						if(idUsuario.equals(String.valueOf(data.getIdUser()))) {
							LOGGER.info("VALIDANDO USUARIO - OK - H: {} - P: {}", idUsuario, data.getIdUser());
							idTarjeta = headerArray[2];
							if(idTarjeta.equals(String.valueOf(data.getIdCard()))) {
								LOGGER.info("VALIDANDO ID TARJETA - OK - H: {} - P: {}", idTarjeta, data.getIdCard());
								return true;
								/*
								 * ip = headerArray[4]; if(ip.equals(remoteAddr)) {
								 * LOGGER.info("VALIDANDO IP - OK - H: {} - P: {}", ip, remoteAddr); return
								 * true; } else {
								 * LOGGER.info("ERROR AL VALIDAR IP - NO COINCIDEN - H: {} - BD:{}", ip,
								 * remoteAddr); }
								 */
							} else {
								LOGGER.info("ERROR AL VALIDAR TARJETAS  - NO COINCIDEN - H: {} - BD:{}", idTarjeta, data.getIdCard());
							}
						} else {
							LOGGER.info("ERROR AL VALIDAR USUARIO  - NO COINCIDEN - H: {} - BD:{}", idUsuario, data.getIdUser());
						}
					} else {
						LOGGER.info("ERROR AL VALIDAR TIME STAMP  - EXCEDIO TIEMPO LIMITE - H: {} - BD:{}", token, tokenBd);
					}
				} else {
					LOGGER.info("ERROR AL VALIDAR HEADER  - DATOS INCOMPLETOS - {}", header);
				}
			}else {
				LOGGER.info("ERROR AL VALIDAR HEADER  - NO HEADER - {}", header);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL CONSULTAR EL TOKEN EN BD - CAUSA: ["+e.getCause()+"]");
		}
		return false;
	}
	
	private ReglasResponse validaReglas(long idUsuario, String tarjeta, String imei, int idTarjeta, String operacion) {
		ReglasResponse response = null;
		ValidacionVO validacion = null;
		String resp = null;
		try {
			resp = mapper.validaReglas(idUsuario, imei, idTarjeta, tarjeta, operacion);
			LOGGER.info("PAYWORKS - VALIDACION REGLAS - {}", resp);
			validacion = GSON.fromJson(resp, ValidacionVO.class);
			if(validacion.getIdError() != 0){
				response = new ReglasResponse(-1, "920",validacion.getMensajeError());
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar Reglas de Negocio.", e);
			response = new ReglasResponse(-1, "920", "Ocurrio un error al validar Reglas de Negocio.");
		}
		return response;
	}
	
	public boolean isUserWhiteList(long idUsuario) {
		int count = 0;
		try {
			LOGGER.info("VALIDANDO USUARIO DE WHITE LIST - {}", idUsuario );
			count = mapper.getWhiteList(idUsuario);
			if(count > 0 ) {
				LOGGER.info("USUARIO DE WHITE LIST OK - {}", idUsuario );
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	private void sendNotification(double monto, String idioma, Integer idPais, Integer idApp, long idUsuario, String tipoUsuario) {
		PushRequest request = null;
		try {
			request = new PushRequest();
			request.setId_usuario(idUsuario);
			request.setIdApp(idApp);
			request.setIdioma(idioma);
			request.setIdPais(idPais);
			request.setModulo(10L);
			request.setTipoUsuario(tipoUsuario);
			PushParams montoParam = new PushParams();
			montoParam.setName("<monto>");
			montoParam.setValue(new Double(monto).toString());
			List<PushParams> params = new ArrayList<PushParams>();
			params.add(montoParam);
			request.setParams(params);
			
			pushClient.sendNotification(request);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}
	
}
