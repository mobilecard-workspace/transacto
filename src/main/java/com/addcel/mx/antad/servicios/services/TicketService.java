package com.addcel.mx.antad.servicios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.mx.antad.servicios.model.vo.AntadCredentials;
import com.addcel.mx.antad.servicios.model.vo.AntadInfo;
import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.mx.antad.servicios.utils.UtilsService;

@Service
public class TicketService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TicketService.class);
		
	@Autowired
	private AntadServiciosMapper mapper;
	
	@Autowired
	private MailSenderService mailSenderService;

	public ModelAndView reportaIncidente(MobilePaymentRequestVO pago, ModelMap modelo, String sessionId) {
		ModelAndView mav = new ModelAndView("payworks/levantaTicket");
		AntadInfo pagoInfo = null;
		String bodyMail = null;
		String recipients = null;
		AntadCredentials credentials;
		UsuarioVO usuarioVO = null;
		try {
			pagoInfo = mapper.buscarDetallePago(String.valueOf(pago.getIdTransaccion()));
			credentials = mapper.antadCredenciales();
			LOGGER.info("ENVIANDO REPORTE - ID SESSION - {} - ID TRANSACCION {}", sessionId, pago.getIdTransaccion());
			bodyMail = mapper.getParametro("@REPORTE_COMPRAANTAD");
			recipients = mapper.getParametro("@RECIPIENTS_COMPRAANTAD");
			LOGGER.info("OBTENIENDO DATOS DE USUARIO - ID SESSION - {} - ID USUARIO - {}", sessionId, pagoInfo.getIdUsuario());
			usuarioVO = mapper.getDatosUsuario(pagoInfo.getIdUsuario());
			LOGGER.info("OBTENIENDO TEMPLATE CORREOS - ID SESSION - {} - ID TRANSACCION {}", sessionId, pago.getIdTransaccion());
			
			bodyMail = bodyMail.replaceAll("<#NOMBREUSUARIO#>", usuarioVO.getNombre());
			bodyMail = bodyMail.replaceAll("<#CONTACTO#>", usuarioVO.getNumCelular());
			bodyMail = bodyMail.replaceAll("<#COMERCIO#>", String.valueOf(credentials.getComercio()));
			
			bodyMail = bodyMail.replaceAll("<#TITULO#>", pagoInfo.getConcepto());
			bodyMail = bodyMail.replaceAll("<#SUCURSAL#>", credentials.getSucursal());
			bodyMail = bodyMail.replaceAll("<#REFERENCIA#>", pagoInfo.getReferencia());
			bodyMail = bodyMail.replaceAll("<#FECHA#>", UtilsService.getFechaActual());
			bodyMail = bodyMail.replaceAll("<#MONTO#>", String.valueOf(pagoInfo.getMonto()));
			bodyMail = bodyMail.replaceAll("<#INCIDENCIA#>", "Aprovisionamiento erroneo");
			bodyMail = bodyMail.replaceAll("<#TICKET#>", "");
			LOGGER.info("ENVIANDO CORREO DE REPORTE DE TICKET  - ID SESSION - {} - ID TRANSACCION {}", sessionId, pago.getIdTransaccion());
			mailSenderService.enviarCorreoTicket(bodyMail, recipients, pagoInfo);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL ENVIAR REPORTE - ID SESSION - {} - EXCEPTION - {}", sessionId, e.getLocalizedMessage());
		}
		return mav;
	}
		
	
}
