//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.03.07 a las 11:06:47 PM CST 
//


package com.addcel.mx.antad.servicios.client.antad;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.addcel.mx.antad.servicios.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Consulta_QNAME = new QName("http://service.bridge.antad.addcel.com/", "consulta");
    private final static QName _ConsultaResponse_QNAME = new QName("http://service.bridge.antad.addcel.com/", "consultaResponse");
    private final static QName _ProcesaAutorizacion_QNAME = new QName("http://service.bridge.antad.addcel.com/", "procesaAutorizacion");
    private final static QName _ProcesaAutorizacionResponse_QNAME = new QName("http://service.bridge.antad.addcel.com/", "procesaAutorizacionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.addcel.mx.antad.servicios.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Consulta }
     * 
     */
    public Consulta createConsulta() {
        return new Consulta();
    }

    /**
     * Create an instance of {@link ConsultaResponse }
     * 
     */
    public ConsultaResponse createConsultaResponse() {
        return new ConsultaResponse();
    }

    /**
     * Create an instance of {@link ProcesaAutorizacion }
     * 
     */
    public ProcesaAutorizacion createProcesaAutorizacion() {
        return new ProcesaAutorizacion();
    }

    /**
     * Create an instance of {@link ProcesaAutorizacionResponse }
     * 
     */
    public ProcesaAutorizacionResponse createProcesaAutorizacionResponse() {
        return new ProcesaAutorizacionResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Consulta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.bridge.antad.addcel.com/", name = "consulta")
    public JAXBElement<Consulta> createConsulta(Consulta value) {
        return new JAXBElement<Consulta>(_Consulta_QNAME, Consulta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.bridge.antad.addcel.com/", name = "consultaResponse")
    public JAXBElement<ConsultaResponse> createConsultaResponse(ConsultaResponse value) {
        return new JAXBElement<ConsultaResponse>(_ConsultaResponse_QNAME, ConsultaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcesaAutorizacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.bridge.antad.addcel.com/", name = "procesaAutorizacion")
    public JAXBElement<ProcesaAutorizacion> createProcesaAutorizacion(ProcesaAutorizacion value) {
        return new JAXBElement<ProcesaAutorizacion>(_ProcesaAutorizacion_QNAME, ProcesaAutorizacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcesaAutorizacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.bridge.antad.addcel.com/", name = "procesaAutorizacionResponse")
    public JAXBElement<ProcesaAutorizacionResponse> createProcesaAutorizacionResponse(ProcesaAutorizacionResponse value) {
        return new JAXBElement<ProcesaAutorizacionResponse>(_ProcesaAutorizacionResponse_QNAME, ProcesaAutorizacionResponse.class, null, value);
    }

}
