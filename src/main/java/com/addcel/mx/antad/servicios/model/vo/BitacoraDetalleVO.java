package com.addcel.mx.antad.servicios.model.vo;

public class BitacoraDetalleVO extends BitacoraVO {

	private long idBitacora;
	
	private long idUsuario;
	
	private String noControl;
	
	private String mail;
	
	private String descripcion;
	
	private String nombre;
	
	private String tarjeta;
	
	private int tipoTarjeta;
	
	private int conMsi;
	
	private int plazo;
	
	private double total;
	
	private double comision;
	
	private String afiliacion;
	
	private String tipoTDC;
	
	private String fecha;
	
	private String fechaModifica;
	
	private int status;
	
	private String noAutorizacion;
	
	private String noAutorizacionCancelacion;
	
	private int idTransaccionCancel;
	
	private String datoExtra;

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNoControl() {
		return noControl;
	}

	public void setNoControl(String noControl) {
		this.noControl = noControl;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public int getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public int getConMsi() {
		return conMsi;
	}

	public void setConMsi(int conMsi) {
		this.conMsi = conMsi;
	}

	public int getPlazo() {
		return plazo;
	}

	public void setPlazo(int plazo) {
		this.plazo = plazo;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getTipoTDC() {
		return tipoTDC;
	}

	public void setTipoTDC(String tipoTDC) {
		this.tipoTDC = tipoTDC;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getFechaModifica() {
		return fechaModifica;
	}

	public void setFechaModifica(String fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getNoAutorizacion() {
		return noAutorizacion;
	}

	public void setNoAutorizacion(String noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
	}

	public String getNoAutorizacionCancelacion() {
		return noAutorizacionCancelacion;
	}

	public void setNoAutorizacionCancelacion(String noAutorizacionCancelacion) {
		this.noAutorizacionCancelacion = noAutorizacionCancelacion;
	}

	public int getIdTransaccionCancel() {
		return idTransaccionCancel;
	}

	public void setIdTransaccionCancel(int idTransaccionCancel) {
		this.idTransaccionCancel = idTransaccionCancel;
	}

	public String getDatoExtra() {
		return datoExtra;
	}

	public void setDatoExtra(String datoExtra) {
		this.datoExtra = datoExtra;
	}
	
}
