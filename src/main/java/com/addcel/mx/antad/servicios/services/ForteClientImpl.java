package com.addcel.mx.antad.servicios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.mx.antad.servicios.model.vo.ForteRequest;
import com.addcel.mx.antad.servicios.model.vo.ForteResponse;
import com.addcel.mx.antad.servicios.model.vo.ForteReverse;
import com.google.gson.Gson;

@Service
public class ForteClientImpl {
	
	@Autowired
	private AntadServiciosMapper mapper;

//	@Autowired
//	private PushClient pushClient;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ForteClientImpl.class);
	
	private static final String PATH_AUTHORIZE = "http://localhost/Forte/transactions/sale/generic/database/383684/244532/{idUsuario}";
	
	private static final String PATH_REVERSE = "http://localhost/Forte/transactions/sale/void/383684/244532/{id_Usuario}";
	
	private static final String PATH_MANUAL = "http://localhost/Forte/transactions/sale/generic/manual/383684/244532/{idUsuario}";
	
	private static final String AUTH = "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx";
	
	private Gson GSON = new Gson();
	
	
	public ForteResponse authorize(ForteRequest request, Long idUsuario) {
		ForteResponse response = null;
		try {
			
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.add("AUTHORIZATION", AUTH);
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<ForteRequest> requestEntity = new HttpEntity<>(request,headers);
	        LOGGER.info("Request Object: {}", GSON.toJson(request));
	        System.out.println("Request Object: "+requestEntity);
	        ResponseEntity<String> respForte = restTemplate.exchange(PATH_AUTHORIZE.replace("{idUsuario}",idUsuario.toString()), 
	        		HttpMethod.POST, requestEntity, String.class);
	        
	        LOGGER.info("RESPUESTA DE FORTE: {}. Status: {}",respForte.getBody(),respForte.getStatusCode());
	        if (HttpStatus.CREATED ==  respForte.getStatusCode()) {
				response = GSON.fromJson(respForte.getBody(), ForteResponse.class);
			}else {
				throw new Exception("Error al consultar Forte");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return response;
	}
	
	public ForteResponse reverse(String transactionId, String imei, Long idUsuario, double lat, double lon, String modelo, 
			String software) {
		ForteResponse response = null;
		ForteReverse reverse = new ForteReverse();
		try {
			reverse.setIdApplicacion(1);
			reverse.setIdioma("es");
			reverse.setImei(imei);
			reverse.setLat(lat);
			reverse.setLon(lon);
			reverse.setModelo(modelo);
			reverse.setSoftware(software);
			reverse.setTransactionId(transactionId);
			
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.add("AUTHORIZATION", AUTH);
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<ForteReverse> requestEntity = new HttpEntity<>(reverse,headers);
	        LOGGER.info("Reverse Object: {}", GSON.toJson(reverse));
	        System.out.println("Request Object: "+requestEntity);
	        ResponseEntity<String> respForte = restTemplate.exchange(PATH_REVERSE.replace("{idUsuario}",idUsuario.toString()), 
	        		HttpMethod.POST, requestEntity, String.class);
	        
	        LOGGER.info("RESPUESTA DE FORTE REVERSO: {}. Status: {}",respForte.getBody(),respForte.getStatusCode());
	        if (HttpStatus.CREATED ==  respForte.getStatusCode()) {
				response = GSON.fromJson(respForte.getBody(), ForteResponse.class);
			}else {
				throw new Exception("Error al consultar Forte");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return response;
	}
	
	

}