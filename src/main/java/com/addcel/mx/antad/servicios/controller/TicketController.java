package com.addcel.mx.antad.servicios.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.services.TicketService;

@Controller
public class TicketController {

	@Autowired
	TicketService service;
	
	@RequestMapping(value = "/enviaReporte/transacto")
	public ModelAndView procesaPago(@ModelAttribute MobilePaymentRequestVO pago,  ModelMap modelo, HttpSession session) {
		return service.reportaIncidente(pago, modelo, session.getId());
	}
		
}
