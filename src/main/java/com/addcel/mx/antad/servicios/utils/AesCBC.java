package com.addcel.mx.antad.servicios.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class AesCBC {
	
	private byte[] key;
    
	private byte[] iv;

    private static final String ALGORITHM = "AES";
    
    public AesCBC() {
	}
    
    public static void main(String[] args) {
		byte[] test="Primer Test Claro".getBytes();
//		byte[] key = DatatypeConverter.parseHexBinary("ff39a0df-9d11-4c10-85a0-cfd16c67e518");
		byte[] key = "ff39a0df-9d11-4c10-85a0-cfd16c67e518".getBytes();
		byte[] iv = DatatypeConverter.parseHexBinary("064df9633d9f5dd0b5614843f6b4b059");
		AesCBC aes = new AesCBC(key,iv);
		try{
		    String result = DatatypeConverter.printBase64Binary(aes.encrypt(test));
		    System.out.println(result);
		}catch(Exception e){
		    e.printStackTrace();
		}
	}

    public AesCBC(byte[] key, byte[] iv) {
        this.key = key;
        this.iv = iv;
    }

    public byte[] encrypt(byte[] plainText) throws Exception{
        SecretKeySpec secretKey = new SecretKeySpec(key,ALGORITHM);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
        Cipher cipher=Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
        return cipher.doFinal(plainText);
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }

    public byte[] getIv() {
        return iv;
    }

    public void setIv(byte[] iv) {
        this.iv = iv;
    }
}
