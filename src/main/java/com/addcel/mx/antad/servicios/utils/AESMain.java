package com.addcel.mx.antad.servicios.utils;

public class AESMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String src = "Primer Test Claro";
		String encrypted = AESUtil.encrypt(src);
		String decrypted = AESUtil.decrypt(encrypted);
		System.out.println("src: " + src);
		System.out.println("encrypted: " + encrypted);
		System.out.println("decrypted: " + decrypted);
		//2UZpiCwLgjMSCP7XsU5HllTBqAVyGHqdMKrRp+Vutao=
		
		System.out.println("*****************************");
		src = "1234567891234567";
		encrypted = AESUtil.encrypt(src);
		decrypted = AESUtil.decrypt(encrypted);
		System.out.println("src: " + src);
		System.out.println("encrypted: " + encrypted);
		System.out.println("decrypted: " + decrypted);
		//efwIHjKM3720vuvAL75FKt/lkk7oJR3mJbkLclQ1mCU=
		
//		try {
//			System.out.println("*****************************");
//			decrypted = AESUtil.decrypt("T09FHRyRbIvYFE1LO6o4yXwwoeWZvoFZwiBHVxIx+Ug=");
//			System.out.println("decrypted: " + decrypted);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}

}