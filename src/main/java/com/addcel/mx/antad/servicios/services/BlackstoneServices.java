package com.addcel.mx.antad.servicios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.mx.antad.servicios.model.vo.BlackstoneRequest;
import com.addcel.mx.antad.servicios.model.vo.BlackstoneResponse;
import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.utils.UtilsService;

@Service
public class BlackstoneServices {

	private static final Logger LOGGER = LoggerFactory.getLogger(BlackstoneServices.class);
	
	private static final String URL_BLACKSTONE = "http://localhost/MCBlackstoneServices/mobilecard/blackstone/send/payment";
	
	@Autowired
	private UtilsService jsonUtils;

	public BlackstoneResponse blackstonePayment(MobilePaymentRequestVO pago){
		String json = null;
		BlackstoneRequest blackstoneRequest = new BlackstoneRequest();
		BlackstoneResponse blackstoneResponse = null;
		try {
			blackstoneRequest.setIdUsuario(Long.valueOf(pago.getIdUser()));
			blackstoneRequest.setConcepto("PAY TOP UP");
			blackstoneRequest.setIdProveedor(Integer.valueOf(pago.getIdProveedor()));
			blackstoneRequest.setIdProducto(91);
			blackstoneRequest.setMonto(pago.getCargo()+pago.getComision());
			blackstoneRequest.setCvv2("");
			blackstoneRequest.setSoftware(pago.getSoftware());
			blackstoneRequest.setWkey(null);
			blackstoneRequest.setImei(pago.getImei());
			blackstoneRequest.setTipo(pago.getTipo());
			blackstoneRequest.setModelo(pago.getModelo());
			blackstoneRequest.setZipCode("");
			blackstoneRequest.setCardNumber("4111111111111111");
			blackstoneRequest.setExpDate("1218");
			blackstoneRequest.setReversalid("1");
			blackstoneRequest.setTransactionType("1");
			blackstoneRequest.setNameOnCard("Test");
			json = jsonUtils.objectToJson(blackstoneRequest);
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("json", json);
			
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
			
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(URL_BLACKSTONE, map, String.class);
			blackstoneResponse = (BlackstoneResponse) jsonUtils.jsonToObject(response.getBody(), BlackstoneResponse.class);
			LOGGER.info("BLACKSTONE RESPONSE - {}", response.getBody());
		} catch (Exception e) {
			e.printStackTrace();
			blackstoneResponse = new BlackstoneResponse();
			blackstoneResponse.setErrorCode(0);
			blackstoneResponse.setMessage("ERROR ON BLACKSTONE");
		}
		return blackstoneResponse;
	}
	
}
