package com.addcel.mx.antad.servicios.model.vo;

public class BitacoraVO {

	public BitacoraVO() {
	}
	
	public BitacoraVO(int idApp, long idUsuario, String bitConcepto, String imei,
			String destino, String tarjetaCompra, String tipo, String software,
			String modelo, String wkey, double bitCargo, String idioma) {
		super();
		this.idApp = idApp;
		this.idUsuario = idUsuario;
		this.concepto = bitConcepto;
		this.imei = imei;
		this.destino = destino;
		this.tarjetaCompra = tarjetaCompra;
		this.tipo = tipo;
		this.software = software;
		this.modelo = modelo;
		this.wkey = wkey;
		this.cargo = bitCargo;
		this.idioma = idioma;
	}
	
	private long idTransaccion;
	
	private long idUsuario; 
	
	private int idProveedor;
	
	private long idProducto;
	
	private long idBitacora;
	
	private String fecha;
	
	private String hora;
	
	private String concepto;
	
	private double cargo;
	
	private String ticket;
	
	private String noAutorizacion;
	
	private String codigoError;
	
	private int cardId;
	
	private int status;
	
	private String imei;
	
	private String destino;
	
	private String tarjetaCompra;
	
	private String tipo;
	
	private String software;
	
	private String modelo;
	
	private String wkey;
	
	private int pase;
	
	private double comision;
	
	private int idApp;
	
	private String idioma;
	
	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public double getCargo() {
		return cargo;
	}

	public void setCargo(double cargo) {
		this.cargo = cargo;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getNoAutorizacion() {
		return noAutorizacion;
	}

	public void setNoAutorizacion(String noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
	}

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getTarjetaCompra() {
		return tarjetaCompra;
	}

	public void setTarjetaCompra(String tarjetaCompra) {
		this.tarjetaCompra = tarjetaCompra;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getWkey() {
		return wkey;
	}

	public void setWkey(String wkey) {
		this.wkey = wkey;
	}

	public int getPase() {
		return pase;
	}

	public void setPase(int pase) {
		this.pase = pase;
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}

	public int getIdApp() {
		return idApp;
	}

	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

}
