package com.addcel.mx.antad.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AprovisionamientoResponse{

	private int idError;
	
	private String mensajeError;
	
	private double monto;
	
	private String descripcion;
	
	private String numAutorizacion;
	
	private String mensajeAntad;
	
	private String eMail;
	
	private String referencia;
	
	private String concepto;
	
	private long idTransaccion;
	
	private double comision;

	private String respCode;
	
	private String numAuth;
	
	private String mensajeTicket;
	
	private String mensajeCajero;
	
	private String folioTransaccion;
	
	private String folioComercio;
	
	private String datosAdicionales;
	
	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getMensajeAntad() {
		return mensajeAntad;
	}

	public void setMensajeAntad(String mensajeAntad) {
		this.mensajeAntad = mensajeAntad;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getNumAuth() {
		return numAuth;
	}

	public void setNumAuth(String numAuth) {
		this.numAuth = numAuth;
	}

	public String getMensajeTicket() {
		return mensajeTicket;
	}

	public void setMensajeTicket(String mensajeTicket) {
		this.mensajeTicket = mensajeTicket;
	}

	public String getMensajeCajero() {
		return mensajeCajero;
	}

	public void setMensajeCajero(String mensajeCajero) {
		this.mensajeCajero = mensajeCajero;
	}

	public String getFolioTransaccion() {
		return folioTransaccion;
	}

	public void setFolioTransaccion(String folioTransaccion) {
		this.folioTransaccion = folioTransaccion;
	}

	public String getFolioComercio() {
		return folioComercio;
	}

	public void setFolioComercio(String folioComercio) {
		this.folioComercio = folioComercio;
	}

	public String getDatosAdicionales() {
		return datosAdicionales;
	}

	public void setDatosAdicionales(String datosAdicionales) {
		this.datosAdicionales = datosAdicionales;
	}
	
}
